(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [888],
  {
    3005: function (e, n, t) {
      "use strict";
      var r = t(7294);
      n.Z = function () {
        var e = (0, r.useState)(),
          n = e[0],
          t = e[1];
        return (
          (0, r.useEffect)(function () {
            var e = function () {
              var e = window.innerWidth < 992;
              t(e);
            };
            return (
              e(),
              window.addEventListener("resize", e),
              function () {
                window.removeEventListener("resize", e);
              }
            );
          }, []),
          n
        );
      };
    },
    5325: function (e, n, t) {
      "use strict";
      t.r(n),
        t.d(n, {
          default: function () {
            return y;
          },
        });
      var r = t(5893),
        o = t(6265),
        i = t(9008),
        a = t(1163),
        c = t(7294),
        s = function () {
          var e = (0, a.useRouter)(),
            n = (0, c.useState)(!1),
            t = n[0],
            o = n[1];
          return (
            (0, c.useEffect)(
              function () {
                var n = (e.pathname, !0);
                o(n);
              },
              [e]
            ),
            (0, r.jsx)(r.Fragment, {
              children:
                t &&
                (0, r.jsx)("footer", {
                  id: "footer",
                  children: (0, r.jsxs)("div", {
                    className: "row mx-0 py-4",
                    children: [
                      (0, r.jsx)("div", {
                        className: "col-md-6 py-3 py-lg-0 px-0",
                        children: (0, r.jsx)("a", {
                          className: "return-home-footer",
                          href: `index.html`,
                          children: (0, r.jsx)("p", {
                            className: "logo-footer",
                            children: "Mambo",
                          }),
                        }),
                      }),
                      (0, r.jsxs)("div", {
                        className: "col-md-6 py-3 py-lg-0 px-0 info-footer",
                        children: [
                          (0, r.jsxs)("div", {
                            className:
                              "iconos d-flex justify-content-center justify-content-md-end",
                            children: [
                              (0, r.jsx)("a", {
                                href: "https://www.instagram.com/mambogota/",
                                target: "_blank",
                                className: "icon-instagram",
                                children: " instagram",
                              }),
                              (0, r.jsx)("a", {
                                href: "https://www.youtube.com/channel/UCCv1ANvfMC0LyV_CBw8U-UA",
                                target: "_blank",
                                className: "icon-youtube",
                                children: " youtube",
                              }),
                              (0, r.jsx)("a", {
                                href: "https://www.facebook.com/fanmambogota",
                                target: "_blank",
                                className: "icon-facebook",
                                children: " facebook",
                              }),
                              (0, r.jsx)("a", {
                                href: "https://twitter.com/_mambogota",
                                target: "_blank",
                                className: "icon-twitter",
                                children: " twitter",
                              }),
                              (0, r.jsx)("a", {
                                href: "https://tiktok.com/@mambogota",
                                target: "_blank",
                                className: "icon-tiktok",
                                children: " tiktok",
                              }),
                            ],
                          }),
                          (0, r.jsx)("p", { children: "Contacto: " }),
                          (0, r.jsx)("p", { children: "(+571) 2860466" }),
                          (0, r.jsx)("p", { children: "(+57) 313 432 54 71" }),
                          (0, r.jsx)("p", { children: "educacion@mambogota.com" }),
                        ],
                      }),
                    ],
                  }),
                }),
            })
          );
        },
        l =
          (t(1664),
            function (e, n) {
              (0, c.useEffect)(
                function () {
                  function t(t) {
                    e.current && !e.current.contains(t.target) && n(t.target);
                  }
                  return (
                    document.addEventListener("mousedown", t),
                    function () {
                      document.removeEventListener("mousedown", t);
                    }
                  );
                },
                [e]
              );
            }),
        u = function (e) {
          var n = e.name,
            t = e.options,
            o = (0, c.useState)(!1),
            i = o[0],
            a = o[1],
            s = (0, c.useRef)(null);
          l(s, function () {
            a(!1);
          });
          return (0, r.jsx)(r.Fragment, {
            children: (0, r.jsxs)("li", {
              ref: s,
              className: i ? "subMenu-header option-active" : "subMenu-header",
              onMouseEnter: function () {
                return window.innerWidth > 992 ? a(!0) : "";
              },
              onMouseLeave: function () {
                return window.innerWidth > 992 ? a(!1) : "";
              },
              onClick: function () {
                window.innerWidth < 992 && a(!i);
              },
              children: [
                (0, r.jsxs)("div", {
                  className: "options-menu-header",
                  children: [
                    "Cursos" === n ? (0, r.jsx)("a", { href: "./cursos", children: n }) : n,
                    (0, r.jsx)("div", { className: "icono-down" }),
                  ],
                }),
                i && (0, r.jsx)("div", { children: (0, r.jsx)("ul", { children: t() }) }),
              ],
            }),
          });
        },
        d = t(5552),
        m = t(1722),
        f = t(3005),
        h = function () {
          var e = (0, c.useState)(0),
            n = e[0],
            t = e[1],
            o = (0, c.useState)(0),
            i = o[0],
            s = o[1],
            l = (0, c.useState)(!1),
            h = l[0],
            p = l[1],
            j = (0, c.useState)(!1),
            w = j[0],
            v = j[1],
            g = (0, a.useRouter)(),
            b = (0, f.Z)();
          (0, c.useEffect)(
            function () {
              if ("/" === g.pathname && !!document.getElementById("galeria-mosaico")) {
                var e = document.getElementById("galeria-mosaico").offsetTop;
                window.scrollTo({ top: e - n, behavior: "smooth" });
              } else "/mambo-viajero" === g.pathname ? t(75) : t(147);
            },
            [g, i]
          );
          var x = (0, c.useCallback)(function (e) {
            if (null !== e) {
              var r = !1,
                o = function (e) {
                  r ||
                    window.requestAnimationFrame(function () {
                      window.scrollY >= 10 ? p(!0) : p(!1), (r = !1);
                    }),
                    (r = !0);
                },
                a = function () {
                  e.getBoundingClientRect().width > 992 && v(!1);
                },
                c = function () {
                  var r;
                  b ? (r = e.getBoundingClientRect().height) : (r = window.scrollY > 0 ? 75 : 147);
                  var o = "/mambo-viajero" === g.pathname ? 75 : r;
                  t(o), s(o <= i ? o : n);
                };
              return (
                c(),
                a(),
                window.addEventListener("resize", c),
                window.addEventListener("scroll", c),
                window.addEventListener("resize", a),
                window.addEventListener("scroll", o),
                function () {
                  window.removeEventListener("resize", c),
                    window.removeEventListener("scroll", c),
                    window.removeEventListener("resize", a),
                    window.removeEventListener("scroll", o);
                }
              );
            }
          }, []);
          const ruta = window.location.href.search('/www/') !== -1 ? '/www/' : '/app.asar/'
          var num = window.location.href.split(ruta)[1].split('/')
          var link = "";
          num.forEach((el, index) => link = index !== 0 ? link + '../' : '')
          return (0, r.jsxs)(r.Fragment, {
            children: [
              (0, r.jsxs)("header", {
                id: "header",
                ref: x,
                className: (function () {
                  var e;
                  return (
                    w
                      ? (e =
                        "/mambo-viajero" === g.pathname
                          ? "header-scroll menu-responsive"
                          : "header menu-responsive")
                      : ((e = h ? "header-scroll" : "header"),
                        (e = "/mambo-viajero" === g.pathname ? "header-scroll" : e)),
                    e
                  );
                })(),
                children: [
                  (0, r.jsxs)("div", {
                    className: "d-flex align-item-center pt-3 pb-2",
                    children: [
                      (0, r.jsxs)("div", {
                        className: "logo d-flex align-item-center",
                        children: [
                          (0, r.jsx)("a", { href: "/", children: "Academia mambo" }),
                          (0, r.jsx)("img", {
                            src: w ? `${link}images/isologo-blanco.svg` : `${link}images/isologo.svg`,
                            role: "button",
                            onClick: function () {
                              return (window.location.href = "/");
                            },
                          }),
                        ],
                      }),
                      (0, r.jsxs)("div", {
                        className: "icono-menu ms-auto d-lg-none",
                        onClick: function () {
                          return v(!w);
                        },
                        children: [
                          (0, r.jsx)("img", {
                            alt: "Icono menu",
                            className: w ? "d-none" : "d-block",
                            src: `${link}/images/iconos/menu.svg`,
                          }),
                          (0, r.jsx)("img", {
                            alt: "Icono cerrar",
                            className: w ? "d-block" : "d-none",
                            src: `${link}/images/iconos/x.svg`,
                          }),
                        ],
                      }),
                    ],
                  }),
                  (0, r.jsxs)("ul", {
                    className: "menu-header",
                    children: [
                      (0, r.jsx)(u, {
                        name: "Cursos",
                        options: function () {
                          var e = [];
                          return (
                            d.Z.forEach(function (n, t) {

                              if (!!window.location.href) {

                                const ruta = window.location.href.search('/www/') !== -1 ? '/www/' : '/app.asar/'
                                var num = window.location.href.split(ruta)[1].split('/')
                                var link = "";
                                num.forEach((el, index) => link = index !== 0 ? link + '../' : '')
                              }
                              e.push(
                                (0, r.jsx)(
                                  "li",
                                  {
                                    onClick: function () {
                                      return v(!1);
                                    },
                                    children: (0, r.jsx)("a", {
                                      href: link.concat((0, m.Z)(n.name)).concat('/index.html'),
                                      children: n.name,
                                    }),
                                  },
                                  "cursos-".concat(t)
                                )
                              );
                            }),
                            e
                          );
                        },
                      }),
                      (0, r.jsx)(u, {
                        name: "Explora",
                        options: function () {
                          var e = [];
                          return (
                            e.push(
                              (0, r.jsx)(
                                "li",
                                {
                                  onClick: function () {
                                    return v(!1);
                                  },
                                  children: (0, r.jsx)("a", {
                                    href: `${link}ficha-mambo-viajero/index.html`,
                                    children: "App Aula MAMBO",
                                  }),
                                },
                                "ficha-mambo-viajero"
                              )
                            ),
                            [
                              { name: "Colecci\xf3n mambo", link: "https://www.mambogota.com/" },
                              {
                                name: "Centro de documentaci\xf3n",
                                link: "https://www.mambogota.com/",
                              },
                              { name: "Cont\xe1ctanos", link: "mailto: educacion@mambogota.com" },
                            ].forEach(function (n, t) {
                              e.push(
                                (0, r.jsx)(
                                  "li",
                                  {
                                    onClick: function () {
                                      return v(!1);
                                    },
                                    children: (0, r.jsx)("a", {
                                      href: n.link,
                                      target: "_blank",
                                      rel: "noopener noreferrer",
                                      onClick: function () {
                                        return v(!1);
                                      },
                                      children: n.name,
                                    }),
                                  },
                                  "explora-".concat(t)
                                )
                              );
                            }),
                            e
                          );
                        },
                      }),
                      (0, r.jsx)("li", {
                        onClick: function () {
                          return v(!1);
                        },
                        children: (0, r.jsx)("a", { href: "/acerca", children: "Acerca de" }),
                      }),
                      (0, r.jsx)("li", {
                        className: "ms-lg-auto mt-auto mt-lg-0",
                        children: (0, r.jsx)("a", {
                          href: "",
                          className: "btn-iniciar-sesion",
                          children: "iniciar sesi\xf3n",
                        }),
                      }),
                    ],
                  }),
                ],
              }),
              (0, r.jsx)("div", { className: "white-space", style: { height: n } }),
            ],
          });
        },
        p = t(5921);
      function j(e, n) {
        var t = Object.keys(e);
        if (Object.getOwnPropertySymbols) {
          var r = Object.getOwnPropertySymbols(e);
          n &&
            (r = r.filter(function (n) {
              return Object.getOwnPropertyDescriptor(e, n).enumerable;
            })),
            t.push.apply(t, r);
        }
        return t;
      }
      function w(e) {
        for (var n = 1; n < arguments.length; n++) {
          var t = null != arguments[n] ? arguments[n] : {};
          n % 2
            ? j(Object(t), !0).forEach(function (n) {
              (0, o.Z)(e, n, t[n]);
            })
            : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t))
              : j(Object(t)).forEach(function (n) {
                Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(t, n));
              });
        }
        return e;
      }
      var v = function (e, n) {
        switch (n.type) {
          case "SET_DATA":
            return w(w({}, e), {}, { generalData: n.data });
          default:
            return e;
        }
      },
        g = { language: "es", pending: !0, generalData: [] },
        b = (0, p.fH)(function () {
          return (0, c.useReducer)(v, g);
        }),
        x = b.Provider;
      b.useTrackedState, b.useUpdate, t(8882), t(3184), t(4525);
      function O(e, n) {
        var t = Object.keys(e);
        if (Object.getOwnPropertySymbols) {
          var r = Object.getOwnPropertySymbols(e);
          n &&
            (r = r.filter(function (n) {
              return Object.getOwnPropertyDescriptor(e, n).enumerable;
            })),
            t.push.apply(t, r);
        }
        return t;
      }
      function k(e) {
        for (var n = 1; n < arguments.length; n++) {
          var t = null != arguments[n] ? arguments[n] : {};
          n % 2
            ? O(Object(t), !0).forEach(function (n) {
              (0, o.Z)(e, n, t[n]);
            })
            : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t))
              : O(Object(t)).forEach(function (n) {
                Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(t, n));
              });
        }
        return e;
      }
      function y(e) {
        var n = e.Component,
          t = e.pageProps;
        return (0, r.jsxs)(r.Fragment, {
          children: [
            (0, r.jsxs)(i.default, {
              children: [
                (0, r.jsx)("title", { children: "Academia MAMBO" }),
                (0, r.jsx)("a", { rel: "icon", href: "/favicon.ico" }),
              ],
            }),
            (0, r.jsx)(h, {}),
            (0, r.jsx)(x, { children: (0, r.jsx)(n, k({}, t)) }),
            (0, r.jsx)(s, {}),
          ],
        });
      }
    },
    1780: function (e, n, t) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "/_app",
        function () {
          return t(5325);
        },
      ]);
    },
    8882: function () { },
  },
  function (e) {
    var n = function (n) {
      return e((e.s = n));
    };
    e.O(0, [774, 246, 433, 707, 519, 2], function () {
      return n(1780), n(2441);
    });
    var t = e.O();
    _N_E = t;
  },
]);
