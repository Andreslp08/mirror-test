(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [448],
  {
    3729: function (s, e, n) {
      "use strict";
      n.r(e);
      var c = n(5893),
        i = n(5552),
        r = n(1722);
      e.default = function () {
        return (0, c.jsx)(c.Fragment, {
          children: (0, c.jsxs)("div", {
            className: "container-fluid px-0",
            children: [
              (0, c.jsxs)("div", {
                className: "main-content",
                children: [
                  (0, c.jsx)("h1", { className: "titulos-azul", children: "Cursos" }),
                  (0, c.jsx)("p", {
                    className: "mb-lg-4 text-acerca",
                    children:
                      "Cursos virtuales con artistas invitados nacionales e internacionales, incluyen videotutoriales, referentes, ejercicios de creaci\xf3n, conceptos clave y preguntas.",
                  }),
                ],
              }),
              (0, c.jsx)("div", {
                className: "cursos-option",
                children: (function () {
                  var s = [];
                  return (
                    i.Z.forEach(function (e, n) {
                      s.push(
                        (0, c.jsxs)(
                          "a",
                          {
                            href: "/".concat((0, r.Z)(e.name)),
                            className: "home-cover-item d-block",
                            children: [
                              (0, c.jsxs)("div", {
                                className: "titulo-cursos",
                                children: [
                                  (0, c.jsx)("h2", { children: e.name }),
                                  (0, c.jsx)("p", { children: e.subtitle }),
                                ],
                              }),
                              (0, c.jsx)("p", {
                                className: "curso-hover",
                                children: e.description,
                              }),
                            ],
                          },
                          "cursos-".concat(n)
                        )
                      );
                    }),
                    s
                  );
                })(),
              }),
            ],
          }),
        });
      };
    },
    4841: function (s, e, n) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "/cursos",
        function () {
          return n(3729);
        },
      ]);
    },
  },
  function (s) {
    s.O(0, [774, 246, 433, 707, 2], function () {
      return (e = 4841), s((s.s = e));
      var e;
    });
    var e = s.O();
    _N_E = e;
  },
]);
