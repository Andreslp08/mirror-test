(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [405, 179],
  {
    807: function (e, n, s) {
      "use strict";
      var a = s(5893),
        i = s(3845),
        r = s(2997),
        o = s(787),
        t = s(7649),
        c = s(4519),
        l = s(4002);
      i.Z.use([r.Z, o.Z, t.Z]);
      n.Z = function () {
        return (0, a.jsx)(a.Fragment, {
          children: (0, a.jsxs)(c.t, {
            className: "banner-home",
            slidesPerView: 1,
            loop: !0,
            autoplay: { delay: 8e3, disableOnInteraction: !1 },
            pagination: {
              clickable: !0,
              renderBullet: function (e, n) {
                return '<span class="' + n + '">' + (e + 1) + "</span>";
              },
              dynamicBullets: !0,
              dynamicMainBullets: 3,
            },
            lazy: !0,
            centeredSlides: !0,
            children: [
              (0, a.jsx)(l.o, {
                className: "py-5",
                style: { backgroundImage: "url(/images/testimonios/testimonio_1.jpg)" },
                children: (0, a.jsx)("p", {
                  children:
                    "\u201cParticip\xe9 en 'H\xe9roes y Villanos', aprend\xed sobre el uso de la t\xe9cnica del collage, sobre la necesidad de trabajar las distintas dimensiones humanas, tanto aquellas que tienen que ver con las virtudes como con la sombra humana, presentes en cada existencia.\u201d",
                }),
              }),
              (0, a.jsx)(l.o, {
                className: "py-5",
                style: { backgroundImage: "url(/images/testimonios/testimonio_2.jpg)" },
                children: (0, a.jsx)("p", {
                  children:
                    "\u201cEn 'Cuerpos Desobedientes' aprend\xed todo sobre el performance.\u201d ",
                }),
              }),
              (0, a.jsx)(l.o, {
                className: "py-5",
                style: { backgroundImage: "url(/images/testimonios/testimonio_3.jpg)" },
                children: (0, a.jsx)("p", {
                  children:
                    "\u201cEn 'Gente haciendo cosas raras a distancia' aprend\xed t\xe9cnicas de creaci\xf3n interesantes y diferentes maneras de comunicarme de una forma din\xe1mica utilizando fotos, videos, audios y otros. Me gust\xf3 mucho el desarrollo de los diferentes ejercicios que sacaron una gran creatividad en m\xed.\u201d",
                }),
              }),
              (0, a.jsx)(l.o, {
                className: "py-5",
                style: { backgroundImage: "url(/images/testimonios/testimonio_4.jpg)" },
                children: (0, a.jsx)("p", {
                  children:
                    '"El app Aula MAMBO est\xe1 muy bien desarrollada, es llamativa y de f\xe1cil manejo."',
                }),
              }),
              (0, a.jsx)(l.o, {
                className: "py-5",
                style: { backgroundImage: "url(/images/testimonios/testimonio_7.jpg)" },
                children: (0, a.jsx)("p", {
                  children:
                    '"Estoy muy agradecido con el Museo de Arte Moderno de Bogot\xe1 ya que nos ha brindado las herramientas para que nuestros estudiantes se familiaricen con el arte moderno, las estrategias son muy innovadoras y han gustado en la comunidad educativa, ya que estas actividades no solo han impactado en los estudiantes sino en los docentes y los padres de familia." ',
                }),
              }),
            ],
          }),
        });
      };
    },
    3005: function (e, n, s) {
      "use strict";
      var a = s(7294);
      n.Z = function () {
        var e = (0, a.useState)(),
          n = e[0],
          s = e[1];
        return (
          (0, a.useEffect)(function () {
            var e = function () {
              var e = window.innerWidth < 992;
              s(e);
            };
            return (
              e(),
              window.addEventListener("resize", e),
              function () {
                window.removeEventListener("resize", e);
              }
            );
          }, []),
          n
        );
      };
    },
    6071: function (e, n, s) {
      "use strict";
      var a = s(3848);
      var i = s(9448)(s(7294)),
        r = s(1689),
        o = s(2441),
        t = s(5749),
        c = {};
      function l(e, n, s, a) {
        if (e && (0, r.isLocalURL)(n)) {
          e.prefetch(n, s, a).catch(function (e) {
            0;
          });
          var i = a && "undefined" !== typeof a.locale ? a.locale : e && e.locale;
          c[n + "%" + s + (i ? "%" + i : "")] = !0;
        }
      }
    },
    5749: function (e, n, s) {
      "use strict";
      var a = s(3848);
      (n.__esModule = !0),
        (n.useIntersection = function (e) {
          var n = e.rootMargin,
            s = e.disabled || !o,
            c = (0, i.useRef)(),
            l = (0, i.useState)(!1),
            d = a(l, 2),
            u = d[0],
            m = d[1],
            p = (0, i.useCallback)(
              function (e) {
                c.current && (c.current(), (c.current = void 0)),
                  s ||
                    u ||
                    (e &&
                      e.tagName &&
                      (c.current = (function (e, n, s) {
                        var a = (function (e) {
                            var n = e.rootMargin || "",
                              s = t.get(n);
                            if (s) return s;
                            var a = new Map(),
                              i = new IntersectionObserver(function (e) {
                                e.forEach(function (e) {
                                  var n = a.get(e.target),
                                    s = e.isIntersecting || e.intersectionRatio > 0;
                                  n && s && n(s);
                                });
                              }, e);
                            return t.set(n, (s = { id: n, observer: i, elements: a })), s;
                          })(s),
                          i = a.id,
                          r = a.observer,
                          o = a.elements;
                        return (
                          o.set(e, n),
                          r.observe(e),
                          function () {
                            o.delete(e),
                              r.unobserve(e),
                              0 === o.size && (r.disconnect(), t.delete(i));
                          }
                        );
                      })(
                        e,
                        function (e) {
                          return e && m(e);
                        },
                        { rootMargin: n }
                      )));
              },
              [s, n, u]
            );
          return (
            (0, i.useEffect)(
              function () {
                if (!o && !u) {
                  var e = (0, r.requestIdleCallback)(function () {
                    return m(!0);
                  });
                  return function () {
                    return (0, r.cancelIdleCallback)(e);
                  };
                }
              },
              [u]
            ),
            [p, u]
          );
        });
      var i = s(7294),
        r = s(8391),
        o = "undefined" !== typeof IntersectionObserver;
      var t = new Map();
    },
    7442: function (e, n, s) {
      "use strict";
      s.r(n),
        s.d(n, {
          default: function () {
            return c;
          },
        });
      var a = s(5893),
        i = s(807),
        r = (s(1664), s(7294)),
        o = s(3005),
        t = function () {
          var e = (0, r.useState)(),
            n = e[0],
            s = e[1],
            t = (0, o.Z)();
          (0, r.useEffect)(
            function () {
              return (
                c(),
                window.addEventListener("resize", c),
                window.addEventListener("scroll", c),
                function () {
                  window.removeEventListener("resize", c), window.removeEventListener("scroll", c);
                }
              );
            },
            [t]
          );
          var c = function () {
            var e = window.innerHeight;
            s(t ? "1400px" : e - 75);
          };
          return (0, a.jsx)(a.Fragment, {
            children: (0, a.jsx)("div", {
              className: "contenedor",
              children: (0, a.jsxs)("div", {
                className: "container-fluid px-0",
                children: [
                  (0, a.jsxs)("div", {
                    className: "main-content ",
                    children: [
                      (0, a.jsxs)("h1", {
                        className: "titulos-azul ",
                        children: [
                          "El arte colombiano y del mundo ",
                          (0, a.jsx)("br", { className: "d-none d-lg-block" }),
                          " al alcance de cualquier persona",
                        ],
                      }),
                      (0, a.jsxs)("p", {
                        className: "subtitulo-home mb-0",
                        children: [
                          "Arte ",
                          (0, a.jsx)("span", { children: "+" }),
                          " educaci\xf3n ",
                          (0, a.jsx)("span", { children: "+" }),
                          " creatividad ",
                          (0, a.jsx)("span", { children: "+" }),
                          " aprendizaje ",
                          (0, a.jsx)("span", { children: "=" }),
                          " Academia MAMBO",
                        ],
                      }),
                      (0, a.jsxs)("p", {
                        className: "text-home pt-lg-4 mb-0",
                        children: [
                          "Academia MAMBO es la plataforma educativa del Museo de Arte Moderno de Bogot\xe1 que brinda ",
                          (0, a.jsx)("br", { className: "d-none d-lg-block" }),
                          " herramientas interactivas para apoyar los procesos de aprendizaje e investigaci\xf3n art\xedstica. ",
                        ],
                      }),
                    ],
                  }),
                  (0, a.jsxs)("div", {
                    className: "main-option hijo-1",
                    style: { height: n },
                    id: "galeria-mosaico",
                    children: [
                      (0, a.jsx)("a", {
                        className: "home-cover-item option-cursos",
                        href: "/cursos",
                        children: (0, a.jsxs)("div", {
                          children: [
                            (0, a.jsx)("h2", { children: "Cursos" }),
                            (0, a.jsx)("p", {
                              children:
                                "Cursos virtuales con artistas invitados nacionales e internacionales, incluyen videotutoriales, referentes, ejercicios de creaci\xf3n, conceptos clave y preguntas.",
                            }),
                          ],
                        }),
                      }),
                      (0, a.jsx)("a", {
                        className: "home-cover-item option-app",
                        href: "/ficha-mambo-viajero",
                        children: (0, a.jsxs)("div", {
                          children: [
                            (0, a.jsxs)("h2", {
                              children: [
                                "App ",
                                (0, a.jsx)("br", {}),
                                "Aula ",
                                (0, a.jsx)("br", {}),
                                "MAMBO ",
                              ],
                            }),
                            (0, a.jsxs)("p", {
                              className: "text-end",
                              children: [
                                "Aplicaci\xf3n educativa sobre arte ",
                                (0, a.jsx)("br", {}),
                                " moderno, conceptual y contempor\xe1neo ",
                                (0, a.jsx)("br", {}),
                                "en Colombia y el mundo. Incluye ",
                                (0, a.jsx)("br", {}),
                                " recursos en l\xednea, im\xe1genes, una l\xednea ",
                                (0, a.jsx)("br", {}),
                                " de tiempo, un mapa y tres juegos.",
                              ],
                            }),
                          ],
                        }),
                      }),
                      (0, a.jsxs)("div", {
                        className: "home-cover-item option-centro",
                        children: [
                          (0, a.jsxs)("h2", {
                            className: "text-end",
                            children: ["Centro de ", (0, a.jsx)("br", {}), "documentaci\xf3n"],
                          }),
                          (0, a.jsxs)("p", {
                            children: [
                              "Cat\xe1logo bibliogr\xe1fico digital de los ",
                              (0, a.jsx)("br", {}),
                              "libros, revistas, grabaciones de audio ",
                              (0, a.jsx)("br", {}),
                              "y videos de la biblioteca del Museo.",
                            ],
                          }),
                        ],
                      }),
                      (0, a.jsxs)("div", {
                        className: "home-cover-item option-coleccion",
                        children: [
                          (0, a.jsxs)("h2", {
                            className: "text-end",
                            children: ["Colecci\xf3n ", (0, a.jsx)("br", {}), " Mambo"],
                          }),
                          (0, a.jsxs)("p", {
                            className: "text-end",
                            children: [
                              "M\xe1s de 4500 obras de  arte moderno y  ",
                              (0, a.jsx)("br", {}),
                              "contempor\xe1neo.",
                            ],
                          }),
                        ],
                      }),
                    ],
                  }),
                  (0, a.jsx)("div", {
                    className: "main-content",
                    children: (0, a.jsxs)("p", {
                      className: "text-home py-lg-5 mb-0 ",
                      children: [
                        (0, a.jsx)("span", { className: "texto_bold", children: "Ofrecemos" }),
                        " acceso a recursos gratuitos y pagos como cursos virtuales con artistas invitados colombianos e internacionales, la app educativa Aula MAMBO, la Colecci\xf3n y el cat\xe1logo del Centro de documentaci\xf3n del Museo.",
                      ],
                    }),
                  }),
                  (0, a.jsx)(i.Z, { className: "" }),
                ],
              }),
            }),
          });
        },
        c = function () {
          return (0, a.jsx)(a.Fragment, { children: (0, a.jsx)(t, {}) });
        };
    },
    8581: function (e, n, s) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "/",
        function () {
          return s(7442);
        },
      ]);
    },
    1664: function (e, n, s) {
      s(6071);
    },
    4453: function () {},
  },
  function (e) {
    e.O(0, [774, 246, 433], function () {
      return (n = 8581), e((e.s = n));
      var n;
    });
    var n = e.O();
    _N_E = n;
  },
]);
