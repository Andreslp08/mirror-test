(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [963],
  {
    6086: function (e) {
      "use strict";
      var a = Object.assign.bind(Object);
      (e.exports = a), (e.exports.default = e.exports);
    },
    4615: function (e, a, s) {
      "use strict";
      s.r(a);
      var n = s(5893);
      s(7294);
      a.default = function () {
        return (0, n.jsx)(n.Fragment, {
          children: (0, n.jsxs)("div", {
            className: "main-content ficha pb-5",
            children: [
              (0, n.jsx)("h1", { className: "titulos-azul pb-4", children: "App aula mambo" }),
              (0, n.jsx)("iframe", { src: "../video/app-aula-mambo.mp4", className: "video-ficha" }),
              (0, n.jsx)("div", {
                className: "row justify-content-between mt-lg-4 pt-2",
                children: (0, n.jsx)("div", {
                  className: "col-lg-6",
                  children: (0, n.jsx)("a", {
                    href: "http://app.mambogota.com/",
                    target: "_blank",
                    className: "btn-azul",
                    children: "Ingresa al aula mambo",
                  }),
                }),
              }),
              (0, n.jsxs)("div", {
                className: "row mt-5",
                children: [
                  (0, n.jsxs)("div", {
                    className: "col-lg-10",
                    children: [
                      (0, n.jsx)("h3", { className: "subtitulo-ficha", children: "Acerca de" }),
                      (0, n.jsxs)("ul", {
                        className: "modulos-2",
                        children: [
                          "Esta es una aplicaci\xf3n para apoyar los procesos de formaci\xf3n en el \xe1rea de artes de secundaria y para promover el aprendizaje sobre las diversas manifestaciones art\xedsticas modernas y contempor\xe1neas de Colombia y el mundo. Incluye ",
                          (0, n.jsx)("span", { className: "negrilla", children: "9 cap\xedtulos" }),
                          ", cada uno tiene un contenido con ",
                          (0, n.jsx)("span", {
                            className: "negrilla",
                            children: "hiperv\xednculos",
                          }),
                          ", una propuesta de reflexi\xf3n, un ",
                          (0, n.jsx)("span", { className: "negrilla", children: "sab\xedas que" }),
                          " con un dato relevante, ",
                          (0, n.jsx)("span", { className: "negrilla", children: "actividades" }),
                          " que abordan los temas de manera pedag\xf3gica, ",
                          (0, n.jsx)("span", { className: "negrilla", children: "preguntas" }),
                          " que invitan a debatir en diferentes niveles: el personal, el local, el nacional y el global, junto con ",
                          (0, n.jsx)("span", {
                            className: "negrilla",
                            children: "conceptos clave",
                          }),
                          " que contribuyen en la comprensi\xf3n de los temas. Tambi\xe9n se incluyen como recursos transversales una ",
                          (0, n.jsx)("span", {
                            className: "negrilla",
                            children: "l\xednea de tiempo",
                          }),
                          ", un ",
                          (0, n.jsx)("span", {
                            className: "negrilla",
                            children: "mapa interactivo",
                          }),
                          " e im\xe1genes en alta calidad.",
                        ],
                      }),
                      (0, n.jsxs)("ul", {
                        className: "modulos-2",
                        children: [
                          "Cada cap\xedtulo se identifica con un color diferente, \xedconos que dividen los contenidos, botones que permiten activar las ",
                          (0, n.jsx)("span", {
                            className: "negrilla",
                            children: "audiodescripciones",
                          }),
                          " y el ",
                          (0, n.jsx)("span", {
                            className: "negrilla",
                            children: "cambio de idiomas",
                          }),
                          ". Adem\xe1s contiene ",
                          (0, n.jsx)("span", { className: "negrilla", children: "3 juegos" }),
                          ": Responde la ",
                          (0, n.jsx)("span", { className: "negrilla", children: "Trivia" }),
                          " sobre la historia del arte moderno en Colombia, ",
                          (0, n.jsx)("span", {
                            className: "negrilla",
                            children: "Cruza con Zapatos",
                          }),
                          " a trav\xe9s de la autopista de objetos cotidianos, y en ",
                          (0, n.jsx)("span", { className: "negrilla", children: "Runner" }),
                          " conduce el cami\xf3n del MAMBO Viajero recogiendo monedas para coleccionar obras de arte.",
                        ],
                      }),
                    ],
                  }),
                  (0, n.jsxs)("div", {
                    className: "col-lg-10 pt-4",
                    children: [
                      (0, n.jsx)("h3", {
                        className: "subtitulo-ficha",
                        children: "a quien va dirigido",
                      }),
                      (0, n.jsx)("ul", {
                        className: "modulos-2",
                        children:
                          "Esta aplicaci\xf3n est\xe1 dirigida a adolescentes, j\xf3venes, docentes y personas interesadas en la historia del arte moderno, conceptual y contempor\xe1neo en Colombia y el mundo.",
                      }),
                    ],
                  }),
                ],
              }),
              (0, n.jsx)("div", {
                className: "mt-4",
                children: (0, n.jsxs)("div", {
                  className: "row ",
                  children: [
                    (0, n.jsxs)("div", {
                      className: "col-lg-6",
                      children: [
                        (0, n.jsx)("h3", {
                          className: "subtitulo-ficha",
                          children: "Cap\xedtulos",
                        }),
                        (0, n.jsxs)("ul", {
                          className: "modulos-2 info-2",
                          children: [
                            (0, n.jsx)("p", { children: "MAMBO Pop Up" }),
                            (0, n.jsx)("p", { children: "El Arte de la Desobediencia" }),
                            (0, n.jsx)("p", { children: "Arte Moderno en el mundo" }),
                            (0, n.jsx)("p", { children: "Arte Moderno en Colombia" }),
                            (0, n.jsx)("p", { children: "Arte Conceptual en el mundo" }),
                            (0, n.jsx)("p", { children: "Arte Conceptual en Colombia" }),
                            (0, n.jsx)("p", { children: "Arte Contempor\xe1neo en el mundo" }),
                            (0, n.jsx)("p", { children: "Arte Contempor\xe1neo en Colombia" }),
                            (0, n.jsx)("p", { children: "Nuevos Medios" }),
                            (0, n.jsx)("p", { children: "L\xednea de Tiempo" }),
                            (0, n.jsx)("p", { children: "Mapa" }),
                            (0, n.jsx)("p", { children: "Juegos" }),
                          ],
                        }),
                      ],
                    }),
                    (0, n.jsxs)("div", {
                      className: "col-lg-6",
                      children: [
                        (0, n.jsx)("h3", {
                          className: "subtitulo-ficha text-lg-end",
                          children: "HABILIDADES QUE OBTENDR\xc1S",
                        }),
                        (0, n.jsxs)("ul", {
                          className: "modulos-2 text-lg-end",
                          children: [
                            (0, n.jsx)("p", { children: "Pensamiento cr\xedtico y creatividad." }),
                            (0, n.jsx)("p", { children: "Uso de herramientas tecnol\xf3gicas." }),
                            (0, n.jsx)("p", {
                              children: "Formaci\xf3n para la ciudadan\xeda mundial.",
                            }),
                            (0, n.jsxs)("p", {
                              children: [
                                "Entendimiento de los acontecimientos de",
                                (0, n.jsx)("br", {}),
                                " la historia del arte moderno, conceptual y",
                                (0, n.jsx)("br", {}),
                                " contempor\xe1neo en Colombia y del mundo.",
                              ],
                            }),
                            (0, n.jsxs)("p", {
                              children: [
                                "Apropiaci\xf3n del patrimonio de la",
                                (0, n.jsx)("br", {}),
                                " colecci\xf3n del MAMBO.",
                              ],
                            }),
                            (0, n.jsxs)("p", {
                              children: [
                                "Conocimiento sobre los nuevos medios",
                                (0, n.jsx)("br", {}),
                                " del arte contempor\xe1neo.",
                              ],
                            }),
                            (0, n.jsx)("p", {
                              children: "Biling\xfcismo (ingl\xe9s y espa\xf1ol).",
                            }),
                            (0, n.jsx)("p", { children: "An\xe1lisis de im\xe1genes." }),
                          ],
                        }),
                      ],
                    }),
                  ],
                }),
              }),
            ],
          }),
        });
      };
    },
    3349: function (e, a, s) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "/ficha-mambo-viajero",
        function () {
          return s(4615);
        },
      ]);
    },
  },
  function (e) {
    e.O(0, [774], function () {
      return (a = 3349), e((e.s = a));
      var a;
    });
    var a = e.O();
    _N_E = a;
  },
]);
