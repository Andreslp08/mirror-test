(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [728],
  {
    305: function (e, n, t) {
      "use strict";
      var o = t(6265),
        i = t(1722);
      function r(e, n) {
        var t = Object.keys(e);
        if (Object.getOwnPropertySymbols) {
          var o = Object.getOwnPropertySymbols(e);
          n &&
            (o = o.filter(function (n) {
              return Object.getOwnPropertyDescriptor(e, n).enumerable;
            })),
            t.push.apply(t, o);
        }
        return t;
      }
      function c(e) {
        for (var n = 1; n < arguments.length; n++) {
          var t = null != arguments[n] ? arguments[n] : {};
          n % 2
            ? r(Object(t), !0).forEach(function (n) {
                (0, o.Z)(e, n, t[n]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t))
            : r(Object(t)).forEach(function (n) {
                Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(t, n));
              });
        }
        return e;
      }
      n.Z = function (e, n, t, o) {
        var r,
          s = null !== n ? JSON.parse(n) : null,
          a = (0, i.Z)(e.name),
          u = null !== s && s[a],
          l = null === (r = e.contenido) || void 0 === r ? void 0 : r.modulos,
          d = [];
        return (
          void 0 !== l &&
            l.map(function (n) {
              var r = (0, i.Z)(n.name),
                s = { name: n.name, subtitulo: n.subtitulo, active: !!t && r === t, porcentaje: 0 },
                a = [];
              n.contenido.map(function (n) {
                var t = n.name,
                  c = (0, i.Z)(t),
                  s = "/"
                    .concat((0, i.Z)(e.name), "/")
                    .concat(r, "/")
                    .concat(c);
                a.push({ active: !!o && c === o, isComplete: !1, link: s, name: t });
              }),
                d.push(c(c({}, s), {}, { sections: a }));
            }),
          u ? s[a] : { modulos: d, porcentaje: 0 }
        );
      };
    },
    3005: function (e, n, t) {
      "use strict";
      var o = t(7294);
      n.Z = function () {
        var e = (0, o.useState)(),
          n = e[0],
          t = e[1];
        return (
          (0, o.useEffect)(function () {
            var e = function () {
              var e = window.innerWidth < 992;
              t(e);
            };
            return (
              e(),
              window.addEventListener("resize", e),
              function () {
                window.removeEventListener("resize", e);
              }
            );
          }, []),
          n
        );
      };
    },
    8378: function (e, n, t) {
      "use strict";
      t.r(n),
        t.d(n, {
          __N_SSG: function () {
            return w;
          },
          default: function () {
            return x;
          },
        });
      var o = t(5893),
        i = t(6265),
        r = t(7294),
        c = t(1664),
        s = t(3005),
        a = function (e) {
          var n = e.width;
          return (0, o.jsx)("div", {
            className: "progress-bar",
            children: (0, o.jsx)("div", { style: { width: "".concat(n, "%") } }),
          });
        },
        u = function (e) {
          var n = e.width,
            t = n + 1;
          return (
            n >= 10 && (t = n - 15),
            (0, o.jsx)("div", {
              className: "progress-bar-menu",
              style: { width: "".concat(t, "%") },
            })
          );
        },
        l = t(1722),
        d = function (e) {
          var n = e.activeSection,
            t = e.changeActiveSection,
            i = e.index,
            a = e.item,
            u = e.modulo,
            d = e.showHideMenu,
            m = (0, r.useState)(),
            f = m[0],
            h = m[1],
            p = window.location.pathname,
            v = (0, l.Z)(a.name),
            g = (0, s.Z)(),
            j = (0, r.useCallback)(function (e) {
              if (null !== e) {
                e.style.height = "auto";
                var t = e.getBoundingClientRect().height,
                  o = n === v ? t : 0;
                h(t),
                  setTimeout(function () {
                    e.style.height = o;
                  }, 0);
              }
            }, []);
          
          
            var num = e.link.split('/')
            var link = `../../${num[2]}/${num[3]}/index.html`;
          
          return (
            (0, r.useEffect)(
              function () {
                u === (0, l.Z)(v) && t(v, !0);
              },
              [u]
            ),
            (0, o.jsxs)("li", {
              className: "mb-2",
              children: [
                (0, o.jsxs)("p", {
                  className: "d-flex align-items-center text-uppercase mb-0",
                  onClick: function () {
                    t(v);
                  },
                  children: [
                    (0, o.jsx)("span", {
                      className: u === (0, l.Z)(v) ? "num-list num-list-active" : "num-list",
                      children: i + 1,
                    }),
                    (0, o.jsx)("span", {
                      className:
                        u === (0, l.Z)(v) ? "name-section option-list-active" : "name-section",
                      children: a.name,
                    }),
                  ],
                }),
                (0, o.jsx)("ul", {
                  className: "menu-section-content",
                  ref: j,
                  style: { height: n === v ? f : 0 },
                  children: a.sections.map(function (e, n) {
                    return (0, o.jsxs)(
                      "li",
                      {
                        className: p === e.link ? "option-list-active" : "",
                        onClick: function () {
                          t(v, !1), g && d();
                        },
                        children: [
                          (0, o.jsx)(c.default, { href: link, children: e.name }),
                          e.isComplete && (0, o.jsx)("span", { className: "icon-check" }),
                        ],
                      },
                      "moduloMenu".concat(n)
                    );
                  }),
                }),
              ],
            })
          );
        },
        m = (0, r.forwardRef)(function (e, n) {
          var t = e.itemsMenu,
            i = e.heightMenu,
            c = e.modulo,
            s = e.showHideMenu,
            a = e.paddingMenu,
            u = (0, r.useState)(c),
            l = u[0],
            m = u[1],
            f = function (e, n) {
              var t = e;
              void 0 === n && (t = l !== e && e), m(t);
            };
          return (0, o.jsx)("ul", {
            className: "menu-section",
            ref: n,
            style: { height: i, padding: a },
            children: t.map(function (e, n) {
              return (0,
              o.jsx)(d, { activeSection: l, changeActiveSection: f, index: n, item: e, modulo: c, showHideMenu: s }, "itemMenu".concat(n));
            }),
          });
        }),
        f = (0, r.forwardRef)(function (e, n) {
          var t = e.heightMenu,
            i = e.infoMenu,
            r = e.isShowMenu,
            c = e.nombreCurso,
            a = e.modulo,
            l = e.showHideMenu,
            d = n.refMenu,
            f = n.refMenuMobileButton,
            h = (0, s.Z)();
          return (0,
          o.jsxs)(o.Fragment, { children: [h && (0, o.jsxs)("button", { className: "btn-menu-curso titulos-azul w-100 py-2 px-3 px-sm-0 d-flex justify-content-center", onClick: l, ref: f, children: [(0, o.jsx)("p", { className: "col-11 mb-0 mt-1", children: c }), (0, o.jsx)("div", { className: r ? "icono-down-active" : "icono-down" })] }), !h && (0, o.jsxs)(o.Fragment, { children: [(0, o.jsx)("p", { className: "titulos-azul", children: c }), (0, o.jsxs)("div", { className: "progress-menu", children: [(0, o.jsx)(u, { width: i.porcentaje }), (0, o.jsxs)("p", { className: "progress-num", children: [parseInt(i.porcentaje), "%"] })] })] }), (0, o.jsx)(m, { heightMenu: r ? t : 0, itemsMenu: i.modulos, modulo: a, paddingMenu: r && h ? "10px 20px" : 0, ref: d, showHideMenu: l })] });
        }),
        h = function (e) {
          var n = e.subtitulo,
            t = e.contenido,
            i = e.infoMenu,
            u = e.name,
            l = e.modulo,
            d = e.progressBarWidth,
            m = e.nombreCurso,
            h = e.itemsNavegacion,
            p = h.prevItem,
            v = h.nextItem,
            g = (0, s.Z)(),
            j = (0, r.useState)(),
            w = j[0],
            x = j[1],
            b = (0, r.useState)(),
            y = b[0],
            N = b[1],
            O = (0, r.useState)(),
            E = (O[0], O[1]),
            M = (0, r.useState)(0),
            S = M[0],
            Z = M[1],
            k = (0, r.useState)(!g),
            C = k[0],
            P = k[1],
            B = (0, r.useRef)(null),
            _ = (0, r.useRef)(null);
          
            if (!!v) {
              var num = v.link.split('/')
              var link = `../../${num[2]}/${num[3]}/index.html`; 
            }
          
            if (!!p) {
              var num = p.link.split('/')
              var linkAnterior = `../../${num[2]}/${num[3]}/index.html`;
            }
          (0, r.useEffect)(
            function () {
              return (
                z(),
                window.addEventListener("scroll", z),
                window.addEventListener("resize", z),
                function () {
                  window.removeEventListener("scroll", z), window.removeEventListener("resize", z);
                }
              );
            },
            [B, g]
          ),
            (0, r.useEffect)(
              function () {
                var e = null !== _.current ? _.current.getBoundingClientRect().height : 0;
                P(!g), E(e);
              },
              [g]
            );
          var z = function () {
            if (null !== B.current) {
              var e,
                n = document.getElementById("header").getBoundingClientRect().height,
                t = document.getElementById("footer").getBoundingClientRect().top,
                o = B.current.offsetTop,
                i =
                  (e = g
                    ? window.innerHeight - n
                    : t < window.innerHeight
                    ? window.innerHeight - (window.innerHeight - t) - n
                    : window.innerHeight - n) -
                  o -
                  30;
              x(e), Z(n), N(i);
            }
          };
          return (0, o.jsxs)(o.Fragment, {
            children: [
              (0, o.jsx)(a, { width: d }),
              (0, o.jsx)("div", {
                className: "main-content-curso",
                children: (0, o.jsxs)("div", {
                  className: "row mx-0",
                  children: [
                    (0, o.jsx)("div", {
                      className: "menu col-lg-3 px-0",
                      style: { height: C ? w : 0, top: S },
                      children: (0, o.jsx)(f, {
                        heightMenu: y,
                        infoMenu: i,
                        isShowMenu: C,
                        nombreCurso: m,
                        modulo: l,
                        ref: { refMenu: B, refMenuMobileButton: _ },
                        showHideMenu: function (e) {
                          P("object" !== typeof e ? e : !C);
                        },
                      }),
                    }),
                    (0, o.jsx)("div", {
                      className: "contenedor-modulos-curso col-lg-9 pb-lg-5 ",
                      children: (0, o.jsxs)("div", {
                        children: [
                          null !== p &&
                            (0, o.jsx)("div", {
                              className: "d-flex align-items-center link-back",
                              children: (0, o.jsx)(c.default, { href: linkAnterior, children: p.name }),
                            }),
                          "Presentaci\xf3n" !== u
                            ? (0, o.jsx)("p", { className: "titulos-azul", children: u })
                            : (function () {
                                if (null !== v)
                                  return (0, o.jsxs)("p", {
                                    className: "titulos-azul",
                                    children: [
                                      v.nombreModulo,
                                      (0, o.jsx)("br", {}),
                                      (0, o.jsx)("span", {
                                        className: "subtitulo-collage",
                                        children: n,
                                      }),
                                    ],
                                  });
                              })(),
                          t(),
                          null === v &&
                            (0, o.jsx)(o.Fragment, {
                              children: (0, o.jsxs)("div", {
                                className: "d-sm-flex  align-items-center py-3 pt-lg-5 link-next",
                                children: [
                                  (0, o.jsx)("p", {
                                    className: "mb-0 text-continuar",
                                    children: "Continuar con",
                                  }),
                                  (0, o.jsx)(c.default, {
                                    href: "/cursos",
                                    children: "Cursos virtuales",
                                  }),
                                ],
                              }),
                            }),
                          null !== v &&
                            (0, o.jsx)(o.Fragment, {
                              children: (0, o.jsxs)("div", {
                                className: "d-sm-flex  align-items-center py-3 pt-lg-5 link-next",
                                children: [
                                  "Presentaci\xf3n" !== u
                                    ? (0, o.jsx)("p", {
                                        className: "mb-0 text-continuar",
                                        children: "Continuar con",
                                      })
                                    : (0, o.jsx)("p", {
                                        className: "mb-0 text-continuar",
                                        children: "Empezar con",
                                      }),
                                  (0, o.jsx)(c.default, { href: link, children: v.name }),
                                ],
                              }),
                            }),
                        ],
                      }),
                    }),
                  ],
                }),
              }),
            ],
          });
        },
        p = t(5552),
        v = t(305);
      function g(e, n) {
        var t = Object.keys(e);
        if (Object.getOwnPropertySymbols) {
          var o = Object.getOwnPropertySymbols(e);
          n &&
            (o = o.filter(function (n) {
              return Object.getOwnPropertyDescriptor(e, n).enumerable;
            })),
            t.push.apply(t, o);
        }
        return t;
      }
      function j(e) {
        for (var n = 1; n < arguments.length; n++) {
          var t = null != arguments[n] ? arguments[n] : {};
          n % 2
            ? g(Object(t), !0).forEach(function (n) {
                (0, i.Z)(e, n, t[n]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t))
            : g(Object(t)).forEach(function (n) {
                Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(t, n));
              });
        }
        return e;
      }
      var w = !0,
        x = function (e) {
          var n = e.curso,
            t = e.modulo,
            c = e.seccion,
            s = function (n, t) {
              return n.find(function (n) {
                return (0, l.Z)(n.name) === e[t];
              });
            },
            a = function (n, t) {
              return n.findIndex(function (n) {
                return (0, l.Z)(n.name) === e[t];
              });
            },
            u = s(p.Z, "curso"),
            d = s(u.contenido.modulos, "modulo"),
            m = s(d.contenido, "seccion"),
            f = (0, r.useState)(0),
            g = f[0],
            w = f[1],
            x = (0, r.useState)(),
            b = x[0],
            y = x[1],
            N = (0, r.useState)(!1),
            O = N[0],
            E = N[1],
            M = (0, r.useState)({}),
            S = M[0],
            Z = M[1],
            k = (0, r.useRef)(null);
          (0, r.useEffect)(function () {
            var e = localStorage.getItem("academyProgress");
            return (
              y((0, v.Z)(u, e, t, c)),
              E(!0),
              function () {
                window.removeEventListener("scroll", C), window.removeEventListener("resize", C);
              }
            );
          }, []),
            (0, r.useEffect)(
              function () {
                var e,
                  o,
                  i = u.contenido.modulos,
                  r = a(i, "modulo"),
                  c = a(d.contenido, "seccion");
                if (0 === c)
                  if (0 === r) e = null;
                  else {
                    var s = i[r - 1],
                      m = s.contenido[s.contenido.length - 1].name;
                    e = {
                      name: m,
                      link: "/"
                        .concat(n, "/")
                        .concat((0, l.Z)(s.name), "/")
                        .concat((0, l.Z)(m)),
                    };
                  }
                else {
                  var f = d.contenido[c - 1].name;
                  e = {
                    name: f,
                    link: "/"
                      .concat(n, "/")
                      .concat(t, "/")
                      .concat((0, l.Z)(f)),
                  };
                }
                if (c === d.contenido.length - 1)
                  if (r === i.length - 1) o = null;
                  else {
                    var h = i[r + 1],
                      p = h.contenido[0].name;
                    o = {
                      moduloFinalizado: !0,
                      nombreModulo: d.name,
                      name: h.name,
                      link: "/"
                        .concat(n, "/")
                        .concat((0, l.Z)(h.name), "/")
                        .concat((0, l.Z)(p)),
                    };
                  }
                else {
                  var v = d.contenido[c + 1].name;
                  o = {
                    moduloFinalizado: !1,
                    nombreModulo: d.name,
                    name: v,
                    link: "/"
                      .concat(n, "/")
                      .concat(t, "/")
                      .concat((0, l.Z)(v)),
                  };
                }
                Z({ prevItem: e, nextItem: o }),
                  w(0),
                  setTimeout(function () {
                    return (
                      O &&
                        (C(),
                        window.addEventListener("scroll", C),
                        window.addEventListener("resize", C)),
                      function () {
                        window.removeEventListener("scroll", C),
                          window.removeEventListener("resize", C);
                      }
                    );
                  }, 1e3);
              },
              [O, m, d]
            );
          var C = function () {
              if (null !== k.current) {
                var e,
                  n = k.current.getBoundingClientRect().height,
                  t = document.getElementsByTagName("html")[0].scrollTop,
                  o = document.getElementsByTagName("header")[0].getBoundingClientRect().height,
                  i = k.current.getBoundingClientRect().height + o - window.innerHeight;
                if (n > window.innerHeight) {
                  var r = (100 * t) / i,
                    c = r >= 0 ? r : 100;
                  e = c >= 100 ? 100 : c;
                } else e = 100;
                w(e), 100 === e && P();
              }
            },
            P = function () {
              var e,
                t,
                o = b.modulos.slice();
              o.forEach(function (n, t) {
                n.name === d.name && (e = t);
              });
              var r = o[e].sections;
              r.forEach(function (e, n) {
                e.name === m.name && (t = n);
              }),
                (r[t].isComplete = !0);
              var c = 0;
              r.forEach(function (e) {
                e.isComplete && (c += Math.ceil(100 / r.length));
              }),
                (o[e].porcentaje = c);
              var s = 0;
              o.forEach(function (e) {
                var n = 100 / o.length,
                  t = e.porcentaje >= 100 ? n : (e.porcentaje * n) / 100;
                s += t;
              });
              var a = j(
                  j({}, b),
                  {},
                  {
                    porcentaje: s,
                    modulos: o,
                    seccionActiva: "/"
                      .concat(n, "/")
                      .concat((0, l.Z)(d.name), "/")
                      .concat((0, l.Z)(m.name)),
                  }
                ),
                u = localStorage.getItem("academyProgress"),
                f = null !== u ? JSON.parse(u) : {};
              localStorage.setItem(
                "academyProgress",
                JSON.stringify(j(j({}, f), {}, (0, i.Z)({}, n, a)))
              ),
                y(a);
            };
          return (0, o.jsx)("div", {
            children:
              O &&
              (0, o.jsx)("div", {
                ref: k,
                children: (0, o.jsx)(h, {
                  contenido: m.contenido,
                  infoMenu: b,
                  itemsNavegacion: S,
                  modulo: t,
                  name: m.name,
                  nombreCurso: u.name,
                  progressBarWidth: g,
                  subtitulo: d.subtitulo,
                }),
              }),
          });
        };
    },
    3505: function (e, n, t) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "/[curso]/[modulo]/[seccion]",
        function () {
          return t(8378);
        },
      ]);
    },
    6265: function (e, n, t) {
      "use strict";
      function o(e, n, t) {
        return (
          n in e
            ? Object.defineProperty(e, n, {
                value: t,
                enumerable: !0,
                configurable: !0,
                writable: !0,
              })
            : (e[n] = t),
          e
        );
      }
      t.d(n, {
        Z: function () {
          return o;
        },
      });
    },
  },
  function (e) {
    e.O(0, [774, 246, 433, 707, 2], function () {
      return (n = 3505), e((e.s = n));
      var n;
    });
    var n = e.O();
    _N_E = n;
  },
]);
