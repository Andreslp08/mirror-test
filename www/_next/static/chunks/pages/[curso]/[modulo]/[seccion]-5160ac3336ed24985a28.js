(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [728],
  {
    305: function (e, n, t) {
      "use strict";
      var o = t(6265),
        i = t(1722);
      function r(e, n) {
        var t = Object.keys(e);
        if (Object.getOwnPropertySymbols) {
          var o = Object.getOwnPropertySymbols(e);
          n &&
            (o = o.filter(function (n) {
              return Object.getOwnPropertyDescriptor(e, n).enumerable;
            })),
            t.push.apply(t, o);
        }
        return t;
      }
      function c(e) {
        for (var n = 1; n < arguments.length; n++) {
          var t = null != arguments[n] ? arguments[n] : {};
          n % 2
            ? r(Object(t), !0).forEach(function (n) {
              (0, o.Z)(e, n, t[n]);
            })
            : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t))
              : r(Object(t)).forEach(function (n) {
                Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(t, n));
              });
        }
        return e;
      }
      n.Z = function (e, n, t, o) {
        var r,
          s = null !== n ? JSON.parse(n) : null,
          a = (0, i.Z)(e.name),
          u = null !== s && s[a],
          l = null === (r = e.contenido) || void 0 === r ? void 0 : r.modulos,
          d = [];
        return (
          void 0 !== l &&
          l.map(function (n) {
            var r = (0, i.Z)(n.name),
              s = { name: n.name, subtitulo: n.subtitulo, active: !!t && r === t, porcentaje: 0 },
              a = [];
            n.contenido.map(function (n) {
              var t = n.name,
                c = (0, i.Z)(t),
                s = "/"
                  .concat((0, i.Z)(e.name), "/")
                  .concat(r, "/")
                  .concat(c);
              a.push({ active: !!o && c === o, isComplete: !1, link: s, name: t });
            }),
              d.push(c(c({}, s), {}, { sections: a }));
          }),
          u ? s[a] : { modulos: d, porcentaje: 0 }
        );
      };
    },
    3005: function (e, n, t) {
      "use strict";
      var o = t(7294);
      n.Z = function () {
        var e = (0, o.useState)(),
          n = e[0],
          t = e[1];
        return (
          (0, o.useEffect)(function () {
            var e = function () {
              var e = window.innerWidth < 992;
              t(e);
            };
            return (
              e(),
              window.addEventListener("resize", e),
              function () {
                window.removeEventListener("resize", e);
              }
            );
          }, []),
          n
        );
      };
    },
    8378: function (e, n, t) {
      "use strict";
      t.r(n),
        t.d(n, {
          __N_SSG: function () {
            return j;
          },
          default: function () {
            return w;
          },
        });
      var o = t(5893),
        i = t(6265),
        r = t(7294),
        c = (t(1664), t(3005)),
        s = function (e) {
          var n = e.width;
          return (0, o.jsx)("div", {
            className: "progress-bar",
            children: (0, o.jsx)("div", { style: { width: "".concat(n, "%") } }),
          });
        },
        a = function (e) {
          var n = e.width,
            t = n + 1;
          return (
            n >= 10 && (t = n - 15),
            (0, o.jsx)("div", {
              className: "progress-bar-menu",
              style: { width: "".concat(t, "%") },
            })
          );
        },
        u = t(1722),
        l = function (e) {
          var n = e.activeSection,
            t = e.changeActiveSection,
            i = e.index,
            s = e.item,
            a = e.modulo,
            l = e.showHideMenu,
            d = (0, r.useState)(),
            m = d[0],
            f = d[1],
            h = window.location.pathname,
            p = (0, u.Z)(s.name),
            v = (0, c.Z)(),
            g = (0, r.useCallback)(function (e) {
              if (null !== e) {
                e.style.height = "auto";
                var t = e.getBoundingClientRect().height,
                  o = n === p ? t : 0;
                f(t),
                  setTimeout(function () {
                    e.style.height = o;
                  }, 0);
              }
            }, []);
          return (
            (0, r.useEffect)(
              function () {
                a === (0, u.Z)(p) && t(p, !0);
              },
              [a]
            ),
            (0, o.jsxs)("li", {
              className: "mb-2",
              children: [
                (0, o.jsxs)("p", {
                  className: "d-flex align-items-center text-uppercase mb-0",
                  onClick: function () {
                    t(p);
                  },
                  children: [
                    (0, o.jsx)("span", {
                      className: a === (0, u.Z)(p) ? "num-list num-list-active" : "num-list",
                      children: i + 1,
                    }),
                    (0, o.jsx)("span", {
                      className:
                        a === (0, u.Z)(p) ? "name-section option-list-active" : "name-section",
                      children: s.name,
                    }),
                  ],
                }),
                (0, o.jsx)("ul", {
                  className: "menu-section-content",
                  ref: g,
                  style: { height: n === p ? m : 0 },
                  children: s.sections.map(function (e, n) {

                    var num = e.link.split('/')
                    var link = `../../${num[2]}/${num[3]}/index.html`;
                    return (0, o.jsxs)(
                      "li",
                      {
                        className: h === e.link ? "option-list-active" : "",
                        onClick: function () {
                          t(p, !1), v && l();
                        },
                        children: [
                          (0, o.jsx)("a", { href: link, children: e.name }),
                          e.isComplete && (0, o.jsx)("span", { className: "icon-check" }),
                        ],
                      },
                      "moduloMenu".concat(n)
                    );
                  }),
                }),
              ],
            })
          );
        },
        d = (0, r.forwardRef)(function (e, n) {
          var t = e.itemsMenu,
            i = e.heightMenu,
            c = e.modulo,
            s = e.showHideMenu,
            a = e.paddingMenu,
            u = (0, r.useState)(c),
            d = u[0],
            m = u[1],
            f = function (e, n) {
              var t = e;
              void 0 === n && (t = d !== e && e), m(t);
            };
          return (0, o.jsx)("ul", {
            className: "menu-section",
            ref: n,
            style: { height: i, padding: a },
            children: t.map(function (e, n) {
              return (0,
                o.jsx)(l, { activeSection: d, changeActiveSection: f, index: n, item: e, modulo: c, showHideMenu: s }, "itemMenu".concat(n));
            }),
          });
        }),
        m = (0, r.forwardRef)(function (e, n) {
          var t = e.heightMenu,
            i = e.infoMenu,
            r = e.isShowMenu,
            s = e.nombreCurso,
            u = e.modulo,
            l = e.showHideMenu,
            m = n.refMenu,
            f = n.refMenuMobileButton,
            h = (0, c.Z)();
          return (0,
            o.jsxs)(o.Fragment, { children: [h && (0, o.jsxs)("button", { className: "btn-menu-curso titulos-azul w-100 py-2 px-3 px-sm-0 d-flex justify-content-center", onClick: l, ref: f, children: [(0, o.jsx)("p", { className: "col-11 mb-0 mt-1", children: s }), (0, o.jsx)("div", { className: r ? "icono-down-active" : "icono-down" })] }), !h && (0, o.jsxs)(o.Fragment, { children: [(0, o.jsx)("p", { className: "titulos-azul", children: s }), (0, o.jsxs)("div", { className: "progress-menu", children: [(0, o.jsx)(a, { width: i.porcentaje }), (0, o.jsxs)("p", { className: "progress-num", children: [parseInt(i.porcentaje), "%"] })] })] }), (0, o.jsx)(d, { heightMenu: r ? t : 0, itemsMenu: i.modulos, modulo: u, paddingMenu: r && h ? "10px 20px" : 0, ref: m, showHideMenu: l })] });
        }),
        f = function (e) {
          var n = e.subtitulo,
            t = e.contenido,
            i = e.infoMenu,
            a = e.name,
            u = e.modulo,
            l = e.progressBarWidth,
            d = e.nombreCurso,
            f = e.itemsNavegacion,
            h = f.prevItem,
            p = f.nextItem,
            v = (0, c.Z)(),
            g = (0, r.useState)(),
            j = g[0],
            w = g[1],
            x = (0, r.useState)(),
            b = x[0],
            y = x[1],
            N = (0, r.useState)(),
            O = (N[0], N[1]),
            E = (0, r.useState)(0),
            M = E[0],
            S = E[1],
            Z = (0, r.useState)(!v),
            k = Z[0],
            C = Z[1],
            P = (0, r.useRef)(null),
            B = (0, r.useRef)(null);
          (0, r.useEffect)(
            function () {
              return (
                _(),
                window.addEventListener("scroll", _),
                window.addEventListener("resize", _),
                function () {
                  window.removeEventListener("scroll", _), window.removeEventListener("resize", _);
                }
              );
            },
            [P, v]
          ),
            (0, r.useEffect)(
              function () {
                var e = null !== B.current ? B.current.getBoundingClientRect().height : 0;
                C(!v), O(e);
              },
              [v]
            );
          var _ = function () {
            if (null !== P.current) {
              var e,
                n = document.getElementById("header").getBoundingClientRect().height,
                t = document.getElementById("footer").getBoundingClientRect().top,
                o = P.current.offsetTop,
                i =
                  (e = v
                    ? window.innerHeight - n
                    : t < window.innerHeight
                      ? window.innerHeight - (window.innerHeight - t) - n
                      : window.innerHeight - n) -
                  o -
                  30;
              w(e), S(n), y(i);
            }
          };
          if (!!p) {
            var num = p.link.split('/')
            var link = `../../${num[2]}/${num[3]}/index.html`;
          }
          if (!!h) {
            var num = h.link.split('/')
            var linkAnterior = `../../${num[2]}/${num[3]}/index.html`;
          }

          return (0, o.jsxs)(o.Fragment, {
            children: [
              (0, o.jsx)(s, { width: l }),
              (0, o.jsx)("div", {
                className: "main-content-curso",
                children: (0, o.jsxs)("div", {
                  className: "row mx-0",
                  children: [
                    (0, o.jsx)("div", {
                      className: "menu col-lg-3 px-0",
                      style: { height: k ? j : 0, top: M },
                      children: (0, o.jsx)(m, {
                        heightMenu: b,
                        infoMenu: i,
                        isShowMenu: k,
                        nombreCurso: d,
                        modulo: u,
                        ref: { refMenu: P, refMenuMobileButton: B },
                        showHideMenu: function (e) {
                          C("object" !== typeof e ? e : !k);
                        },
                      }),
                    }),
                    (0, o.jsx)("div", {
                      className: "contenedor-modulos-curso col-lg-9 pb-lg-5 ",
                      children: (0, o.jsxs)("div", {
                        children: [
                          null !== h &&
                          (0, o.jsx)("div", {
                            className: "d-flex align-items-center link-back",
                            children: (0, o.jsx)("a", { href: linkAnterior, children: h.name }),
                          }),
                          "Presentaci\xf3n" !== a
                            ? (0, o.jsx)("p", { className: "titulos-azul", children: a })
                            : (function () {
                              if (null !== p)
                                return (0, o.jsxs)("p", {
                                  className: "titulos-azul",
                                  children: [
                                    p.nombreModulo,
                                    (0, o.jsx)("br", {}),
                                    (0, o.jsx)("span", {
                                      className: "subtitulo-collage",
                                      children: n,
                                    }),
                                  ],
                                });
                            })(),
                          t(),
                          null === p &&
                          (0, o.jsx)(o.Fragment, {
                            children: (0, o.jsxs)("div", {
                              className: "d-sm-flex  align-items-center py-3 pt-lg-5 link-next",
                              children: [
                                (0, o.jsx)("p", {
                                  className: "mb-0 text-continuar",
                                  children: "Continuar con",
                                }),
                                (0, o.jsx)("a", {
                                  href: "../../../cursos/index.html",
                                  children: "Cursos virtuales",
                                }),
                              ],
                            }),
                          }),
                          null !== p &&
                          (0, o.jsx)(o.Fragment, {

                            children: (0, o.jsxs)("div", {
                              className: "d-sm-flex  align-items-center py-3 pt-lg-5 link-next",
                              children: [
                                "Presentaci\xf3n" !== a
                                  ? (0, o.jsx)("p", {
                                    className: "mb-0 text-continuar",
                                    children: "Continuar con",
                                  })
                                  : (0, o.jsx)("p", {
                                    className: "mb-0 text-continuar",
                                    children: "Empezar con",
                                  }),
                                (0, o.jsx)("a", { href: link, children: p.name }),
                              ],
                            }),
                          }),
                        ],
                      }),
                    }),
                  ],
                }),
              }),
            ],
          });
        },
        h = t(5552),
        p = t(305);
      function v(e, n) {
        var t = Object.keys(e);
        if (Object.getOwnPropertySymbols) {
          var o = Object.getOwnPropertySymbols(e);
          n &&
            (o = o.filter(function (n) {
              return Object.getOwnPropertyDescriptor(e, n).enumerable;
            })),
            t.push.apply(t, o);
        }
        return t;
      }
      function g(e) {
        for (var n = 1; n < arguments.length; n++) {
          var t = null != arguments[n] ? arguments[n] : {};
          n % 2
            ? v(Object(t), !0).forEach(function (n) {
              (0, i.Z)(e, n, t[n]);
            })
            : Object.getOwnPropertyDescriptors
              ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t))
              : v(Object(t)).forEach(function (n) {
                Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(t, n));
              });
        }
        return e;
      }
      var j = !0,
        w = function (e) {
          var n = e.curso,
            t = e.modulo,
            c = e.seccion,
            s = function (n, t) {
              return n.find(function (n) {
                return (0, u.Z)(n.name) === e[t];
              });
            },
            a = function (n, t) {
              return n.findIndex(function (n) {
                return (0, u.Z)(n.name) === e[t];
              });
            },
            l = s(h.Z, "curso"),
            d = s(l.contenido.modulos, "modulo"),
            m = s(d.contenido, "seccion"),
            v = (0, r.useState)(0),
            j = v[0],
            w = v[1],
            x = (0, r.useState)(),
            b = x[0],
            y = x[1],
            N = (0, r.useState)(!1),
            O = N[0],
            E = N[1],
            M = (0, r.useState)({}),
            S = M[0],
            Z = M[1],
            k = (0, r.useRef)(null);
          (0, r.useEffect)(function () {
            var e = localStorage.getItem("academyProgress");
            return (
              y((0, p.Z)(l, e, t, c)),
              E(!0),
              function () {
                window.removeEventListener("scroll", C), window.removeEventListener("resize", C);
              }
            );
          }, []),
            (0, r.useEffect)(
              function () {
                var e,
                  o,
                  i = l.contenido.modulos,
                  r = a(i, "modulo"),
                  c = a(d.contenido, "seccion");
                if (0 === c)
                  if (0 === r) e = null;
                  else {
                    var s = i[r - 1],
                      m = s.contenido[s.contenido.length - 1].name;
                    e = {
                      name: m,
                      link: "/"
                        .concat(n, "/")
                        .concat((0, u.Z)(s.name), "/")
                        .concat((0, u.Z)(m)),
                    };
                  }
                else {
                  var f = d.contenido[c - 1].name;
                  e = {
                    name: f,
                    link: "/"
                      .concat(n, "/")
                      .concat(t, "/")
                      .concat((0, u.Z)(f)),
                  };
                }
                if (c === d.contenido.length - 1)
                  if (r === i.length - 1) o = null;
                  else {
                    var h = i[r + 1],
                      p = h.contenido[0].name;
                    o = {
                      moduloFinalizado: !0,
                      nombreModulo: d.name,
                      name: h.name,
                      link: "/"
                        .concat(n, "/")
                        .concat((0, u.Z)(h.name), "/")
                        .concat((0, u.Z)(p)),
                    };
                  }
                else {
                  var v = d.contenido[c + 1].name;
                  o = {
                    moduloFinalizado: !1,
                    nombreModulo: d.name,
                    name: v,
                    link: "/"
                      .concat(n, "/")
                      .concat(t, "/")
                      .concat((0, u.Z)(v)),
                  };
                }
                Z({ prevItem: e, nextItem: o }),
                  w(0),
                  setTimeout(function () {
                    return (
                      O &&
                      (C(),
                        window.addEventListener("scroll", C),
                        window.addEventListener("resize", C)),
                      function () {
                        window.removeEventListener("scroll", C),
                          window.removeEventListener("resize", C);
                      }
                    );
                  }, 1e3);
              },
              [O, m, d]
            );
          var C = function () {
            if (null !== k.current) {
              var e,
                n = k.current.getBoundingClientRect().height,
                t = document.getElementsByTagName("html")[0].scrollTop,
                o = document.getElementsByTagName("header")[0].getBoundingClientRect().height,
                i = k.current.getBoundingClientRect().height + o - window.innerHeight;
              if (n > window.innerHeight) {
                var r = (100 * t) / i,
                  c = r >= 0 ? r : 100;
                e = c >= 100 ? 100 : c;
              } else e = 100;
              w(e), 100 === e && P();
            }
          },
            P = function () {
              var e,
                t,
                o = b.modulos.slice();
              o.forEach(function (n, t) {
                n.name === d.name && (e = t);
              });
              var r = o[e].sections;
              r.forEach(function (e, n) {
                e.name === m.name && (t = n);
              }),
                (r[t].isComplete = !0);
              var c = 0;
              r.forEach(function (e) {
                e.isComplete && (c += Math.ceil(100 / r.length));
              }),
                (o[e].porcentaje = c);
              var s = 0;
              o.forEach(function (e) {
                var n = 100 / o.length,
                  t = e.porcentaje >= 100 ? n : (e.porcentaje * n) / 100;
                s += t;
              });
              var a = g(
                g({}, b),
                {},
                {
                  porcentaje: s,
                  modulos: o,
                  seccionActiva: "/"
                    .concat(n, "/")
                    .concat((0, u.Z)(d.name), "/")
                    .concat((0, u.Z)(m.name)),
                }
              ),
                l = localStorage.getItem("academyProgress"),
                f = null !== l ? JSON.parse(l) : {};
              localStorage.setItem(
                "academyProgress",
                JSON.stringify(g(g({}, f), {}, (0, i.Z)({}, n, a)))
              ),
                y(a);
            };
          return (0, o.jsx)("div", {
            children:
              O &&
              (0, o.jsx)("div", {
                ref: k,
                children: (0, o.jsx)(f, {
                  contenido: m.contenido,
                  infoMenu: b,
                  itemsNavegacion: S,
                  modulo: t,
                  name: m.name,
                  nombreCurso: l.name,
                  progressBarWidth: j,
                  subtitulo: d.subtitulo,
                }),
              }),
          });
        };
    },
    3505: function (e, n, t) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "/[curso]/[modulo]/[seccion]",
        function () {
          return t(8378);
        },
      ]);
    },
    6265: function (e, n, t) {
      "use strict";
      function o(e, n, t) {
        return (
          n in e
            ? Object.defineProperty(e, n, {
              value: t,
              enumerable: !0,
              configurable: !0,
              writable: !0,
            })
            : (e[n] = t),
          e
        );
      }
      t.d(n, {
        Z: function () {
          return o;
        },
      });
    },
  },
  function (e) {
    e.O(0, [774, 246, 433, 707, 2], function () {
      return (n = 3505), e((e.s = n));
      var n;
    });
    var n = e.O();
    _N_E = n;
  },
]);
