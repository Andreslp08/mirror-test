(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [17],
  {
    305: function (e, s, n) {
      "use strict";
      var c = n(6265),
        i = n(1722);
      function r(e, s) {
        var n = Object.keys(e);
        if (Object.getOwnPropertySymbols) {
          var c = Object.getOwnPropertySymbols(e);
          s &&
            (c = c.filter(function (s) {
              return Object.getOwnPropertyDescriptor(e, s).enumerable;
            })),
            n.push.apply(n, c);
        }
        return n;
      }
      function t(e) {
        for (var s = 1; s < arguments.length; s++) {
          var n = null != arguments[s] ? arguments[s] : {};
          s % 2
            ? r(Object(n), !0).forEach(function (s) {
                (0, c.Z)(e, s, n[s]);
              })
            : Object.getOwnPropertyDescriptors
            ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n))
            : r(Object(n)).forEach(function (s) {
                Object.defineProperty(e, s, Object.getOwnPropertyDescriptor(n, s));
              });
        }
        return e;
      }
      s.Z = function (e, s, n, c) {
        var r,
          a = null !== s ? JSON.parse(s) : null,
          l = (0, i.Z)(e.name),
          o = null !== a && a[l],
          u = null === (r = e.contenido) || void 0 === r ? void 0 : r.modulos,
          d = [];
        return (
          void 0 !== u &&
            u.map(function (s) {
              var r = (0, i.Z)(s.name),
                a = { name: s.name, subtitulo: s.subtitulo, active: !!n && r === n, porcentaje: 0 },
                l = [];
              s.contenido.map(function (s) {
                var n = s.name,
                  t = (0, i.Z)(n),
                  a = "/"
                    .concat((0, i.Z)(e.name), "/")
                    .concat(r, "/")
                    .concat(t);
                l.push({ active: !!c && t === c, isComplete: !1, link: a, name: n });
              }),
                d.push(t(t({}, a), {}, { sections: l }));
            }),
          o ? a[l] : { modulos: d, porcentaje: 0 }
        );
      };
    },
    8684: function (e, s, n) {
      "use strict";
      n.r(s),
        n.d(s, {
          __N_SSG: function () {
            return u;
          },
          default: function () {
            return d;
          },
        });
      var c = n(5893),
        i = n(7294),
        r = n(1722),
        t = n(1558),
        a = function (e) {
          var s = e.curso,
            n = e.infoMenu,
            a = s.name,
            l = s.contenido,
            o = l.autor,
            u = l.arroba,
            d = l.duracion,
            m = l.dirigido,
            h = l.categoria,
            f = l.descripcion,
            j = l.video,
            g = l.habilidades,
            p = l.modulos,
            x = l.pie_imagen_titulo,
            v = l.pie_imagen_cuerpo,
            b = l.testimonios,
            N = (p.subtitulo, (0, i.useState)(n.modulos[0].sections[0].link)),
            N2 = N[0].split('/'),
            _ = N[0].search('index.html') !== -1 ? `./${N2[1]}/${N2[2]}/${N2[3]}` : `./${N2[2]}/${N2[3]}/index.html`,
            w = N[1];
          
          return (
            (0, i.useEffect)(
              function () {
                var e = (0, r.Z)(a),
                  s = JSON.parse(window.localStorage.getItem("academyProgress"));
                if (null !== s) {
                  var n = void 0 !== s[e] ? s[e].seccionActiva : _;
                  w(n);
                }
              },
              [s]
            ),
            (0, c.jsxs)(c.Fragment, {
              children: [
                (0, c.jsxs)("div", {
                  className: "main-content ficha",
                  children: [
                    (0, c.jsx)("h1", { className: "titulos-azul", children: a }),
                    (0, c.jsxs)("p", { className: "categoria", children: [h, " "] }),
                    (0, c.jsxs)("div", {
                      className: "d-lg-flex justify-content-between mb-3",
                      children: [
                        (0, c.jsx)("div", {
                          className: "col-lg-3 order-lg-1",
                          children: (0, c.jsx)("ul", {
                            className: "info",
                            children: d.map(function (e) {
                              return (0, c.jsx)("li", { children: e }, e);
                            }),
                          }),
                        }),
                        (0, c.jsx)("div", {
                          className: "col-lg-7 order-lg-0",
                          children: (0, c.jsx)("p", { children: f }),
                        }),
                      ],
                    }),
                    (0, c.jsx)("iframe", {
                      src: j,
                      className: "video-ficha",
                      controls: "controls",
                      autoplay: "false",
                    }),
                    (0, c.jsxs)("div", {
                      className: "row justify-content-between mt-lg-4",
                      children: [
                        (0, c.jsx)("div", {
                          className: "col-lg-6 mb-3 mb-lg-0",
                          children: (0, c.jsx)("a", {
                            href: _,
                            className: "btn-azul",
                            children: "Comenzar curso",
                          }),
                        }),
                        (0, c.jsx)("div", {
                          className: "col-lg-6",
                          children: (0, c.jsx)("a", {
                            href: "./index.html",
                            className: "btn-azul",
                            children: "Comprar curso",
                          }),
                        }),
                      ],
                    }),
                    (0, c.jsxs)("div", {
                      className: "row mt-4",
                      children: [
                        (0, c.jsxs)("div", {
                          className: "col-lg-6",
                          children: [
                            (0, c.jsx)("h3", {
                              className: "subtitulo-ficha",
                              children: "Contenidos",
                            }),
                            (0, c.jsx)("ul", {
                              className: "modulos",
                              children: n.modulos.map(function (e, s) {
                                return (0,
                                c.jsxs)("li", { children: ["M\xf3dulo ", s + 1, ". ", e.name, (0, c.jsx)("p", { children: e.subtitulo })] }, "item".concat(s));
                              }),
                            }),
                          ],
                        }),
                        (0, c.jsxs)("div", {
                          className: "col-lg-6",
                          children: [
                            (0, c.jsx)("h3", {
                              className: "subtitulo-ficha text-lg-end",
                              children: "Habilidades que obtendr\xe1s",
                            }),
                            (0, c.jsx)("ul", {
                              className: "list-ficha",
                              children: g.map(function (e) {
                                return (0, c.jsx)("li", { children: e }, e);
                              }),
                            }),
                          ],
                        }),
                      ],
                    }),
                    (0, c.jsxs)("div", {
                      className: "row mt-lg-4 mb-5",
                      children: [
                        (0, c.jsxs)("div", {
                          className: "col-lg-6  order-lg-2",
                          children: [
                            (0, c.jsx)("h3", {
                              className: "subtitulo-ficha",
                              children: "A quien va dirigido",
                            }),
                            (0, c.jsx)("p", { className: "text-ficha", children: m }),
                          ],
                        }),
                        (0, c.jsxs)("div", {
                          className: "col-lg-6 order-lg-3",
                          children: [
                            (0, c.jsx)("img", {
                              src: "../images/".concat((0, r.Z)(a), "2.jpg"),
                              className: "w-100 mb-4 mb-lg-0",
                            }),
                            (0, c.jsxs)("p", {
                              className: "pie_imagen_texto",
                              children: [
                                (0, c.jsxs)("span", {
                                  className: "pie_imagen_titulo",
                                  children: [x, " "],
                                }),
                                v,
                              ],
                            }),
                          ],
                        }),
                        (0, c.jsxs)("div", {
                          className: "col-lg-6 order-lg-0",
                          children: [
                            (0, c.jsx)("h3", {
                              className: "subtitulo-ficha",
                              children: "Perfil del artista",
                            }),
                            (0, c.jsx)("img", {
                              src: "../images/".concat((0, r.Z)(a), ".jpg"),
                              className: "w-100 mb-4",
                            }),
                          ],
                        }),
                        (0, c.jsx)("div", {
                          className: "col-lg-6 order-lg-1",
                          children: (0, c.jsxs)("div", {
                            className: "pr-4 text-lg-end pt-lg-5",
                            children: [
                              (0, c.jsx)("p", { children: o }),
                              (0, c.jsx)("p", { children: u }),
                            ],
                          }),
                        }),
                      ],
                    }),
                  ],
                }),
                !!b && (0, c.jsx)(t.Z, { testimonios: b }),
              ],
            })
          );
        },
        l = n(5552),
        o = n(305),
        u = !0,
        d = function (e) {
          var s = e.curso,
            n = (0, i.useState)({}),
            t = n[0],
            u = n[1],
            d = (0, i.useState)(!1),
            m = d[0],
            h = d[1],
            f = (0, i.useState)(),
            j = f[0],
            g = f[1];
          return (
            (0, i.useEffect)(
              function () {
                var e = l.Z.find(function (e) {
                  return (0, r.Z)(e.name) === s;
                });
                u(e);
                var n = localStorage.getItem("academyProgress");
                g((0, o.Z)(e, n)), h(!0);
              },
              [s]
            ),
            (0, c.jsx)("section", { children: m && (0, c.jsx)(a, { curso: t, infoMenu: j }) })
          );
        };
    },
    5626: function (e, s, n) {
      (window.__NEXT_P = window.__NEXT_P || []).push([
        "/[curso]",
        function () {
          return n(8684);
        },
      ]);
    },
    6265: function (e, s, n) {
      "use strict";
      function c(e, s, n) {
        return (
          s in e
            ? Object.defineProperty(e, s, {
                value: n,
                enumerable: !0,
                configurable: !0,
                writable: !0,
              })
            : (e[s] = n),
          e
        );
      }
      n.d(s, {
        Z: function () {
          return c;
        },
      });
    },
  },
  function (e) {
    e.O(0, [774, 246, 433, 707, 2], function () {
      return (s = 5626), e((e.s = s));
      var s;
    });
    var s = e.O();
    _N_E = s;
  },
]);
