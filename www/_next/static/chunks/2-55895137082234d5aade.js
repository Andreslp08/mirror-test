(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
  [2, 179], {
    2642: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(892),
        r = o.n(n);
      o(4567);
      a.Z = function (e) {
        var a = e.activeImage,
          o = e.closeImageLightBox,
          n = e.imagenes,
          c = (0, i.useState)(a),
          l = c[0],
          t = c[1],
          d = (0, i.useState)(58),
          u = d[0],
          m = d[1],
          p = function () {
            var e = window.document.getElementsByClassName("ril-caption ril__caption")[0];
            e && m(parseFloat(e.offsetHeight) + 8)
          };
        return (0, i.useEffect)((function () {
          p()
        }), [l]), (0, s.jsx)(s.Fragment, {
          children: (0, s.jsx)(r(), {
            enableZoom: !0,
            imageCaption: n[l].caption,
            imagePadding: u,
            mainSrc: n[l].src,
            nextSrc: n[(l + 1) % n.length].src,
            prevSrc: n[(l + n.length - 1) % n.length].src,
            onCloseRequest: function () {
              return o()
            },
            onAfterOpen: function () {
              return p()
            },
            onMoveNextRequest: function () {
              t((l + 1) % n.length)
            },
            onMovePrevRequest: function () {
              t((l + n.length - 1) % n.length)
            }
          })
        })
      }
    },
    6902: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/cuerpos-desobedientes/modulo1/conceptos/selfie-1.jpg",
          caption: "\u201cSelfie durante el festival Holi.\u201d Fuente: Wikimedia Commons bajo Licencia C.C. BY 4.0"
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/conceptos/performatividad.jpg",
          caption: "\u201cOperaria labra piezas para aviones en un torno rev\xf3lver en una f\xe1brica de Consolidated Aircraft Corporation en Fort Worth\u201d, Texas, Estados Unidos. 1942. Fuente: Wikimedia Commons bajo dominio p\xfablico. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/conceptos/we-can-do-it.jpg",
          caption: "\u201cPodemos hacerlo\u201d. J. Howard Miller. Fuente: Wikimedia Commons bajo dominio p\xfablico. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/conceptos/selfie-2.jpg",
          caption: "\u201cSelfie stick silhouette\u201d. Fuente: Wikimedia Commons bajo dominio p\xfablico. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/conceptos/pose.jpg",
          caption: "\u201cModel posing outdoor\u201d. Vladimir Pustovit. Fuente: Wikimedia Commons bajo licencia C.C. B"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-5-conceptos",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-uno-conceptos",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Autorepresentaci\xf3n:"
                  }), (0, s.jsx)("p", {
                    children: "Una forma de representaci\xf3n de la identidad propia en la que se confrontan los estereotipos"
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-dos-conceptos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Performatividad de g\xe9nero: "
                  }), (0, s.jsx)("p", {
                    children: " Es entender el g\xe9nero como una construcci\xf3n social en funci\xf3n de las normas sociales."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-tres-conceptos",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Estereotipos:"
                  }), (0, s.jsx)("p", {
                    children: "Percepci\xf3n exagerada y simplificada que se tiene sobre una persona o grupo que comparten ciertas caracter\xedsticas, cualidades y habilidades."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cuatro-conceptos",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Selfie:"
                  }), (0, s.jsx)("p", {
                    children: "Autorretrato realizado con un tel\xe9fono celular con el objetivo de publicarlo en redes sociales."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("3")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cinco-conceptos",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Pose:"
                  }), (0, s.jsx)("p", {
                    children: "Postura o posici\xf3n en la que se coloca una persona que va a ser fotografiada, retratada o pintada por otra."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("4")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    5853: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = (o(2004), o(2642));
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/cuerpos-desobedientes/modulo1/ejercicios/ejercicio-valentina.gif",
          caption: ""
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/ejercicios/el-hombre-dehoy.jpg",
          caption: " "
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/ejercicios/jennifer-video.gif",
          caption: ""
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/ejercicios/paola-ejercicio.gif",
          caption: ""
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = (e[0], e[1], (0, i.useState)(!1)),
            o = a[0],
            c = a[1],
            l = (0, i.useState)(null),
            t = l[0],
            d = l[1],
            u = function (e) {
              c(!o), d(void 0 !== e ? e : null)
            },
            m = function (e) {
              var a;
              for (var o in r) o === e && (a = r[o].src);
              return a
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Escoge una foto publicitaria en la que veas estereotipos de g\xe9nero. "
              }), (0, s.jsx)("li", {
                children: "T\xf3mate una foto imitando la pose."
              }), (0, s.jsx)("li", {
                children: "Realiza una serie de fotos en la que esa pose se transforme hacia un resultado inesperado. Puedes a\xf1adir texto, audio, usar efectos o emoticones sobre las fotos."
              }), (0, s.jsx)("li", {
                children: "Organiza las fotos como una secuencia: primero la foto de referencia, despu\xe9s tu copia de la pose y luego la transformaci\xf3n de la pose."
              }), (0, s.jsx)("li", {
                children: "S\xfabelas a instagram o tik tok como una secuencia."
              })]
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: Fotos de referencia o im\xe1genes de revistas, c\xe1mara de celular, de tablet, de computador o fotogr\xe1fica"
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-4",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  u("0")
                },
                style: {
                  backgroundImage: "url(".concat(m("0"))
                },
                children: (0, s.jsx)("p", {
                  children: (0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Valentina Bernal"
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-dos",
                onClick: function () {
                  u("1")
                },
                style: {
                  backgroundImage: "url(".concat(m("1"))
                },
                children: (0, s.jsx)("p", {
                  children: (0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "John G\xf3mez"
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-tres-jenn",
                onClick: function () {
                  u("2")
                },
                style: {
                  backgroundImage: "url(".concat(m("2"))
                },
                children: (0, s.jsx)("p", {
                  children: (0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Jennifer Alcalde"
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-cuatro-paola",
                onClick: function () {
                  u("3")
                },
                style: {
                  backgroundImage: "url(".concat(m("3"))
                },
                children: (0, s.jsx)("p", {
                  children: (0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Paola Salamanca"
                  })
                })
              }), o && (0, s.jsx)(n.Z, {
                activeImage: t,
                imagenes: r,
                closeImageLightBox: u
              })]
            })]
          })
        }
    },
    6925: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Autopresentaci\xf3n",
        subtitulo: "Fotoperformance a partir de una selfie",
        contenido: [o(7924).Z, o(2776).Z, o(5258).Z, o(5853).Z, o(6902).Z, o(9722).Z, o(814).Z]
      }
    },
    7924: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo aprender\xe1s sobre la estrategia de \u2018autorrepresentaci\xf3n\u2019 como una respuesta a los estereotipos de belleza. Analizar\xe1s la influencia de lo fotogr\xe1fico y los medios en la identidad de g\xe9nero. Realizar\xe1s un ejercicio de autorrepresentaci\xf3n por medio del fotoperformance inspirado en el an\xe1lisis de las im\xe1genes en redes sociales, como la selfie, con el fin de generar preguntas en torno a la identidad como \xbfQui\xe9n soy yo? \xbfQu\xe9 es ser un chico? \xbfQu\xe9 es ser una chica? para enfrentar las presiones sobre el cuerpo que en la edad escolar y a lo largo de la vida son muy fuertes. "
            }), (0, s.jsx)("img", {
              src: "/images/cuerpos-desobedientes/banner_mod1.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    814: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/autorepresentacion-clase-teorica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/autorepresentacion-clase-practica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/autorepresentacion-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n con artista invitada Amber Bemak"
            })]
          })
        }
      }
    },
    9722: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/cuerpos-desobedientes/modulo1/referentes/guerrilla.jpg",
          caption: "Guerrilla Girls Do Women Have To Be Naked  To Get Into the Met. Museum? 1989.  Visita la obra en este enlace: bit.ly/3o9vYMT"
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/referentes/1642.jpg",
          caption: "Dora Franco Autorretrato. 1987. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/referentes/lamassone.jpg",
          caption: "Karen Lamassone A Morales, Serie 24 dibujos por segundo Visita la obra en este enlace: bit.ly/3uoj8M4"
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/referentes/cindy.jpg",
          caption: "Cindy Sherman Untitled #92 (Centerfold) 1981 Visita la obra en este enlace: mo.ma/3ojStyG"
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/referentes/debora.png",
          caption: "D\xe9bora Arango Adolescencia 1944-1948 Visita la obra en este enlace: bit.ly/3o9vYMT"
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/referentes/francesca.jpg",
          caption: "Francesca Morosi 500,000 Young Girls asking: Am I Pretty or Ugly? Visita la obra en este enlace: bit.ly/3AL7gpO"
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/referentes/royal.jpg",
          caption: "Royal Osiris Karaoke Ensemble - The Art of Luv (Part 1): Elliot. 2016 Visita la obra en este enlace: bit.ly/2Y7ISAp"
        }, {
          src: "/images/cuerpos-desobedientes/modulo1/referentes/florencia.png",
          caption: "Florencia Aliberti 1986.Argentina. Auto-exposiciones Watch me Shrink.2015. Visita la obra en este enlace: bit.ly/3CSf0Xs"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-referentes-8",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-referentes-8-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Do Women Have To Be Naked  To Get Into the Met. Museum?,"
                    }), " 1989.", (0, s.jsx)("br", {}), "Guerrilla Girls."]
                  }), "          "]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-referentes-8-dos",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "A Morales, Serie 24 dibujos por segundo,"
                    }), " 1982.", (0, s.jsx)("br", {}), " Karen Lamassone.", (0, s.jsx)("br", {}), " Fuente Colecci\xf3n MAMBO.", (0, s.jsx)("br", {})]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-referentes-8-tres",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Untitled #92 (Centerfold),"
                    }), "1981.", (0, s.jsx)("br", {}), "Cindy Sherman.", (0, s.jsx)("br", {}), "Fuente: Museo de Arte Moderno de Nueva York, MOMA."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-referentes-8-cuatro",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Autorretrato, "
                    }), "1987.", (0, s.jsx)("br", {}), "Dora Franco.", (0, s.jsx)("br", {}), " Fuente Colecci\xf3n MAMBO."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-referentes-8-cinco",
                onClick: function () {
                  d("5")
                },
                style: {
                  backgroundImage: "url(".concat(u("5"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: '500,000 Young Girls asking "Am I Pretty or Ugly?",'
                    }), " 2014.", (0, s.jsx)("br", {}), "Francesca Morosi.", (0, s.jsx)("br", {})]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-referentes-8-seis",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Adolescencia, "
                    }), "1944-1948.", (0, s.jsx)("br", {}), "D\xe9bora Arango.", (0, s.jsx)("br", {}), " Fuente: Museo de Arte Moderno de Medell\xedn."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-referentes-8-siete",
                onClick: function () {
                  d("6")
                },
                style: {
                  backgroundImage: "url(".concat(u("6"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "The Art of Luv (Part 1): Elliot, "
                    }), "2016.", (0, s.jsx)("br", {}), "Royal Osiris Karaoke Ensemble.", (0, s.jsx)("br", {})]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-referentes-8-ocho",
                onClick: function () {
                  d("7")
                },
                style: {
                  backgroundImage: "url(".concat(u("7"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Auto-exposiciones,"
                    }), " 2015.", (0, s.jsx)("br", {}), "Florencia Aliberti.", (0, s.jsx)("br", {})]
                  })
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    5258: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "A partir de los referentes investigar\xe1s y seleccionar\xe1s una o varias fotos de los medios de comunicaci\xf3n y las redes sociales en las que se observen estereotipos de masculinidad y de feminidad. A partir de esta selecci\xf3n, realizar\xe1s un fotoperformance a partir de esa imagen en donde se realice una variaci\xf3n de la pose con el fin de generar nuevas ideas sobre el cuerpo, la imagen y las redes sociales. "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/autorepresentacion-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    2776: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "Entender\xe1s c\xf3mo la autorrepresentaci\xf3n de lo femenino ha sido una estrategia art\xedstica para cuestionar la mirada masculina y los sistemas de representaci\xf3n que naturalizan las actitudes de la cultura machista. Realizar\xe1s un an\xe1lisis de la manera como las artistas cuestionan las ideas de lo femenino y lo masculino en relaci\xf3n con la imagen y los estereotipos. "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/autorepresentacion-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    9265: function (e, a, o) {
      "use strict";
      o(1558), o(1664);
      a.Z = {
        name: "Cuerpos desobedientes: G\xe9nero y performance",
        duracion: ["4 m\xf3dulos", "8 sesiones", "12 horas - 14 horas", "4 ejercicios de creaci\xf3n", "8 videos", "En Espa\xf1ol"],
        autor: "Nadia Granados, Maestra en Artes pl\xe1sticas Universidad Nacional de Colombia 2000. Mag\xedster en Artes Visuales UNAM 2020.  Reconocida internacionalmente como una de las figuras m\xe1s destacadas de la escena del performance transfeminista latinoamericano. Su pr\xe1ctica art\xedstica plantea preguntas a las estrategias de manipulaci\xf3n que existen detr\xe1s de diferentes  sistemas de  representaci\xf3n de los mass media haciendo una cr\xedtica directa a estas estructuras de poder simb\xf3lico. En varios de sus proyectos, ha encarnado una feminidad  inspirada en los estereotipos sociales asignados a la mujer latina sexualmente provocativa, buscando quebrar los sistemas de representaci\xf3n de la sensualidad, por medio del performance, cine experimental, multimedia, web art y cabaret.",
        arroba: "",
        categoria: "Performance",
        descripcion: "Este curso te invita a investigar, crear y reflexionar en torno a la representaci\xf3n del g\xe9nero en la historia del arte y los medios de comunicaci\xf3n. Incentivar\xe1 en ti la capacidad para analizar la construcci\xf3n de g\xe9nero. En este curso producido por el Museo de Arte Moderno de Bogot\xe1, la artista Nadia Granados te guiar\xe1 a trav\xe9s de diferentes t\xe9cnicas de performance y arte multimedia al tiempo que investigas y realizas ejercicios de creaci\xf3n para identificar problem\xe1ticas asociadas con el cuerpo y transformarlas desde el arte. ",
        video: "../video/cuerpos-desobedientes-portada.mp4",
        habilidades: ["Expresi\xf3n corporal", "Creaci\xf3n audiovisual", "T\xe9cnicas de performance", "Creaci\xf3n de personajes", "An\xe1lisis de los roles de g\xe9nero", "Herramientas de transformaci\xf3n social desde el arte", "Lectura de im\xe1genes"],
        dirigido: "Este curso est\xe1 dirigido a adolescentes, j\xf3venes y adultos interesados en el performance, el video, la fotograf\xeda y los estudios de g\xe9nero. No se requiere ser artista o conocedor de las t\xe9cnicas de teatro, performance o video. Recomendado para estudiantes de bachillerato con intereses en el teatro, danza, performance y m\xfasica, docentes, artistas y p\xfablico en general interesado en aprender y desarrollar t\xe9cnicas de investigaci\xf3n y creaci\xf3n en torno al cuerpo. ",
        pie_imagen_titulo: "El martirio de San Sebasti\xe1n,",
        pie_imagen_cuerpo: "1980. \xc1lvaro Barrios. Fuente: Colecci\xf3n MAMBO",
        modulos: [o(6925).Z, o(4714).Z, o(4375).Z, o(7179).Z],
        testimonios: ["En \u2018Cuerpos Desobedientes\u2019 aprend\xed todo sobre el performance.\u201d", "\u201cAprend\xed la posibilidad de expresarme a trav\xe9s del cuerpo gracias a las t\xe9cnicas que nos compart\xeda.", "\u201cAprend\xed que el performer es autor de su obra y tambi\xe9n es el actor. Aprend\xed a usar la c\xe1mara en el video performance y la interacci\xf3n con archivos audiovisuales en tiempo real", "\u201cDescubr\xed el potencial del cuerpo como herramienta a trav\xe9s de la pr\xe1ctica, m\xe1s all\xe1 del marco te\xf3rico.\u201d", "\u201cExplor\xe9 mi propia performatividad.\u201d", "\u201cPerd\xed el miedo al momento de abordar el cuerpo como canal de comunicaci\xf3n y me arriesgu\xe9 a explorar otro camino en el arte\u201d", "\u201cFue una experiencia formadora, interesante e inspiradora\u201d"]
      }
    },
    5328: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/cuerpos-desobedientes/modulo2/conceptos/deconstruccion.jpg",
          caption: "Jacques Derrid\xe1, fil\xf3sofo creador del concepto \u2018deconstrucci\xf3n\u2019. Fuente: Wikimedia Commons bajo licencia C.C. BY 3.0"
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/conceptos/resignificacion.jpg",
          caption: "Las tejedoras de Mampuj\xe1n, son un grupo de mujeres v\xedctimas del conflicto armado quienes han resignificado los episodios violentos a trav\xe9s del tejido de tapices que narran su historia. Fuente: El Nuevo Siglo. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/conceptos/violencia.jpg",
          caption: "\u201cMujer maltratada con un Bast\xf3n\u201d. Francisco Goya (1796). Fuente: Wikimedia Commons bajo dominio p\xfablico. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/conceptos/gramatica.jpg",
          caption: "\u201c\xbfDeben las mujeres desnudarse para entrar al museo?\u201d Guerrilla Girls (1985). \xc9ste fue uno de los colectivos que utiliz\xf3 la comunicaci\xf3n para subvertir la gram\xe1tica cultural. Fuente: Wikimedia Commons bajo licencia C.C. BY SA 2.0"
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/conceptos/normalizar.jpg",
          caption: "\u201cRetrato de Michel Foucault\u201d (1926-1984) fil\xf3sofo franc\xe9s que explic\xf3 ampliamente el t\xe9rmino "
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-5-conceptos",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-uno",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Violencia dom\xe9stica:"
                  }), (0, s.jsx)("p", {
                    children: "Es un patr\xf3n de conducta amenazante o violenta, combinado con otros tipos de maltrato como el sicol\xf3gico, verbal o sexual que se usan para dominar a otra persona en el hogar o el n\xfacleo familiar.\xa0"
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Resignificaci\xf3n:"
                  }), (0, s.jsx)("p", {
                    children: "Darle un nuevo significado a un acontecimiento o a una conducta, esto quiere decir que se le otorga un valor o un sentido diferente a algo."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-tres",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Deconstrucci\xf3n:"
                  }), (0, s.jsx)("p", {
                    children: "Proceso en el que se revisa la estructura de algo para evidenciar sus caracter\xedsticas, ambig\xfcedades, fallas o contradicciones."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cuatro",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Gram\xe1tica cultural:"
                  }), (0, s.jsx)("p", {
                    children: "Normas, reglas y convenciones que regulan las interacciones, las relaciones sociales y las representaciones de objetos, espacios e im\xe1genes. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("3")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cinco",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Normalizar:"
                  }), (0, s.jsx)("p", {
                    children: "Identificar como natural o com\xfan un comportamiento que no necesariamente debe serlo."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("4")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    6867: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2004),
        r = o(2642);
      o(4567);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(l, {})
        }
      };
      var c = [{
          src: "/images/cuerpos-desobedientes/modulo2/ejercicios/milena.png",
          caption: ""
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/ejercicios/lorena.png",
          caption: ""
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/ejercicios/narvaez.png",
          caption: ""
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/ejercicios/narvaez.png",
          caption: ""
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/ejercicios/narvaez.png",
          caption: ""
        }],
        l = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            l = (0, i.useState)(!1),
            t = l[0],
            d = l[1],
            u = (0, i.useState)(null),
            m = u[0],
            p = u[1],
            g = function (e) {
              d(!t), p(void 0 !== e ? e : null)
            },
            h = function (e) {
              var a;
              for (var o in c) o === e && (a = c[o].src);
              return a
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Escoge 5 objetos de tu casa."
              }), (0, s.jsx)("li", {
                children: "Haz una lista de acciones o de verbos que puedan realizarse con cada objeto (Estos verbos deben remitir a acciones f\xedsicas)."
              }), (0, s.jsx)("li", {
                children: "Asocia el verbo con un material u objeto. "
              }), (0, s.jsx)("li", {
                children: "Selecciona un objeto o varios y desarrolla una acci\xf3n en la que se resignifique el uso de ese objeto."
              }), (0, s.jsx)("li", {
                children: "Registra la acci\xf3n en video y s\xfabelas a Instagram o Tik Tok."
              })]
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: Objetos cotidianos, papel y l\xe1piz, c\xe1mara de celular, de tablet, de computador o fotogr\xe1fica."
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-5-2 ",
              children: [(0, s.jsxs)("div", {
                className: "modulo-cover-item galeria-5-2-uno px-0",
                children: [(0, s.jsx)(n.Z, {
                  playsinline: !0,
                  repeat: !1,
                  paused: !0,
                  url: "../../../video/valentina.mp4",
                  height: "100%",
                  width: "100%",
                  controls: !0,
                  style: {
                    transition: "all 1s"
                  },
                  light: "/images/cuerpos-desobedientes/modulo2/ejercicios/valentina.png",
                  config: {
                    file: {
                      attributes: {
                        controlsList: "play nodownload"
                      }
                    }
                  }
                }), (0, s.jsx)("p", {
                  children: (0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Valentina Bernal"
                  })
                })]
              }), (0, s.jsxs)("div", {
                className: "modulo-cover-item galeria-5-2-dos px-0",
                children: [(0, s.jsx)(n.Z, {
                  playsinline: !0,
                  url: "../../../video/daniela.mp4",
                  width: "100%",
                  height: "100%",
                  controls: !0,
                  style: {
                    transition: "all 1s"
                  },
                  onEnded: function () {
                    o(!a)
                  },
                  light: "/images/cuerpos-desobedientes/modulo2/ejercicios/licuadora.png",
                  config: {
                    file: {
                      attributes: {
                        controlsList: "play nodownload"
                      }
                    }
                  }
                }), (0, s.jsx)("p", {
                  children: (0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Daniela Narvaez"
                  })
                })]
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-2-tres-crea",
                onClick: function () {
                  g("0")
                },
                style: {
                  backgroundImage: "url(".concat(h("0"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsx)("p", {
                    children: (0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Milena D\xedaz"
                    })
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-2-cuatro",
                onClick: function () {
                  g("1")
                },
                style: {
                  backgroundImage: "url(".concat(h("1"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsx)("p", {
                    children: (0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Lorena Luksny"
                    })
                  })
                })
              }), (0, s.jsxs)("div", {
                className: "modulo-cover-item galeria-5-2-cinco px-0",
                children: [(0, s.jsx)(n.Z, {
                  playsinline: !0,
                  url: "../../../video/inshot.mp4",
                  width: "100%",
                  height: "100%",
                  controls: !0,
                  style: {
                    transition: "all 1s"
                  },
                  onEnded: function () {
                    o(!a)
                  },
                  light: "/images/cuerpos-desobedientes/modulo2/ejercicios/narvaez.png",
                  config: {
                    file: {
                      attributes: {
                        controlsList: "play nodownload"
                      }
                    }
                  }
                }), (0, s.jsx)("p", {
                  children: (0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Daniela Narvaez"
                  })
                })]
              }), t && (0, s.jsx)(r.Z, {
                activeImage: m,
                imagenes: c,
                closeImageLightBox: g
              })]
            })]
          })
        }
    },
    4714: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Lo personal es pol\xedtico",
        subtitulo: "Videoperformance con objetos",
        contenido: [o(9773).Z, o(8143).Z, o(1638).Z, o(6867).Z, o(5328).Z, o(3130).Z, o(7731).Z]
      }
    },
    9773: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo se abordar\xe1 la consigna del movimiento de liberaci\xf3n de las mujeres \u201clo personal es pol\xedtico\u201d entendiendo que aquello que ocurre en la casa, lo que a veces da miedo o verg\xfcenza hablar porque lastima u oprime, es importante discutirlo en el \xe1mbito p\xfablico. Desde la construcci\xf3n de roles de g\xe9nero estandarizados que exigen comportamientos t\xf3xicos, hasta las violencias sexuales perpetradas en la intimidad del hogar, revisar\xe1s estas problem\xe1ticas a la luz de la visi\xf3n cr\xedtica que ofrece el arte feminista. En este m\xf3dulo realizar\xe1s un video performance con objetos. "
            }), (0, s.jsx)("img", {
              src: "/images/cuerpos-desobedientes/banner_mod2.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    7731: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/politico-clase-teorica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/politico-clase-practica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/politico-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n en l\xednea con artista invitado Edi Jimenez"
            })]
          })
        }
      }
    },
    3130: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/cuerpos-desobedientes/modulo2/referentes/calor.jpg",
          caption: " Calor de hogar, 1983. Maria Teresa Cano. Fuente: Museo de Arte Moderno de Medell\xedn"
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/referentes/cuja.jpg",
          caption: "Cuja, 1972. Feliza Bursztyn. Fuente: Colecci\xf3n MAMBO."
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/referentes/semiotics.jpg",
          caption: "Semiotics of the Kitchen, 1974. Martha Rosler. Visita la obra en este enlace: bit.ly/2Y3Ols6"
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/referentes/tendedero.jpg",
          caption: "El Tendedero, 978-2020 Monica Meyer. Fuente: Museo Univesitario de Arte Contempor\xe1neo MUAC. Visita la obra en este enlace: bit.ly/3zFKWfO"
        }, {
          src: "/images/cuerpos-desobedientes/modulo2/referentes/mientras.jpg",
          caption: "Mientras dorm\xedamos, 2004. Lorena Wolffer. Visita la obra en este enlace: bit.ly/2ZFVW0B"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-5",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: " Calor de hogar, "
                  }), " 1983.", (0, s.jsx)("br", {}), " Maria Teresa Cano.", (0, s.jsx)("br", {}), "Fuente: Museo de Arte Moderno", (0, s.jsx)("br", {}), " de Medell\xedn"]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-dos",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: " Semiotics of the Kitchen, "
                  }), " 1974. Martha Rosler.", (0, s.jsx)("br", {}), (0, s.jsx)("br", {})]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-tres",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "El Tendedero, "
                  }), "1978-2020 ", (0, s.jsx)("br", {}), " Monica Meyer.", (0, s.jsx)("br", {}), "Fuente: Museo Univesitario de Arte Contempor\xe1neo MUAC.", (0, s.jsx)("br", {}), " "]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cuatro",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: " Cuja, "
                  }), "1972.", (0, s.jsx)("br", {}), "Feliza Bursztyn. ", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cinco",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Mientras dorm\xedamos, "
                  }), "2004.", (0, s.jsx)("br", {}), "Lorena Wolffer. ", (0, s.jsx)("br", {})]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    1638: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En esta sesi\xf3n estudiar\xe1s diferentes performances y obras de artistas mujeres con el fin de analizar el uso de los objetos y las acciones para resignificar problem\xe1ticas relacionadas con el espacio dom\xe9stico.  "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/politico-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    8143: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "A partir del postulado feminista \u201cLo personal es pol\xedtico\u201d acu\xf1ado por Carol Hanisch y el movimiento por la liberaci\xf3n de la mujer iniciado en los a\xf1os 60 en USA, reflexionar\xe1s en torno a las problem\xe1ticas consideradas personales y del ambito dom\xe9stico. Entender\xe1s el hogar como lugar en el que se construyen las identidades de g\xe9nero y donde se viven violencias en torno a temas como la sexualidad, la apariencia y la salud reproductiva. A partir del an\xe1lisis de las obras de las artistas referentes identificar\xe1s las maneras en que ellas abordaron temas de su intimidad para hacer referencia a las violencias sufridas y normalizadas."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/politico-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    3756: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/cuerpos-desobedientes/modulo3/conceptos/archivo.jpg",
          caption: "\u201cDibujo de un mueble de archivo\u201d, archivo municipal de Wurzburgo, Alemania. Fuente: Wikimedia Commons bajo dominio p\xfablico. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo3/conceptos/reconfiguracion.jpg",
          caption: "\u201cRueda de bicicleta\u201d. Marcel Duchamp (1913). Galleria nazionale d'arte moderna, Rome, Italy. Fuente: Wikimedia Commons bajo dominio p\xf9blico. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo3/conceptos/activismo.jpg",
          caption: "Protestas por el asesinato de George Floyd a manos de la polic\xeda (2020). Fuente: Wikimedia Commons bajo licencia C.C. BY 2.0"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-3",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Archivo:"
                  }), (0, s.jsx)("p", {
                    children: "Es el conjunto de documentos de toda especie, clasificada, registrada y conservada con el fin de divulgar informaci\xf3n."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Reconfiguraci\xf3n: "
                  }), (0, s.jsx)("p", {
                    children: "Tomar los elementos que constituyen algo para darle un nuevo significado o modificar sus caracter\xedsticas."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-tres-activismo",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Activismo: "
                  }), (0, s.jsx)("p", {
                    children: "Es la participaci\xf3n activa en un movimiento por el cambio social y pol\xedtico."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    5586: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = (o(892), o(2004));
      o(4567);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(r, {})
        }
      };
      var r = function () {
        var e = (0, i.useState)(!1),
          a = e[0],
          o = e[1],
          r = (0, i.useState)(!1),
          c = r[0],
          l = r[1],
          t = (0, i.useState)(null),
          d = t[0],
          u = t[1];
        return (0, s.jsxs)(s.Fragment, {
          children: [(0, s.jsxs)("ol", {
            className: "lista-ejercicio",
            children: [(0, s.jsx)("li", {
              children: "Selecciona un tema que quieras tratar relacionado con la exigencia de derechos."
            }), (0, s.jsx)("li", {
              children: " Busca archivos relacionados con el tema como noticias, videos, documentos, fotos, etc."
            }), (0, s.jsx)("li", {
              children: "Selecciona un tema musical, una noticia o un texto. "
            }), (0, s.jsx)("li", {
              children: "Cambiale la letra utilizando la informaci\xf3n encontrada en los archivos y tu posici\xf3n frente a la problem\xe1tica investigada. "
            }), (0, s.jsx)("li", {
              children: "Graba un video tuyo donde se registre la obra realizada. "
            })]
          }), (0, s.jsx)("p", {
            className: "materiales pt-3",
            children: "Materiales: Archivos (noticias, canciones, videos, documentos, etc), tema musical seleccionado, papel y l\xe1piz, versi\xf3n en karaoke de la canci\xf3n, c\xe1mara de celular, de tablet, de computador o fotogr\xe1fica."
          }), (0, s.jsx)("p", {
            className: "subtitulo-naranja",
            children: "Revisa los ejercicios de otros participantes"
          }), (0, s.jsxs)("div", {
            className: "galeria-5-2 ",
            children: [(0, s.jsxs)("div", {
              className: "modulo-cover-item galeria-5-2-uno px-0",
              children: [(0, s.jsx)(n.Z, {
                playsinline: !0,
                url: "../../../video/credo.mp4",
                width: "100%",
                height: "100%",
                controls: !0,
                style: {
                  transition: "all 1s"
                },
                onEnded: function () {
                  o(!a)
                },
                light: "/images/cuerpos-desobedientes/modulo2/ejercicios/credo.png",
                config: {
                  file: {
                    attributes: {
                      controlsList: "play nodownload"
                    }
                  }
                }
              }), (0, s.jsxs)("p", {
                children: [(0, s.jsx)("span", {
                  className: "titulo-obra",
                  children: "Credo Anorexia,"
                }), (0, s.jsx)("br", {}), "Paola Salamanca"]
              })]
            }), (0, s.jsxs)("div", {
              className: "modulo-cover-item galeria-5-2-dos px-0",
              children: [(0, s.jsx)(n.Z, {
                playsinline: !0,
                url: "../../../video/sacha.mp4",
                width: "100%",
                height: "100%",
                controls: !0,
                style: {
                  transition: "all 1s"
                },
                onEnded: function () {
                  o(!a)
                },
                light: "/images/cuerpos-desobedientes/modulo2/ejercicios/sacha.png",
                config: {
                  file: {
                    attributes: {
                      controlsList: "play nodownload"
                    }
                  }
                }
              }), (0, s.jsxs)("p", {
                children: [(0, s.jsx)("span", {
                  className: "titulo-obra",
                  children: "Suturo carretas a domicilio,"
                }), (0, s.jsx)("br", {}), "Sacha Guerrero"]
              })]
            }), (0, s.jsxs)("div", {
              className: "modulo-cover-item galeria-5-2-tres px-0",
              children: [(0, s.jsx)(n.Z, {
                playsinline: !0,
                url: "../../../video/genesareth.mp4",
                width: "100%",
                height: "100%",
                controls: !0,
                style: {
                  transition: "all 1s"
                },
                onEnded: function () {
                  o(!a)
                },
                light: "/images/cuerpos-desobedientes/modulo2/ejercicios/genesareth.png",
                config: {
                  file: {
                    attributes: {
                      controlsList: "play nodownload"
                    }
                  }
                }
              }), (0, s.jsx)("p", {
                children: "Genesareth Valdes"
              }), (0, s.jsxs)("p", {
                children: [(0, s.jsx)("span", {
                  className: "titulo-obra"
                }), (0, s.jsx)("br", {}), "Genesareth Valdes"]
              })]
            }), (0, s.jsxs)("div", {
              className: "modulo-cover-item galeria-5-2-cuatro px-0",
              children: [(0, s.jsx)(n.Z, {
                playsinline: !0,
                url: "../../../video/estefania.mp4",
                width: "100%",
                height: "100%",
                controls: !0,
                style: {
                  transition: "all 1s"
                },
                onEnded: function () {
                  o(!a)
                },
                light: "/images/cuerpos-desobedientes/modulo2/ejercicios/estefania.png",
                config: {
                  file: {
                    attributes: {
                      controlsList: "play nodownload"
                    }
                  }
                }
              }), (0, s.jsxs)("p", {
                children: [(0, s.jsx)("span", {
                  className: "titulo-obra",
                  children: "No somos peligrosas estamos en peligro,"
                }), (0, s.jsx)("br", {}), " Estefan\xeda Jaimes"]
              })]
            }), (0, s.jsxs)("div", {
              className: "modulo-cover-item galeria-5-2-cinco px-0",
              children: [(0, s.jsx)(n.Z, {
                playsinline: !0,
                url: "../../../video/karen.mp4",
                width: "100%",
                height: "100%",
                controls: !0,
                style: {
                  transition: "all 1s"
                },
                onEnded: function () {
                  o(!a)
                },
                light: "/images/cuerpos-desobedientes/modulo2/ejercicios/karen.png",
                config: {
                  file: {
                    attributes: {
                      controlsList: "play nodownload"
                    }
                  }
                }
              }), (0, s.jsxs)("p", {
                children: [(0, s.jsx)("span", {
                  className: "titulo-obra"
                }), (0, s.jsx)("br", {}), " Karen Licedt Alfonso Suarez"]
              })]
            }), c && (0, s.jsx)(ImageLightbox, {
              activeImage: d,
              imagenes: imagenes,
              closeImageLightBox: function (e) {
                l(!c), u(void 0 !== e ? e : null)
              }
            })]
          })]
        })
      }
    },
    4375: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Mi cuerpo como campo de batalla",
        subtitulo: "Reconfiguraci\xf3n de archivos\xa0",
        contenido: [o(3834).Z, o(1844).Z, o(5403).Z, o(5586).Z, o(3756).Z, o(3526).Z, o(6173).Z]
      }
    },
    3834: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo reflexionar\xe1s en torno a la expresi\xf3n art\xedstica y la organizaci\xf3n de colectivos de arte para exigir derechos. Estudiar\xe1s algunas expresiones art\xedsticas de  los colectivos de j\xf3venes que transforman la sociedad positivamente desde el arte y el activismo. Realizar\xe1s un ejercicio de reconfiguraci\xf3n a partir de noticias o canciones para crear una obra de activismo art\xedstico utilizando el cuerpo y archivos audiovisuales. "
            }), (0, s.jsx)("img", {
              src: "/images/cuerpos-desobedientes/banner_mod3.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    6173: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/batalla-clase-teorica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/batalla-clase-practica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/batalla-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n"
            })]
          })
        }
      }
    },
    3526: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/cuerpos-desobedientes/modulo3/referentes/maria.jpg",
          caption: "Maria Evelia Marmolejo An\xf3nimo 1. Cali-Colombia. 1981. Visita la obra en este enlace: bit.ly/3uhnG6O"
        }, {
          src: "/images/cuerpos-desobedientes/modulo3/referentes/colectivo.jpg",
          caption: "Colectivo Sociedad Civil Lava la bandera\u201d 2000. Visita la obra en este enlace: bit.ly/3ibEE1G"
        }, {
          src: "/images/cuerpos-desobedientes/modulo3/referentes/trans.jpg",
          caption: " Red Comunitaria Trans. Visita la obra en este enlace: bit.ly/2Y1LyQ0"
        }, {
          src: "/images/cuerpos-desobedientes/modulo3/referentes/tesis.jpg",
          caption: "Las Tesis. Un violador en tu camino 2019. Visita la obra en este enlace: bit.ly/3ibKyji"
        }, {
          src: "/images/cuerpos-desobedientes/modulo3/referentes/constanza.jpg",
          caption: "Constanza Camelo.Jornadas de limpieza 2. 1994. Visita la obra en este enlace: bit.ly/3ALGU77"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-5-3",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item  galeria-5-3-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "An\xf3nimo 1,"
                  }), " 1981.", (0, s.jsx)("br", {}), "Maria Evelia Marmolejo.", (0, s.jsx)("br", {})]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item  galeria-5-3-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Lava la bandera,"
                  }), " 2000. ", (0, s.jsx)("br", {}), "Colectivo Sociedad Civil.", (0, s.jsx)("br", {})]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item  galeria-5-3-tres",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Red Comunitaria Trans,"
                  }), " Bogot\xe1. ", (0, s.jsx)("br", {}), " "]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item  galeria-5-3-cuatro",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Un violador en tu camino, "
                  }), "2019. ", (0, s.jsx)("br", {}), "Las Tesis.", (0, s.jsx)("br", {})]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item  galeria-5-3-cinco",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Jornadas de limpieza 2, "
                  }), "1994.", (0, s.jsx)("br", {}), "Constanza Camelo.", (0, s.jsx)("br", {}), " "]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    5403: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "Con el fin de entender c\xf3mo se planifica un performance y c\xf3mo las actividades pueden ser resignificadas, en esta sesi\xf3n realizar\xe1s un ejercicio de observaci\xf3n y reconfiguraci\xf3n. En la clase analizar\xe1s algunos archivos relacionados con conflictos relacionados con el poder. Luego realizar\xe1s un ejercicio de reconfiguraci\xf3n de un tema musical, una noticia o un discurso para interpretarlo frente a la c\xe1mara de video. "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/batalla-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    1844: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "Estudiar\xe1s las obras de los artistas referentes que crean a partir de archivos de noticias y de la memoria hihist\xf3rica en respuesta a problem\xe1ticas producidas por el sistema pol\xedtico. Aprender\xe1s a utilizar tu cuerpo como una herramienta de comunicaci\xf3n. "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/batalla-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    2258: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/cuerpos-desobedientes/modulo4/conceptos/subalterno.jpg",
          caption: "\u201cRecolector de basura\u201d. Le\xf3n Ruiz (1933). Fuente: Colecci\xf3n MAMBO. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo4/conceptos/marika.jpg",
          caption: "Marcha del orgullo disidente en Paran\xe1, Argentina (2018). Autora: Paula Kindsvater. Fuente: Wikimedia Commons bajo licencia C.C. BY 4.0. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo4/conceptos/parodia.jpg",
          caption: "\u201cEscena revolucionaria: parodia de La \xdaltima Cena\u201d. Auguste Bouquet (1834). Colecci\xf3n Galer\xeda Nacional de Arte de Washington D.C. Fuente: Wikimedia Commons bajo dominio p\xfablico. "
        }, {
          src: "/images/cuerpos-desobedientes/modulo4/conceptos/alter-ego.jpg",
          caption: "Poster de una adaptaci\xf3n teatral de la obra \u2018El extra\xf1o caso de Dr. Jekyll y Mr. Hyde\u2019, publicado por la compa\xf1\xeda nacional de impresi\xf3n y grabado de Chicago (1888). Fuente: Wikimedia Commons bajo Dominio P\xfablico. "
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-4-conceptos",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Sujeto Subalterno:"
                  }), (0, s.jsx)("p", {
                    children: "Es usado para denotar personas y poblaciones marginadas y oprimidas quienes frecuentemente luchan contra los sistemas de dominaci\xf3n."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Disidencia:"
                  }), (0, s.jsx)("p", {
                    children: "Estar en desacuerdo con una pol\xedtica, doctrina o directriz establecida en un estado u organizaci\xf3n, sea en lo pol\xedtico, religioso o institucional. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-siete",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Parodia:"
                  }), (0, s.jsx)("p", {
                    children: "Es la recreaci\xf3n de un personaje o un hecho, empleando recursos ir\xf3nicos para emitir una opini\xf3n generalmente transgresora sobre la persona o el acontecimiento parodiado."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-seis",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Alter Ego:"
                  }), (0, s.jsx)("p", {
                    children: "Es un segundo yo que se cree distinto de la personalidad normal u original de una persona"
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("3")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    7927: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2004),
        r = o(2642);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(l, {})
        }
      };
      var c = [{
          src: "/images/heroes-y-villanos/modulo4/ejercicios/mao.jpg",
          caption: "Mao. Fotograf\xeda intervenida por Elizabeth Sarmiento."
        }, {
          src: "/images/heroes-y-villanos/modulo4/ejercicios/garavito.jpg",
          caption: "Garavito. Fotograf\xeda intervenida por Maria Isabel Rodr\xedguez."
        }, {
          src: "/images/heroes-y-villanos/modulo4/ejercicios/uribe.jpg",
          caption: "Uribe. Fotograf\xeda intervenida por Ginna Vel\xe1zquez."
        }, {
          src: "/images/heroes-y-villanos/modulo4/ejercicios/picasso.png",
          caption: "Picasso. Fotograf\xeda intervenida por Laura Zamudio. "
        }],
        l = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            l = (0, i.useState)(!1),
            t = l[0],
            d = l[1],
            u = (0, i.useState)(null),
            m = u[0],
            p = u[1];
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("h1", {
              children: "CREA TU ALTER EGO."
            }), (0, s.jsx)("p", {
              children: "Vamos a dise\xf1ar un personaje liberador. Puedes partir de un personaje que ya conozcas"
            }), (0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Selecciona algunos elementos que te ayudar\xe1n a transformarte, puede ser algo muy simple como una m\xe1scara, una peluca, unos zapatos o un bigote. "
              }), (0, s.jsx)("li", {
                children: "Inventa un nombre llamativo para tu personaje."
              }), (0, s.jsx)("li", {
                children: "As\xedgnale un comportamiento:  \xbfC\xf3mo habla?\xbfDe qu\xe9 habla? \xbfhabla? \xbfCanta? \xbfHace fonomimicas?. \xbfCu\xe1les son sus se\xf1as? \xbfC\xf3mo es su pelo?\xbfC\xf3mo se viste?\xbfes una exageraci\xf3n de ti mismo? "
              }), (0, s.jsx)("li", {
                children: "Escribe una biograf\xeda corta de tu alter ego. Qui\xe9n es y por qu\xe9 existe. "
              }), (0, s.jsx)("li", {
                children: "Escribe un manifiesto en el que nos cuentes sobre qu\xe9 se manifiesta tu alter ego. "
              }), (0, s.jsx)("li", {
                children: "Haz un videoperformance o fotoperformance de este personaje."
              })]
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: Papel y l\xe1piz, vestuario, maquillaje u objetos. C\xe1mara de celular, de tablet, de computador o fotogr\xe1fica."
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-video-3",
              children: [(0, s.jsxs)("div", {
                className: "modulo-cover-item galeria-video-3-uno  px-0",
                children: [(0, s.jsx)(n.Z, {
                  playsinline: !0,
                  url: "../../../video/mister.mp4",
                  width: "100%",
                  height: "100%",
                  controls: !0,
                  style: {
                    transition: "all 1s"
                  },
                  onEnded: function () {
                    o(!a)
                  },
                  light: "/images/cuerpos-desobedientes/modulo4/ejercicios/cover-2.png",
                  config: {
                    file: {
                      attributes: {
                        controlsList: "play nodownload"
                      }
                    }
                  }
                }), (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Mister"
                  }), (0, s.jsx)("br", {}), "Daniela Narv\xe1ez"]
                })]
              }), (0, s.jsxs)("div", {
                className: "modulo-cover-item galeria-video-3-dos  px-0",
                children: [(0, s.jsx)(n.Z, {
                  playsinline: !0,
                  url: "../../../video/el-payaso.mp4",
                  width: "100%",
                  height: "100%",
                  controls: !0,
                  style: {
                    transition: "all 1s"
                  },
                  onEnded: function () {
                    o(!a)
                  },
                  light: "/images/cuerpos-desobedientes/modulo4/ejercicios/cover-3.png",
                  config: {
                    file: {
                      attributes: {
                        controlsList: "play nodownload"
                      }
                    }
                  }
                }), (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "El payaso"
                  }), (0, s.jsx)("br", {}), "Genesareth Valdes"]
                })]
              }), (0, s.jsxs)("div", {
                className: "modulo-cover-item galeria-video-3-tres  px-0",
                children: [(0, s.jsx)(n.Z, {
                  playsinline: !0,
                  url: "../../../video/la-mas.mp4",
                  width: "100%",
                  height: "100%",
                  controls: !0,
                  style: {
                    transition: "all 1s"
                  },
                  onEnded: function () {
                    o(!a)
                  },
                  light: "/images/cuerpos-desobedientes/modulo4/ejercicios/cover-1.png",
                  config: {
                    file: {
                      attributes: {
                        controlsList: "play nodownload"
                      }
                    }
                  }
                }), (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "La M\xe1s"
                  }), (0, s.jsx)("br", {}), "Daniela Narv\xe1ez"]
                }), "        "]
              }), t && (0, s.jsx)(r.Z, {
                activeImage: m,
                imagenes: c,
                closeImageLightBox: function (e) {
                  d(!t), p(void 0 !== e ? e : null)
                }
              })]
            })]
          })
        }
    },
    7179: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Mi otro yo",
        subtitulo: "Creaci\xf3n de un alter ego",
        contenido: [o(3317).Z, o(5353).Z, o(9032).Z, o(7927).Z, o(2258).Z, o(8019).Z, o(9152).Z]
      }
    },
    3317: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo explorar\xe1s la creaci\xf3n de personajes a partir del an\xe1lisis de la estrategia de contra-representaci\xf3n y otras manifestaciones como el cabaret, el drag queen y el drag king. Investigar\xe1s diferentes formas de resignificaci\xf3n de los estigmas y estereotipos para proponer la creaci\xf3n de un personaje empoderador. Realizar\xe1s un ejercicio para crear tu alter ego, que tomar\xe1 una voz p\xfablica frente al acoso, el bullying y la violencia de g\xe9nero. "
            }), (0, s.jsx)("img", {
              src: "/images/cuerpos-desobedientes/banner_mod4.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    9152: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/otro-clase-teorica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/otro-clase-practica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/otro-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n con artista invitado El Chico sin cabello de Pan"
            })]
          })
        }
      }
    },
    8019: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/cuerpos-desobedientes/modulo4/referentes/martirio.jpg",
          caption: "El martirio de San Sebasti\xe1n, 1980. \xc1lvaro Barrios. Fuente: Colecci\xf3n MAMBO."
        }, {
          src: "/images/cuerpos-desobedientes/modulo4/referentes/hot-dog.jpg",
          caption: "Hot Dog, 2009. Narcissister.  Visita la obra en este enlace: bit.ly/39OeehQ"
        }, {
          src: "/images/cuerpos-desobedientes/modulo4/referentes/lover-man.png",
          caption: "Lover man, 2000. Santiago Echeverry. Visita la obra en este enlace: bit.ly/3D176vb"
        }, {
          src: "/images/cuerpos-desobedientes/modulo4/referentes/la-chica-boom.jpg",
          caption: "Spectacles 2002-2012, Spectacle 4. La Virgensota Jota, 2006. Xandra Ibarra. La Chica boom. Visita la obra en este enlace: bit.ly/3unMIBg"
        }, {
          src: "/images/cuerpos-desobedientes/modulo4/referentes/man-for-a-day.jpg",
          caption: "Man for a day workshop, 1990. Diane Torr.  Visita la obra en este enlace: bit.ly/3zR51A2"
        }, {
          src: "/images/cuerpos-desobedientes/modulo4/referentes/super-mojado.jpg",
          caption: "Mad Mex y Mextermineitor, s.f. Guillermo G\xf3mez Pe\xf1a.  Visita la obra en este enlace: bit.ly/3zQeyan"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-6",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-uno",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Hot Dog, "
                    }), " 2009.", (0, s.jsx)("br", {}), "Narcissister."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-dos",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Lover man,"
                    }), " 2000.", (0, s.jsx)("br", {}), "Santiago Echeverry."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-tres",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "El martirio de San Sebasti\xe1n,"
                    }), " 1980.", (0, s.jsx)("br", {}), " \xc1lvaro Barrios.", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO. "]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-cuatro",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Spectacles 2002-2012, Spectacle 4. La Virgensota Jota,"
                    }), " 2006.", (0, s.jsx)("br", {}), "Xandra Ibarra. ", (0, s.jsx)("br", {}), "La Chica boom."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-cinco",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Man for a day workshop,"
                    }), "1990.", (0, s.jsx)("br", {}), "Diane Torr. "]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-ocho",
                onClick: function () {
                  d("5")
                },
                style: {
                  backgroundImage: "url(".concat(u("5"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Mad Mex y Mextermineitor, "
                    }), "s.f", (0, s.jsx)("br", {}), "Guillermo G\xf3mez Pe\xf1a. "]
                  })
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    9032: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "Con el fin de entender c\xf3mo se planifica un performance y c\xf3mo las actividades pueden ser resignificadas, en esta sesi\xf3n realizar\xe1s un ejercicio de observaci\xf3n y reconfiguraci\xf3n. En la clase analizar\xe1s algunos archivos relacionados con conflictos relacionados con el poder. Luego realizar\xe1s un ejercicio de reconfiguraci\xf3n de un tema musical, una noticia o un discurso para interpretarlo frente a la c\xe1mara de video. "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/otro-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    5353: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "Estudiar\xe1s las obras de los artistas referentes que crean a partir de archivos de noticias y de la memoria hihist\xf3rica en respuesta a problem\xe1ticas producidas por el sistema pol\xedtico. Aprender\xe1s a utilizar tu cuerpo como una herramienta de comunicaci\xf3n. "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/otro-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    1558: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(3845),
        n = o(2997),
        r = o(787),
        c = o(7649),
        l = o(4519),
        t = o(4002);
      i.Z.use([n.Z, r.Z, c.Z]);
      a.Z = function (e) {
        var a = e.testimonios;
        return (0, s.jsx)(s.Fragment, {
          children: (0, s.jsx)(l.t, {
            className: "banner-home",
            slidesPerView: 1,
            loop: !0,
            autoplay: {
              delay: 8e3,
              disableOnInteraction: !1
            },
            pagination: {
              clickable: !0,
              renderBullet: function (e, a) {
                return '<span class="' + a + '">' + (e + 1) + "</span>"
              },
              dynamicBullets: !0,
              dynamicMainBullets: 3
            },
            lazy: !0,
            centeredSlides: !0,
            children: a.map((function (e, a) {
              return (0, s.jsx)(t.o, {
                className: "py-5",
                style: {
                  backgroundImage: "url(/images/testimonios/testimonio_".concat(a + 1, ".jpg)")
                },
                children: (0, s.jsx)("p", {
                  children: e
                })
              })
            }))
          })
        })
      }
    },
    4995: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo1/conceptos/artepostal.jpeg",
          caption: "Tarjeta postal (1923). Fuente: Wikimedia Commons bajo dominio p\xfablico."
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/conceptos/distancia.jpg",
          caption: "Mapa de los caminos de larga distancia en Europa. Fuente: Wikimedia Commons bajo licencia C.C. BY 3.0"
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/conceptos/construccion.jpg",
          caption: "Taller de creaci\xf3n colaborativa en el proyecto Parchando el MAMBO (2019). Fuente: Archivo MAMBO."
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/conceptos/archivo.jpeg",
          caption: "Archivo de documentos judiciales en Sion (Suiza). Fuente: Wikimedia Commons bajo licencia C.C. BY 4.0"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-4-conceptos",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Arte postal:"
                  }), (0, s.jsx)("p", {
                    children: "En ingl\xe9s Mail Art, es una forma de producci\xf3n de obras y exposiciones de manera colaborativa a la distancia usando el correo para ello. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Distancia: "
                  }), (0, s.jsx)("p", {
                    children: "  Espacio o intervalo de lugar o de tiempo que media entre dos cosas o sucesos. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-tres",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Construcci\xf3n colaborativa: "
                  }), (0, s.jsx)("p", {
                    children: "Trabajo en conjunto realizado por varias personas."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-cuatro",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Archivo:"
                  }), (0, s.jsx)("p", {
                    children: "Conjunto ordenado de documentos."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("3")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    1379: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo1/ejercicios/mariasuarez.jpg",
          caption: "Soy Mar\xeda Alejandra Su\xe1rez Manrique, tengo 20 a\xf1os, soy estudiante de Artes esc\xe9nicas y ciencia pol\xedtica, me gusta montar en bici, el helado de lim\xf3n, el rock en espa\xf1ol y las novelas dist\xf3picas."
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/ejercicios/tamaracastillo.png",
          caption: "Tamara Castillo. En este collage quise jugar con dos dispositivos m\xf3viles. Creo que antes con las c\xe1maras de rollo no ten\xedamos m\xe1s de un intento por fotograf\xeda y exist\xeda la magia de esperar el resultado de la toma. Ahora, con los avances tecnol\xf3gicos y la fotograf\xeda digital cada quien se toma m\xfaltiples im\xe1genes para sentirse c\xf3modo con su reflejo. De hecho, nos enfocamos m\xe1s en la imagen que en compartir con nuestro alrededor o disfrutar el momento."
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/ejercicios/juandavidm.png",
          caption: "Hola, buenas noches. Mucho gusto, mi nombre es Juan David Merch\xe1n, tengo 10 a\xf1os y para mi el arte representa muchas formas de expresar lo que siento. Me gusta, es una pasi\xf3n que me distrae de la cotidianidad, les agradezco por poder compartirlo con ustedes y ser apreciado. Sin importar si es con colores, pintura, cer\xe1mica, reciclaje, m\xfasica. Todo ello me motiva brind\xe1ndome mucha felicidad."
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/ejercicios/ultimapalabra.jpg",
          caption: "Hola, cibercompa\xf1era/os creadora/es. \u201cPara mi selfie, me tom\xe9 el atrevimiento de extraer e imprimir la \xfaltima palabra escrita por cada una/o de ustedes en sus correos de selfies. Tom\xe9 en cuenta los recibidos hasta las 17:12hs. No hubo motivo para escoger esa hora en particular, tan s\xf3lo me propuse no alcanzar a coleccionarlas todas.Y elabor\xe9 una cajita. Soy una cajita de gestos, de encuentros con palabras, de grupos de cosas que no significan nada. Soy un buz\xf3n de \xfaltimas palabras, pero no soy la \xfaltima palabra."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = (e[0], e[1], (0, i.useState)(!1)),
            o = a[0],
            c = a[1],
            l = (0, i.useState)(null),
            t = l[0],
            d = l[1],
            u = function (e) {
              c(!o), d(void 0 !== e ? e : null)
            },
            m = function (e) {
              var a;
              for (var o in r) o === e && (a = r[o].src);
              return a
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Invita a tus amigos, familiares, vecinos o conocidos a participar en un experimento de creaci\xf3n colectiva a distancia."
              }), (0, s.jsx)("li", {
                children: "Haz una lista con sus correos electr\xf3nicos personales."
              }), (0, s.jsx)("li", {
                children: "Env\xeda un correo de presentaci\xf3n en el que expliques de qu\xe9 se trata el ejercicio y env\xeda tu presentaci\xf3n personal con un dibujo, video o fotograf\xeda que te represente. "
              }), (0, s.jsx)("li", {
                children: "P\xeddeles a todos que env\xeden una peque\xf1a presentaci\xf3n personal con una imagen, texto, dibujo, video o fotograf\xeda que los represente."
              }), (0, s.jsx)("li", {
                children: "Debes estar pendiente de los mensajes en tu bandeja de entrada. Despu\xe9s de recibir los correos de presentaci\xf3n de tus compa\xf1eros, escoge uno y utiliza lo que enviaron para crear una nueva obra de arte."
              }), (0, s.jsx)("li", {
                children: "Reenv\xeda la nueva versi\xf3n del contenido para que todos puedan verla."
              }), (0, s.jsx)("li", {
                children: "Todos los participantes deben hacer lo mismo con la presentaci\xf3n de otra persona."
              })]
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: Computador, tel\xe9fono, tablet para revisar y escribir mensajes de correo electr\xf3nico."
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-4",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  u("0")
                },
                style: {
                  backgroundImage: "url(".concat(m("0"))
                },
                children: (0, s.jsx)("p", {
                  children: "Mar\xeda Alejandra Su\xe1rez"
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-dos",
                onClick: function () {
                  u("1")
                },
                style: {
                  backgroundImage: "url(".concat(m("1"))
                },
                children: (0, s.jsx)("p", {
                  children: "Tamara Castillo"
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-tres",
                onClick: function () {
                  u("2")
                },
                style: {
                  backgroundImage: "url(".concat(m("2"))
                },
                children: (0, s.jsx)("p", {
                  children: "Juan David Merchan"
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-cuatro-paola-2",
                onClick: function () {
                  u("3")
                },
                style: {
                  backgroundImage: "url(".concat(m("3"))
                },
                children: (0, s.jsx)("p", {
                  children: "Laura Isabel Turga"
                })
              }), o && (0, s.jsx)(n.Z, {
                activeImage: t,
                imagenes: r,
                closeImageLightBox: u
              })]
            })]
          })
        }
    },
    8129: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "A distancia",
        subtitulo: "Arte postal\xa0",
        contenido: [o(4553).Z, o(9555).Z, o(7453).Z, o(1379).Z, o(4995).Z, o(2735).Z, o(6876).Z]
      }
    },
    4553: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3",
              children: "En este m\xf3dulo investigar\xe1s y crear\xe1s a partir de pr\xe1cticas art\xedsticas que usan los medios de comunicaci\xf3n para la creaci\xf3n colectiva de obras a distancia. Hoy en d\xeda el correo electr\xf3nico parece un medio anacr\xf3nico de comunicaci\xf3n frente a las r\xe1pidas alternativas presentadas por las tecnolog\xedas de mensajer\xeda instant\xe1nea. El papel, los sobres y las estampillas son a\xfan m\xe1s caducas. Sin embargo, hay una gran tradici\xf3n que ha usado los medios de comunicaci\xf3n como soporte para las pr\xe1cticas art\xedsticas."
            }), (0, s.jsx)("img", {
              src: "/images/gente-haciendo-cosas/banner_mod1.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    6876: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/a-distancia-clase-teorica.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/a-distancia-clase-practica.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/a-distancia-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n"
            })]
          })
        }
      }
    },
    2735: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo1/referentes/alvarobarrios.jpg",
          caption: "El gran plexi, 1986. \xc1lvaro Barrios. Fuente: Colecci\xf3n MAMBO."
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/referentes/jonier.png",
          caption: "Papel y l\xe1piz, 1976. Jonier Mar\xedn."
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/referentes/paulobrucky.jpg",
          caption: "Invitaci\xf3n a la II exposici\xf3n de arte postal - Brasil, 1976. Paulo Bruscky. Visita la obra en este enlace:  bit.ly/3zLcrEY"
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/referentes/clementepadin.jpg",
          caption: "Sellos postales, 1973. Clemente Pad\xedn. Visita la obra en este enlace: bit.ly/2WboE87"
        }, {
          src: "/images/gente-haciendo-cosas/modulo1/referentes/ulises.jpg",
          caption: "Ephemera, 1978. Ulises Carri\xf3n. Visita la obra en este enlace: bit.ly/3AG81jQ"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-5",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "El gran plexi, "
                  }), "1986.", (0, s.jsx)("br", {}), "\xc1lvaro Barrios.", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO. "]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-dos",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Invitaci\xf3n a la II exposici\xf3n de arte postal - Brasil,"
                  }), " 1976.", (0, s.jsx)("br", {}), "Paulo Bruscky."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-tres",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Sellos postales, "
                  }), "1973.", (0, s.jsx)("br", {}), " Clemente Pad\xedn. "]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cuatro",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Ephemera, "
                  }), "1978.", (0, s.jsx)("br", {}), "Ulises Carri\xf3n."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cinco",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Papel y l\xe1piz,"
                  }), " 1976.", (0, s.jsx)("br", {}), "Jonier Mar\xedn.", (0, s.jsx)("br", {}), " Fuente: Colecci\xf3n MAMBO."]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    7453: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3",
              children: "Este primer ejercicio tratar\xe1 de experimentar libremente con los otros. Cada participante deber\xe1 invitar amigos, familiares, vecinos o conocidos a participar en la actividad. Se debe crear una lista de correo (mailing list) en la que se desarrollar\xe1n ejercicios de creaci\xf3n colectiva. En el primer ejercicio, se debe enviar un correo de presentaci\xf3n en el que cada participante se describa y env\xede im\xe1genes, textos, o dibujos que representen a cada uno. Una vez sean enviados los mensajes de presentaci\xf3n cada uno debe escoger uno de los mensajes, im\xe1genes o textos y utilizarlos para realizar una nueva creaci\xf3n art\xedstica. Se debe enviar esta nueva imagen a la lista de correos."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/a-distancia-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    9555: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3",
              children: "Hoy m\xe1s que nunca las herramientas de comunicaci\xf3n a distancia son altamente importantes para la vida cotidiana,  sobre todo a partir de la crisis que ha generado en el mundo el COVID-19. En esta sesi\xf3n explorar\xe1s las redes como mecanismo de interacci\xf3n, transmisi\xf3n de informaci\xf3n, generaci\xf3n de relaciones personales y medio de producci\xf3n art\xedstica. "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/a-distancia-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    8946: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo4/conceptos/capitalismo.jpg",
          caption: "\xcdcono de un cerebro dentro de una cabeza. Fuente: Wikimedia Commons bajo licencia C.C. BY 4.0"
        }, {
          src: "/images/gente-haciendo-cosas/modulo4/conceptos/productividad.jpg",
          caption: "Dos hombres y dos mujeres trabajando en los molinos de Minnesac, Pennsylvania. Foto: Lewis Hine (1936). Fuente: Wikimedia Commons bajo dominio p\xfablico."
        }, {
          src: "/images/gente-haciendo-cosas/modulo4/conceptos/fomo.jpg",
          caption: "Dos personas con sus celulares tomando fotos.  Autor: Takashi Hososhima. Fuente: Wikimedia Commons bajo licencia C.C. BY SA 2.0"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-3",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Capitalismo cognitivo: "
                  }), (0, s.jsx)("p", {
                    children: "Sistema econ\xf3mico en el que la riqueza se crea a partir de la generaci\xf3n de conocimiento y la captaci\xf3n de la atenci\xf3n del consumidor. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Productividad: "
                  }), (0, s.jsx)("p", {
                    children: "Relaci\xf3n entre la cantidad de trabajo, el tiempo y el producto que se genera en una acci\xf3n espec\xedfica. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-tres",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "FoMo:"
                  }), (0, s.jsx)("p", {
                    children: "En ingl\xe9s Fear Of Missing Out o Temor (Miedo a perderse algo), se refiere a la ansiedad generada por el deseo de estar constantemente conectado en l\xednea pendiente de la vida de otros."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    6602: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo4/ejercicios/leo.jpg",
          caption: "Los humanos elegimos a qu\xe9 nos aferramos o a qu\xe9 permitimos que nos enlacen otros \u201chumanos\u201d, regularmente para el beneficio de ellos. Si  elijo las pantallas tengo de d\xf3nde escoger: Tv., Cel., Tablet, Compu. O todas las anteriores, para vivir m\xe1s cerca de los que viven lejos de m\xed o de quienes incluso no conozco\u2026 y alejarme de los seres que viven conmigo. Si me quiero mostrar como conocedor de la moda, ah\xed est\xe1n las marcas, y usarlas es la mejor manera de mostrar y exhibir lo que soy, al precio que sea\u2026 Ser seguidor, admirador, fan de: deportistas, cantantes, pol\xedticos\u2026 me hace olvidar de m\xed y de mis prop\xf3sitos, de mis posibilidades, por estar pendiente de los \xe9xitos de estos personajes. Aparentar lo que no tengo, me hace endeudar y quedo encadenado a los bancos o peor a\xfan, a los prestamistas\u2026 Estos momentos desconectado, me han permitido mirar hacia adentro y ver las ataduras que me impiden fluir como buena persona y encontrar el equilibrio con mi entorno\u2026Estas gafas para ver m\xe1s La Vida!!!, son el producto art\xedstico de los Momentos Desconectados. "
        }, {
          src: "/images/gente-haciendo-cosas/modulo4/ejercicios/tamara.jpg",
          caption: "Me desconect\xe9 totalmente por un buen tiempo de los aparatos tecnol\xf3gicos y me concentr\xe9 en observar las flores de mi jard\xedn. Luego, decid\xed trazar en un papel diferentes mandalas y para eso us\xe9 hojas de papel, esferos y marcadores. Las pint\xe9 en distintos colores, figuras y formas. Al finalizarlas les tom\xe9 una foto para crear un collage. Muchas veces nos apoderamos de los aparatos tecnol\xf3gicos y dejamos a un lado los elementos tradicionales como lo es el papel o pincel. El retorno a las manualidades y a lo tradicional me sumergi\xf3 en una reflexi\xf3n sobre la complejidad de las relacionalidades del mundo.  Las flores tienen que ver con las formas complejas al igual que las mandalas que representan la espiritualidad y la profundidad de las conexiones y v\xednculos, por eso el paso de la observaci\xf3n de la naturaleza en forma de flores al ejercicio art\xedstico de la creaci\xf3n de las mandalas, lo sent\xed fluido, relajante  y fruct\xedfero."
        }, {
          src: "/images/gente-haciendo-cosas/modulo4/ejercicios/maria.jpeg",
          caption: "En estos d\xedas, haciendo nada en un lugar sabanero, vi un inesperado atardecer en ese lugar. Lo \xfanico que pod\xeda hacer con relaci\xf3n a las nuevas tecnolog\xedas, era tomar fotograf\xedas; no hab\xeda WiFi, ni conexi\xf3n telef\xf3nica de ning\xfan tipo, ni PC, ni tablet, etc. Era como estar en el centro de \xc1frica, me lo imagino. Estas son cuatro fotograf\xedas de aquel bello atardecer."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = (e[0], e[1], (0, i.useState)(!1)),
            o = a[0],
            c = a[1],
            l = (0, i.useState)(null),
            t = l[0],
            d = l[1],
            u = function (e) {
              c(!o), d(void 0 !== e ? e : null)
            },
            m = function (e) {
              var a;
              for (var o in r) o === e && (a = r[o].src);
              return a
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Apaga cualquier dispositivo electr\xf3nico tengas a la mano por un lapso de una hora."
              }), (0, s.jsx)("li", {
                children: "Selecciona un lugar de inter\xe9s y habitarlo por una hora, sin interrupciones. Debes procurar no hacer nada m\xe1s durante este tiempo simplemente estar ah\xed y contemplar lo que sucede. "
              }), (0, s.jsx)("li", {
                children: "Haz esto mismo por dos d\xedas."
              }), (0, s.jsx)("li", {
                children: "Identifica algo que te haya sucedido en ese momento o alguna idea que haya surgido."
              }), (0, s.jsx)("li", {
                children: "Documenta tu idea y comp\xe1rtela."
              })]
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: C\xe1mara de video / fotograf\xeda"
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-3",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-uno",
                onClick: function () {
                  u("0")
                },
                style: {
                  backgroundImage: "url(".concat(m("0"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "pie-foto-ejercicio",
                      children: "Momentos Desconectados."
                    }), (0, s.jsx)("br", {}), "Leo Erazzo"]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  u("1")
                },
                style: {
                  backgroundImage: "url(".concat(m("1"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsx)("p", {
                    children: "Tamara Cantillo."
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-tres",
                onClick: function () {
                  u("2")
                },
                style: {
                  backgroundImage: "url(".concat(m("2"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsx)("p", {
                    children: "Maria Venegas "
                  })
                })
              }), o && (0, s.jsx)(n.Z, {
                activeImage: t,
                imagenes: r,
                closeImageLightBox: u
              })]
            })]
          })
        }
    },
    9594: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Hacer nada",
        subtitulo: "Crear a partir del aburrimiento",
        contenido: [o(1546).Z, o(2003).Z, o(8904).Z, o(6602).Z, o(8946).Z, o(4538).Z, o(5834).Z]
      }
    },
    1546: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3",
              children: "En este m\xf3dulo la idea es detenerse y aprender a no hacer nada. Reflexionar\xe1s sobre la productividad, el descanso y el ocio como una necesidad para la vida y el arte. Desarrollar\xe1s una perspectiva cr\xedtica sobre el uso de los dispositivos electr\xf3nicos y el impacto de \xe9stos en tu salud mental.  En este punto ya se ha hecho mucho. La vida cotidiana est\xe1 colmada de acciones que son impuestas por sistemas productivos. Hoy no es f\xe1cil estar aburrido, de hecho, est\xe1 mal visto, aqu\xed encontrar\xe1s una manera para aprovechar el aburrimiento. "
            }), (0, s.jsx)("img", {
              src: "/images/gente-haciendo-cosas/banner_mod4.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    5834: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/nada-clase-teorica.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/nada-clase-practica.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/nada-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n"
            })]
          })
        }
      }
    },
    4538: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo4/referentes/fernando.jpg",
          caption: "Trampa, 1980. Fernando Cepeda."
        }, {
          src: "/images/gente-haciendo-cosas/modulo4/referentes/jose.jpg",
          caption: "Atrio y nave central, 1996. Jos\xe9 Alejandro Restrepo."
        }, {
          src: "/images/gente-haciendo-cosas/modulo4/referentes/joseph.jpg",
          caption: "I like America and America Likes me, 1974. Joseph Beuys. Visita la obra en este enlace:  bit.ly/3ABjWiO"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-3",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Trampa,"
                    }), " 1980.", (0, s.jsx)("br", {}), " Fernando Cepeda.", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Atrio y nave central,"
                    }), " 1996.", (0, s.jsx)("br", {}), "Jos\xe9 Alejandro Restrepo.", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-tres",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Atrio y nave central,"
                    }), " 1996.", (0, s.jsx)("br", {}), "Jos\xe9 Alejandro Restrepo.", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO."]
                  })
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    8904: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3",
              children: "A partir de tus intereses personales, realizar\xe1s un ejercicio de observaci\xf3n detallada por largos periodos de tiempo. Debe seleccionarse el mejor medio al alcance para documentar los encuentros resultado del ejercicio."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/nada-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    2003: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3",
              children: "El capital ha evolucionado al punto en que la atenci\xf3n es uno de los principales productos para generar riqueza. De ah\xed que los dispositivos tecnol\xf3gicos  y las redes sociales digitales luchan constantemente para llamar la atenci\xf3n de sus usuarios. Las notificaciones y elementos de dise\xf1o de las interfaces de los tel\xe9fonos inteligentes tratan constantemente de hacer pasar a las personas largas jornadas de tiempo haciendo scroll al infinito para evitar el aburrimiento. El tiempo libre es hoy una forma de producci\xf3n de capital para otros. En esta sesi\xf3n te detendr\xe1s e intentar\xe1s pausar el agitado ritmo de la vida para darte la oportunidad de aburrirte y encontrar en este estado un gran potencial creativo."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/nada-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    8429: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Gente haciendo cosas raras a distancia",
        duracion: ["4 m\xf3dulos", "8 sesiones", "12 horas - 14 horas", "4 ejercicios de creaci\xf3n", "8 videos", "En Espa\xf1ol"],
        autor: "Gabriel Zea estudi\xf3 Artes Pl\xe1sticas en la Universidad Nacional de Colombia y la Maestr\xeda en Tecnolog\xeda y Est\xe9tica de las Artes Electr\xf3nicas en la Universidad Nacional Tres de Febrero en Buenos Aires (Argentina). Su investigaci\xf3n explora cr\xedticamente las condiciones del trabajo art\xedstico y productivo en la actualidad, a partir de la realizaci\xf3n de obras que exploran las materias y procesos de diferentes sistemas productivos y econ\xf3micos tanto materiales como inmateriales. Entre sus exposiciones recientes se encuentran: \u201cMonumento al tornillo desconocido\u201d en el marco del X Premio Luis Caballero; \u201c268 trozos de madera 1178 grapas\u201d en el espacio independiente M\xe1s All\xe1; el \u201cIV Premio Bienal de la Fundaci\xf3n Gilberto Alzate Avenda\xf1o\u201d, del cual fue ganador, entro otras muestras colectivas e individuales. En actividades paralelas a la realizaci\xf3n de proyectos de creaci\xf3n ha realizado la curadur\xeda de Articularte para la Feria Internacional de Arte de Bogot\xe1 y hace parte del grupo gestor de Atendido por sus propietarios.",
        arroba: " ",
        categoria: "Arte conceptual y t\xe9cnicas no convencionales",
        descripcion: "Este curso te invita a investigar, crear y reflexionar en torno a las t\xe9cnicas no convencionales del arte conceptual y contempor\xe1neo. Incentivar\xe1 en ti la capacidad para reflexionar sobre la tecnolog\xeda, los medios y las herramientas de comunicaci\xf3n que usas cotidianamente para utilizarlas en la creaci\xf3n de obras art\xedsticas no convencionales. Aprender\xe1s sobre m\xe9todos de producci\xf3n art\xedstica que no se reconocen como propios de la gram\xe1tica tradicional del arte y reconocer\xe1s la importancia de la historia del arte para construir posiciones cr\xedticas sobre problemas actuales. En este curso producido por el Museo de Arte Moderno de Bogot\xe1, el artista Gabriel Zea te guiar\xe1 a trav\xe9s de diferentes estrategias art\xedsticas al tiempo que investigas y realizas ejercicios de creaci\xf3n que expandir\xe1n tus conceptos sobre qu\xe9 es arte.",
        video: "../video/gente-haciendo-portada.mp4",
        habilidades: ["Conocimientos sobre la historia del arte", "Uso de tecnolog\xedas de informaci\xf3n y comunicaci\xf3n", "T\xe9cnicas no convencionales de creaci\xf3n art\xedstica ", "Lectura de im\xe1genes", "An\xe1lisis de sistemas de representaci\xf3n visual", "Proyectos de creaci\xf3n colectiva"],
        dirigido: "Este curso est\xe1 dirigido a adolescentes, j\xf3venes y adultos interesados en las t\xe9cnicas no convencionales del arte conceptual y contempor\xe1neo. No se requiere ser artista o conocedor de las t\xe9cnicas del arte conceptual. Recomendado para estudiantes de bachillerato con intereses en el arte, docentes, artistas y p\xfablico en general interesado en aprender y desarrollar t\xe9cnicas de investigaci\xf3n y creaci\xf3n inspiradas en el arte conceptual.",
        pie_imagen_titulo: "Trampa,",
        pie_imagen_cuerpo: "1980. Fernando Cepeda.\xa0 Fuente: Colecci\xf3n MAMBO ",
        modulos: [o(8129).Z, o(3434).Z, o(3157).Z, o(9594).Z],
        testimonios: ["\u201cEn \u2018Gente haciendo cosas raras a distancia\u2019 aprend\xed t\xe9cnicas de creaci\xf3n interesantes y diferentes maneras de comunicarme de una forma din\xe1mica utilizando fotos, videos, audios y otros. Me gust\xf3 mucho el desarrollo de los diferentes ejercicios que sacaron una gran creatividad en m\xed\u201d", "\u201cReafirm\xe9 algunos conceptos acerca del arte conceptual y conoc\xed un poco m\xe1s la manera de creaci\xf3n de contenidos art\xedsticos en una era contempor\xe1nea.\u201d", "\u201cAprend\xed sobre arte postal, la desmaterializaci\xf3n del arte y el valor de las ideas. Repens\xe9 los objetos y mi relaci\xf3n con ellos, conmigo misma y mi tiempo.\u201d", "\u201cMe gust\xf3 mucho el sistema de construcci\xf3n colectiva y los intercambios.\u201d", "\u201cAprend\xed a manejar la imagen, editar video y encontrar formas pr\xe1cticas y sencillas de hacer arte\u201d"]
      }
    },
    7883: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo3/conceptos/coleccion.jpg",
          caption: "Colecci\xf3n de arte antiguo y medieval del Museo de Arte de Azerbaiy\xe1n. Foto: Urek Meniashvili. Fuente: Wikimedia Commons bajo la licencia C.C BY SA 3.0"
        }, {
          src: "/images/gente-haciendo-cosas/modulo3/conceptos/inventario.jpeg",
          caption: "Inventario filat\xe9lico (1957). Fuente: Wikimedia Commons bajo dominio p\xfablico."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-2",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-2-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos position-relative",
                  children: [(0, s.jsx)("h2", {
                    children: "Colecci\xf3n:"
                  }), (0, s.jsx)("p", {
                    children: "Conjunto ordenado de cosas, por lo com\xfan de una misma clase y reunidas por su especial inter\xe9s o valor."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-2-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos position-relative",
                  children: [(0, s.jsx)("h2", {
                    children: "Inventario:"
                  }), (0, s.jsx)("p", {
                    children: "Anotaci\xf3n de los bienes y dem\xe1s cosas pertenecientes a una persona o comunidad, hecho con orden y precisi\xf3n."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    9589: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo3/ejercicios/cynthia.jpg",
          caption: "Yo me di cuenta de que ten\xeda art\xedculos dorados en la casa,  incluso dos pares de zapatos de tac\xf3n dorado. Luego record\xe9 las implicaciones culturales que eso ten\xeda en un pa\xeds y ciudad como la m\xeda, busqu\xe9 otros objetos y note que El Oro estaba en representaciones muy comunes y evidentes de la violencia en mi regi\xf3n geogr\xe1fica, decid\xed poner todo eso encima de una pintura igualmente dorada del rostro de una mujer. Ciudad Ju\xe1rez, Chihuahua fue por mucho tiempo la ciudad m\xe1s peligrosa del mundo, caracterizada por el femicidio y el narcotrafico, hay a\xfan muchas huellas- colecciones de esa etapa que persiste como forma de vida en la ciudadan\xeda."
        }, {
          src: "/images/gente-haciendo-cosas/modulo3/ejercicios/tamara.jpg",
          caption: "Colecciono muchas cosas cuando viajo: imanes, llaveros, mu\xf1ecas, servilletas decorativas y postales. Pero algo que considero que he coleccionado por un largo tiempo y se ha vuelto indispensable para m\xed, son los anillos. Tengo una gran colecci\xf3n de diferentes tipos de anillos; me encantan, la textura, el color, el brillo, la forma, el dise\xf1o, entre otras cualidades.  Los anillos son una parte del \u201cvestir del d\xeda a d\xeda\u201d, es algo que extra\xf1o si no llevo conmigo. Me hace falta decorar mis dedos con estos objetos, sin ellos me siento desnuda. En este collage quise mostrar la colecci\xf3n de anillos que tengo y al mismo tiempo plantear una fr\xe1gil frontera entre ser coleccionista y vivir la locura de acumular."
        }, {
          src: "/images/gente-haciendo-cosas/modulo3/ejercicios/omar.jpg",
          caption: "En el caso de mi colecci\xf3n, me preocup\xf3 m\xe1s encontrar un contenedor capaz de potenciar la belleza de los objetos seleccionados."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = (e[0], e[1], (0, i.useState)(!1)),
            o = a[0],
            c = a[1],
            l = (0, i.useState)(null),
            t = l[0],
            d = l[1],
            u = function (e) {
              c(!o), d(void 0 !== e ? e : null)
            },
            m = function (e) {
              var a;
              for (var o in r) o === e && (a = r[o].src);
              return a
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Realiza un inventario de objetos, ideas, textos, etc de inter\xe9s."
              }), (0, s.jsx)("li", {
                children: "Selecciona un grupo de estos objetos y haz una colecci\xf3n de esto."
              }), (0, s.jsx)("li", {
                children: "Dise\xf1a una serie de par\xe1metros para ordenarlos. Puedes escoger ubicarlos de una forma distinta a la acostumbrada, cambiarlos de lugar o incluso modificar los objetos. "
              }), (0, s.jsx)("li", {
                children: "Haz un registro de los objetos o la colecci\xf3n con su nuevo orden. "
              })]
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: Elementos disponibles en la casa, c\xe1mara fotogr\xe1fica para hacer registro."
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-3",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-uno",
                onClick: function () {
                  u("0")
                },
                style: {
                  backgroundImage: "url(".concat(m("0"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsx)("p", {
                    children: "Cynthia Mena"
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  u("1")
                },
                style: {
                  backgroundImage: "url(".concat(m("1"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsx)("p", {
                    children: "Tamara Cantillo"
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-tres",
                onClick: function () {
                  u("2")
                },
                style: {
                  backgroundImage: "url(".concat(m("2"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsx)("p", {
                    children: "Omar Moreno"
                  })
                })
              }), o && (0, s.jsx)(n.Z, {
                activeImage: t,
                imagenes: r,
                closeImageLightBox: u
              })]
            })]
          })
        }
    },
    3157: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Inventario",
        subtitulo: "Colecciones en casa",
        contenido: [o(5227).Z, o(4634).Z, o(4723).Z, o(9589).Z, o(7883).Z, o(5813).Z, o(4970).Z]
      }
    },
    5227: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3",
              children: "En este m\xf3dulo se propone hacer arte a partir de contar, hacer listas, sumar, anotar y pensar en nuestras acciones diarias. Dise\xf1ar\xe1s una colecci\xf3n y elaborar\xe1s obras a partir de los procesos de coleccionar, inventariar y ordenar. \xbfQu\xe9 mejor material para trabajar ahora que debemos permanecer mucho tiempo en nuestras casas que lo que est\xe1 a la mano? Pero, \xbferes consciente de lo que tienes o de la manera en que usas tu tiempo? \xbfCu\xe1ntos pasos das en un d\xeda?\xbfCu\xe1ntas baldosas hay en el ba\xf1o de la casa?"
            }), (0, s.jsx)("img", {
              src: "/images/gente-haciendo-cosas/banner_mod3.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    4970: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/inventario-clase-teorica.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/inventario-clase-practica.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/inventario-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n"
            })]
          })
        }
      }
    },
    5813: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo3/referentes/bocagrande.jpg",
          caption: "Bocagrande 1, 1976. Alicia Barney. "
        }, {
          src: "/images/gente-haciendo-cosas/modulo3/referentes/adolfob.png",
          caption: "Listado de palabras con apuntes, sf. Adolfo Bernal."
        }, {
          src: "/images/gente-haciendo-cosas/modulo3/referentes/adolfo.jpg",
          caption: "Serie Amante/Chicago, Seda/Beatle, Rana/Jinete, Camisa/Bicicleta, Luna/Papel, 1979. Adolfo Bernal."
        }, {
          src: "/images/heroes-y-villanos/modulo2/conceptos/ready.jpg",
          caption: "A-la-cena con zapatos, 1978. Grupo El Sindicato."
        }, {
          src: "/images/gente-haciendo-cosas/modulo3/referentes/bernardo.png",
          caption: "Hect\xe1rea de Heno, 1970.Bernardo Salcedo. Visita la obra en este enlace: bit.ly/3o9zruW"
        }, {
          src: "/images/gente-haciendo-cosas/modulo3/referentes/hincapie.jpg",
          caption: "Una cosa es una cosa, 1990. Mar\xeda Teresa Hincapi\xe9. Visita la obra en este enlace: bit.ly/3u8bgOB"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-6",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Bocagrande 1,"
                    }), " 1976.", (0, s.jsx)("br", {}), " Alicia Barney.", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-dos",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Hect\xe1rea de Heno,"
                    }), " 1970.", (0, s.jsx)("br", {}), " Bernardo Salcedo."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-tres",
                onClick: function () {
                  d("5")
                },
                style: {
                  backgroundImage: "url(".concat(u("5"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Una cosa es una cosa,"
                    }), " 1990.", (0, s.jsx)("br", {}), " Mar\xeda Teresa Hincapi\xe9."]
                  })
                })
              }), (0, s.jsxs)("a", {
                className: "modulo-cover-item galeria-6-tres",
                href: "https://bit.ly/3u8bgOB",
                target: "_blank",
                style: {
                  backgroundImage: "url('/images/gente-haciendo-cosas/modulo3/referentes/hincapie.jpg')"
                },
                children: ["          ", (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Una cosa es una cosa,"
                    }), " 1990.", (0, s.jsx)("br", {}), " Mar\xeda Teresa Hincapi\xe9."]
                  })
                })]
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-cuatro",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Listado de palabras con apuntes,"
                    }), " s.f", (0, s.jsx)("br", {}), "Adolfo Bernal."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-cinco",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Serie Amante/Chicago, Seda/Beatle, Rana/Jinete, Camisa/Bicicleta, Luna/Papel,"
                    }), " 1979.", (0, s.jsx)("br", {}), "Adolfo Bernal.", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-ocho",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "A-la-cena con zapatos,"
                    }), " 1978.", (0, s.jsx)("br", {}), "Grupo El Sindicato.", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO."]
                  })
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    4723: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3",
              children: "Para iniciar es necesario saber qu\xe9 cosas tienes a tu alcance para trabajar, para ello realizar\xe1s un inventario de objetos, ideas, textos, etc. de tu inter\xe9s. Luego proceder\xe1s a realizar una acci\xf3n en la que le des a los objetos un nuevo orden bajo una reglas establecidas. El resultado final del ejercicio ser\xe1 un registro que d\xe9 cuenta del proceso y el nuevo orden establecido con tu colecci\xf3n. "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/inventario-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    4634: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3",
              children: "El arte est\xe1 asociado a la experimentaci\xf3n y los sentidos, por esto es com\xfan pensar que el arte est\xe1 lejos de los sistemas matem\xe1ticos. Sin embargo, el acto de contar e inventariar las acciones cotidianas da pistas sobre las formas de actuar, los h\xe1bitos y los patrones. Contar los objetos nos rodean o llaman la atenci\xf3n permite identificar la relaci\xf3n con los sistemas de consumo. Estos inventarios pueden ser usados como obras de arte o ser materia para construir piezas que surgen de este an\xe1lisis. En esta sesi\xf3n explorar\xe1s mecanismos para registrar tu universos particular y as\xed encontrar lugares de inter\xe9s para crear obras de arte que aborden problemas de la cotidianidad."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/inventario-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    4573: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo2/conceptos/algoritmo.png",
          caption: "Diagrama de una red social que indica los niveles de sociabilidad. Fuente: Wikimedia Commons bajo licencia C.C. BY 3.0"
        }, {
          src: "/images/gente-haciendo-cosas/modulo2/conceptos/autor.jpg",
          caption: "La escritora Gunilla Lundgren presentando su libro en Estocolmo (2019). Fuente: Wikimedia Commons bajo licencia C.C. - S.A 4.0"
        }, {
          src: "/images/gente-haciendo-cosas/modulo2/conceptos/interprete.jpg",
          caption: "Int\xe9rprete del cello en Munich, Alemania (2007). Foto: Jorge Royan. Fuente: Wikimedia Commons bajo la licencia C.C. BY 3.0."
        }, {
          src: "/images/gente-haciendo-cosas/modulo2/conceptos/instruccion.jpg",
          caption: "Instrucciones de papiroflexia para armar un avi\xf3n de papel. Fuente: Wikimedia Commons bajo licencia C.C. BY 2.5."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-4-conceptos",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Algoritmo:"
                  }), (0, s.jsx)("p", {
                    children: "Conjunto ordenado y finito de operaciones que permite hallar la soluci\xf3n de un problema."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Autor: "
                  }), (0, s.jsx)("p", {
                    children: "Persona que ha producido alguna obra cient\xedfica, literaria o art\xedstica."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-tres",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Int\xe9rprete: "
                  }), (0, s.jsx)("p", {
                    children: "Persona que ejecuta una pieza, usualmente musical o teatral."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-cuatro",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Instrucci\xf3n:  "
                  }), (0, s.jsx)("p", {
                    children: "Conjunto de reglas o advertencias para alg\xfan fin."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("3")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    3313: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo2/ejercicios/distraccionario.jpg",
          caption: "DISTRACCIONARIO. Primero: Verifique que su bici no est\xe9 pinchada, que tenga sus papeles al d\xeda. Segundo: Salga a la calle y recorra la ciudad. Tome 4 fotos con un intervalo de: 5-10-35-53 minutos. Tercero: De regreso, clasifiquel\xe1s y n\xf3mbrelas. Cuarto: Suba las fotos al correo del grupo.(Foto 1: atm\xf3sfera. Foto 2: personajes de su obra. Foto 3: estructura compositiva. Foto 4: t\xedtulo de la pintura)"
        }, {
          src: "/images/gente-haciendo-cosas/modulo2/ejercicios/artbol.jpg",
          caption: " AR(T)BOL. Materiales: Hojas de \xe1rbol, plantas o flores, colb\xf3n o cinta, una hoja de papel o cartulina de colores, l\xe1piz, colores o esferos, c\xe1mara o celular. Instrucciones: 1. Salir al parque o a alg\xfan jard\xedn cerca de tu casa. 2. Recoger 10 o m\xe1s hojas de \xe1rboles o flores del suelo. 3.  Regresar a la casa con el material recolectado. 4.  Alistar en la mesa o un lugar c\xf3modo de trabajo los diferentes materiales mencionados en el ejercicio (hojas de \xe1rbol, plantas o flores, colb\xf3n o cinta, una hoja de papel o cartulina de colores, l\xe1piz, colores o esferos, c\xe1mara o celular). 5. Con un l\xe1piz de color, pintar un tronco sobre la cartulina. 6. Pegar con colb\xf3n o cinta las hojas y elementos que recogimos en el parque sobre la cartulina para crear nuestro propio Ar(t)bol. 7.  Cuando hemos finalizado el ejercicio, tomar nuestro celular o c\xe1mara y hacer registro de nuestra obra (Se pueden crear varias variedades de Ar(t)bol). 8. Mandar la imagen de nuestro Ar(t)bol al correo del grupo."
        }, {
          src: "/images/gente-haciendo-cosas/modulo2/ejercicios/babyyoda.jpg",
          caption: "BABY YODA. Materiales: colores, marcador, lapiz, borrador. Pasos: 1.dibujamos un medio c\xedrculo para la cabeza. 2. luego un rect\xe1ngulo para la bufanda que va bajo el medio c\xedrculo. 3.despu\xe9s hacemos un trapecio al rev\xe9s para el abrigo que va ubicado bajo la bufanda. 4.dibujamos dos cuadrados para los brazos que ir\xe1n a los lados del abrigo. 5.dos rect\xe1ngulos peque\xf1os para las mangas de los brazos. 6.continuamos con un \xf3valo peque\xf1o para la nariz. 7.ahora seguimos con las orejas que son tri\xe1ngulos alargados que van uno dentro de otro y de costado. 8. y el \xfaltimo paso es dibujar dos \xf3valos en diagonal con c\xedrculos adentro para que su expresi\xf3n sea de ternura. Bonus: m\xe1s all\xe1 de mis instrucciones puedes a\xf1adirle cosas para marcar tu originalidad, se creativo. Quiz\xe1s no te guste el dibujo, pero aqu\xed te dejo otras opciones del mismo, con diferentes formas y materiales  ( paper craft, plastilina y un t\xedtere de palito). infinidad de t\xe9cnicas para plasmar la misma cosa."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = (e[0], e[1], (0, i.useState)(!1)),
            o = a[0],
            c = a[1],
            l = (0, i.useState)(null),
            t = l[0],
            d = l[1],
            u = function (e) {
              c(!o), d(void 0 !== e ? e : null)
            },
            m = function (e) {
              var a;
              for (var o in r) o === e && (a = r[o].src);
              return a
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Invita a tus amigos, conocidos o familiares a participar en un ejercicio de creaci\xf3n colectiva a distancia."
              }), (0, s.jsx)("li", {
                children: "Haz una lista con sus correos electr\xf3nicos personales."
              }), (0, s.jsx)("li", {
                children: "Elabora una serie de instrucciones para realizar una obra de arte. "
              }), (0, s.jsx)("li", {
                children: "Sigue las instrucciones y realiza una obra de arte. Registra el proceso por medio de fotograf\xedas o videos. "
              }), (0, s.jsx)("li", {
                children: "Env\xeda las instrucciones y el resultado a la lista de correos e invita a los participantes a que hagan lo mismo."
              }), (0, s.jsx)("li", {
                children: "Escoge las instrucciones realizadas por otro participante de la lista de correo y realiza la obra de arte. "
              }), (0, s.jsx)("li", {
                children: "Comparte los resultados por medio de la lista de correos."
              })]
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: Elementos necesarios para realizar la acci\xf3n propuesta. C\xe1mara fotogr\xe1fica para el registro del proceso y resultado"
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-3",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-uno",
                onClick: function () {
                  u("0")
                },
                style: {
                  backgroundImage: "url(".concat(m("0"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsx)("p", {
                    children: (0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Distraccionario"
                    })
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  u("1")
                },
                style: {
                  backgroundImage: "url(".concat(m("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("p", {
                    children: (0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Ar(t)bol"
                    })
                  }), (0, s.jsx)("p", {
                    children: "Tamara Castillo."
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-tres",
                onClick: function () {
                  u("2")
                },
                style: {
                  backgroundImage: "url(".concat(m("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("p", {
                    children: (0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Baby Yoda"
                    })
                  }), (0, s.jsx)("p", {
                    children: "Juan David Merch\xe1n."
                  })]
                })
              }), o && (0, s.jsx)(n.Z, {
                activeImage: t,
                imagenes: r,
                closeImageLightBox: u
              })]
            })]
          })
        }
    },
    3434: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Partituras",
        subtitulo: "Instrucciones para hacer arte",
        contenido: [o(6598).Z, o(2120).Z, o(1052).Z, o(3313).Z, o(4573).Z, o(2706).Z, o(9733).Z]
      }
    },
    6598: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo pensaras, escribir\xe1s y ejecutar\xe1s instrucciones que ser\xe1n convertidas en obras de arte. Cuestionar\xe1s la noci\xf3n de originalidad en el arte y entender\xe1s el lenguaje como un medio de creaci\xf3n art\xedstica. \xbfLas obras de arte deben ser \xfanicas, irreproducibles y ser creadas por la mano genial del artista? \xbfPuede ser el arte fruto de un proceso productivo y estandarizado que permita su realizaci\xf3n fuera del control del artista? Muchas veces se habla del taller como la cocina del artista, el lugar d\xf3nde la magia se materializa en obras de arte. En esa medida, \xbfpodemos pensar en recetas para hacer arte de la misma manera como se hacen deliciosos postres?"
            }), (0, s.jsx)("img", {
              src: "/images/gente-haciendo-cosas/banner_mod2.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    9733: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/partituras-clase-teorica.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/partituras-clase-practica.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/partituras-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n"
            })]
          })
        }
      }
    },
    2706: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/gente-haciendo-cosas/modulo2/referentes/gustavo.png",
          caption: "Partituras mentales, 1976. Gustavo Sorzano. Fuente: Colecci\xf3n MAMBO. "
        }, {
          src: "/images/gente-haciendo-cosas/modulo2/referentes/felixg2.png",
          caption: "Instrucciones para la realizaci\xf3n de una obra de arte, s.f. Felix Gonz\xe1lez-Torres."
        }, {
          src: "/images/gente-haciendo-cosas/modulo2/referentes/beatriz.jpg",
          caption: "Diez metros de Renoir, 1977. Beatriz Gonz\xe1lez. Visita la obra en este enlace: bit.ly/3CJxhq6"
        }, {
          src: "/images/gente-haciendo-cosas/modulo2/referentes/felixg.png",
          caption: "Certificado de originalidad, Sol Lewitt. Visita la obra en este enlace: bit.ly/39Bn7LA"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-4",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Partituras mentales,"
                  }), " 1976.", (0, s.jsx)("br", {}), " Gustavo Sorzano.", (0, s.jsx)("br", {}), "Fuente: Colecci\xf3n MAMBO. "]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-dos",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Diez metros de Renoir,"
                  }), " 1977.", (0, s.jsx)("br", {}), "Beatriz Gonz\xe1lez. "]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-tres",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Certificado de originalidad,"
                  }), (0, s.jsx)("br", {}), "Sol Lewitt."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-cuatro",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("p", {
                  className: "text-lg-end",
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Instrucciones para la realizaci\xf3n de una obra de arte"
                  }), " s.f.", (0, s.jsx)("br", {}), "Felix Gonz\xe1lez-Torres."]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    1052: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En esta sesi\xf3n realizar\xe1s una obra compuesta de dos partes, la primera es crear una serie de instrucciones que te permitir\xe1n ejecutar m\xfaltiples copias con un mismo resultado art\xedstico, la segunda parte es realizar el producto que surge a partir de las instrucciones."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/partituras-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    2120: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("p", {
              className: "pb-lg-3 ",
              children: ["En sus apuntes Marcel Duchamp escribe una secci\xf3n titulada ", (0, s.jsx)("span", {
                className: "fst-italic",
                children: "Especulaciones"
              }), ", en donde se encuentran instrucciones para \u201chacer un cuadro o una escultura desenrollando una bobina de cine\u201d o \u201ccomprar un diccionario y rayar las palabras que son para rayar\u201d. Estas instrucciones escritas en infinitivo, se convierten en una especie de partitura dispuesta para ser ejecutada tanto por el artista u otra persona. Esta estrategia para realizar piezas de arte sin el artista fue una de las herramientas m\xe1s usadas a mediados del siglo XX por artistas como Sol Lewitt que separaban la concepci\xf3n de la obra de su ejecuci\xf3n. En esta sesi\xf3n investigar\xe1s el mundo de los algoritmos e instrucciones para producir obras de arte y explorar\xe1s la noci\xf3n de autor y originalidad. "]
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/partituras-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), "      "]
          })
        }
      }
    },
    2038: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo1/conceptos/retrato.jpg",
          caption: "\u201cFot\xf3grafo\u201d. Le\xf3n Ruiz (1933). Fuente: Colecci\xf3n MAMBO."
        }, {
          src: "/images/heroes-y-villanos/modulo1/conceptos/simbolo.jpg",
          caption: "\u201cS\xedmbolos religiosos\u201d. Ratomir Wilkowski (1990). Fuente: Wikimedia Commons bajo la licencia CC BY-SA 3.0."
        }, {
          src: "/images/heroes-y-villanos/modulo1/conceptos/arquetipo.jpg",
          caption: "\u201cArquetipo de un vertebrado\u201d, Richard Owen (1847).  Fuente: Wikimedia commons."
        }, {
          src: "/images/heroes-y-villanos/modulo1/conceptos/escepticismo.jpg",
          caption: '\u201cSextus Empiricus de Alejandr\xeda, aprox. 200 a 250 d.C., esc\xe9ptico, obra principal: "Conceptos b\xe1sicos pirr\xf3nicos"\u201d. Fuente: Wikimedia Commons bajo dominio p\xfablico.'
        }, {
          src: "/images/heroes-y-villanos/modulo1/conceptos/empatia.png",
          caption: "Empat\xeda cognitiva y afectiva. Melisa Hogan (2020). Fuente: Wikimedia Commons bajo licencia CC BY-SA 4.0 "
        }, {
          src: "/images/heroes-y-villanos/modulo1/conceptos/icono.jpg",
          caption: "\xcdcono de c\xe1mara de seguridad. S,f. Fuente: Flaticon."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-6",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Retrato:"
                  }), (0, s.jsx)("p", {
                    children: "Descripci\xf3n detallada de alguien o de algo. Representaci\xf3n de una persona en dibujo, pintura, escultura o fotograf\xeda."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "S\xedmbolo:"
                  }), (0, s.jsx)("p", {
                    children: " Signo que establece una relaci\xf3n de identidad con una realidad generalmente abstracta. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-tres",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Arquetipo:"
                  }), (0, s.jsx)("p", {
                    children: "Modelo original que sirve como pauta para imitarlo, reproducirlo o copiarlo, o prototipo ideal que sirve como ejemplo de perfecci\xf3n de algo."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-cuatro",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Escepticismo:"
                  }), (0, s.jsx)("p", {
                    children: "Recelo, incredulidad o falta de confianza en la \u201cverdad\u201d o eficacia de una cosa."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("3")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-cinco",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Empat\xeda:"
                  }), (0, s.jsx)("p", {
                    children: "Capacidad para sentir compatibilidad con los sentimientos, pensamientos y realidades de otras personas. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("4")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-seis",
                onClick: function () {
                  d("5")
                },
                style: {
                  backgroundImage: "url(".concat(u("5"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos-derecha",
                  children: [(0, s.jsx)("h2", {
                    children: "\xcdcono:"
                  }), (0, s.jsx)("p", {
                    className: "",
                    children: "Signo que representa un objeto o una idea con el que guarda una relaci\xf3n de identidad o semejanza formal."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("5")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    1416: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2004),
        r = (o(892), o(4567), o(2642));
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(l, {})
        }
      };
      var c = [{
          src: "/images/heroes-y-villanos/modulo1/ejercicios/collage-luisa.jpg",
          caption: "La iron\xeda de tener un h\xe9roe. Collage por Luisa Jim\xe9nez"
        }, {
          src: "/images/heroes-y-villanos/modulo1/ejercicios/collage-isabel.png",
          caption: "D\xe9bora Arango. Collage por Maria Isabel Rodr\xedguez. "
        }, {
          src: "/images/heroes-y-villanos/modulo1/ejercicios/collage-evelyn.jpg",
          caption: "Einstein. Collage por Evelyn Ben\xedtez."
        }, {
          src: "/images/heroes-y-villanos/modulo1/ejercicios/collage-dania.jpg",
          caption: "Mujeres totem. Collage por Dania Viverly Amaya G\xf3mez. "
        }],
        l = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            l = (0, i.useState)(!1),
            t = l[0],
            d = l[1],
            u = (0, i.useState)(null),
            m = u[0],
            p = u[1],
            g = function (e) {
              d(!t), p(void 0 !== e ? e : null)
            },
            h = function (e) {
              return c[e].src
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Escoge a un h\xe9roe o hero\xedna."
              }), (0, s.jsx)("li", {
                children: "Investiga cu\xe1les son sus haza\xf1as m\xe1s memorables y sus cualidades m\xe1s destacables"
              }), (0, s.jsx)("li", {
                children: "Haz una lista de cualidades de este personaje."
              }), (0, s.jsx)("li", {
                children: "Escoge un evento o momento espec\xedfico de la vida de este personaje que demuestre sus cualidades."
              }), (0, s.jsx)("li", {
                children: "Busca una imagen del personaje."
              }), (0, s.jsx)("li", {
                children: "Busca y escoge 10 im\xe1genes de este importante evento y s\xedmbolos que representan sus cualidades."
              })]
            }), (0, s.jsx)("p", {
              className: "titulo-ejercicio",
              children: "Versi\xf3n an\xe1loga"
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: Recortes de revistas o im\xe1genes impresas a color, tijeras, pegastick, cartulina."
            }), (0, s.jsxs)("ol", {
              className: "lista-ejercicio-7",
              children: [(0, s.jsx)("li", {
                children: "Imprime las im\xe1genes y rec\xf3rtalas."
              }), (0, s.jsx)("li", {
                children: "Realiza un collage que comunique por qu\xe9 admiras a este personaje."
              })]
            }), (0, s.jsx)("p", {
              className: "titulo-ejercicio",
              children: "Versi\xf3n digital"
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: PowerPoint"
            }), (0, s.jsxs)("ol", {
              className: "lista-ejercicio-7",
              children: [(0, s.jsx)("li", {
                children: "Imprime las im\xe1genes y rec\xf3rtalas."
              }), (0, s.jsx)("li", {
                children: "Realiza un collage que comunique por qu\xe9 admiras a este personaje."
              })]
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-5",
              children: [(0, s.jsxs)("div", {
                className: "modulo-cover-item galeria-5-uno px-0",
                children: [(0, s.jsx)(n.Z, {
                  playsinline: !0,
                  url: "../../../video/deeper.mp4",
                  width: "100%",
                  height: "100%",
                  controls: !0,
                  style: {
                    transition: "all 1s"
                  },
                  onEnded: function () {
                    o(!a)
                  },
                  light: "/images/heroes-y-villanos/modulo1/ejercicios/deep_png_hover.jpg",
                  config: {
                    file: {
                      attributes: {
                        controlsList: "play nodownload"
                      }
                    }
                  }
                }), (0, s.jsx)("p", {
                  children: "Collage realizado por el artista Deeper in Time (Deep.png)"
                })]
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-dos",
                onClick: function () {
                  g("0")
                },
                style: {
                  backgroundImage: "url(".concat(h("0"), ")")
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "La iron\xeda de tener un h\xe9roe."
                  }), (0, s.jsx)("br", {}), "Collage por Luisa Jim\xe9nez"]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-tres",
                onClick: function () {
                  g("1")
                },
                style: {
                  backgroundImage: "url(".concat(h("1"), ")")
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "D\xe9bora Arango."
                  }), (0, s.jsx)("br", {}), "Collage por Maria Isabel Rodr\xedguez"]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cuatro",
                onClick: function () {
                  g("2")
                },
                style: {
                  backgroundImage: "url(".concat(h("2"), ")")
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Einstein."
                  }), (0, s.jsx)("br", {}), "Collage por Evelyn Ben\xedtez"]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cinco",
                onClick: function () {
                  g("3")
                },
                style: {
                  backgroundImage: "url(".concat(h("3"), ")")
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Mujeres totem."
                  }), (0, s.jsx)("br", {}), "Collage por Dania Viverly Amaya G\xf3mez."]
                })
              }), t && (0, s.jsx)(r.Z, {
                activeImage: m,
                imagenes: c,
                closeImageLightBox: g
              })]
            })]
          })
        }
    },
    1101: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "H\xe9roes",
        subtitulo: "Collage",
        contenido: [o(8042).Z, o(264).Z, o(2250).Z, o(1416).Z, o(2038).Z, o(1001).Z, o(8124).Z]
      }
    },
    8042: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo estudiar\xe1s la construcci\xf3n visual de los h\xe9roes en los medios de comunicaci\xf3n y la historia del arte, con el fin de entender c\xf3mo la imagen de una persona puede ser cargada de significados por medio de elementos visuales. Identificar\xe1s a tus h\xe9roes o hero\xednas personales mientras analizas c\xf3mo se construye la imagen personal en las redes sociales."
            }), (0, s.jsx)("img", {
              src: "/images/heroes-y-villanos/banner_mod1.png",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    8124: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/heroes-clase-teorica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/heroes-clase-practica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/heroes-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n con artista invitado Deep.png"
            })]
          })
        }
      }
    },
    1001: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo1/referentes/declaracion.jpg",
          caption: "Declaraci\xf3n de amor a venezuela, 1976. Juan Camilo Uribe. Fuente: Colecci\xf3n MAMBO"
        }, {
          src: "/images/heroes-y-villanos/modulo1/referentes/supermantel.jpg",
          caption: "El Supermantel, 1984. Marta Elena V\xe9lez. Fuente: Colecci\xf3n MAMBO."
        }, {
          src: "/images/heroes-y-villanos/modulo1/referentes/signs.png",
          caption: "Signs, 1970. Robert Rauschenberg Fuente: MOMA \xa9 Robert Rauschenberg Foundation. Visita la obra en este enlace: mo.ma/3i3MZV3"
        }, {
          src: "/images/heroes-y-villanos/modulo1/referentes/mother.png",
          caption: "My mother, 1986. David Hockney. Visita la obra en este enlace: bit.ly/2XWcQaC"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-4",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Declaraci\xf3n de amor a venezuela,"
                  }), " 1976. ", (0, s.jsx)("br", {}), " Juan Camilo Uribe.", (0, s.jsx)("br", {}), " Fuente: Colecci\xf3n MAMBO."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("p", {
                  className: "text-lg-end",
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "El Supermantel,"
                  }), " 1984. ", (0, s.jsx)("br", {}), "Marta Elena V\xe9lez.", (0, s.jsx)("br", {}), " Fuente: Colecci\xf3n MAMBO."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-tres",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Signs, "
                  }), "1970. ", (0, s.jsx)("br", {}), "Robert Rauschenberg ", (0, s.jsx)("br", {}), " Fuente: MOMA ", (0, s.jsx)("br", {}), " \xa9 Robert Rauschenberg Foundation.", (0, s.jsx)("br", {}), (0, s.jsx)("br", {})]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-cuatro",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("p", {
                  className: "text-lg-end",
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "My mother,"
                  }), " 1986.", (0, s.jsx)("br", {}), "David Hockney.", (0, s.jsx)("br", {})]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    2250: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "Se abordar\xe1n diferentes t\xe9cnicas del collage, estudiando brevemente su origen y c\xf3mo se ha utilizado para la realizaci\xf3n de retratos en la publicidad, la m\xfasica y el dise\xf1o de afiches con el fin de enaltecer a los personajes representados."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/heroes-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    264: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo revisar\xe1s c\xf3mo se han retratado los h\xe9roes a lo largo de la historia y en el presente. Analizar\xe1s las diferentes estrategias visuales que han servido como herramientas para enaltecer personajes hist\xf3ricos y medi\xe1ticos. En este m\xf3dulo se retomar\xe1 la pregunta planteada en la introducci\xf3n: \xbfEs Sim\xf3n Bol\xedvar un h\xe9roe o un villano? a trav\xe9s del estudio de los diferentes retratos que se han hecho de este personaje."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/heroes-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    6064: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo4/conceptos/objetivo.jpg",
          caption: "\u201cMemorial Student Center Texas A&M University\u201d (2015). Fuente: Wikimedia Commons bajo licencia C.C. BY 2.0."
        }, {
          src: "/images/heroes-y-villanos/modulo4/conceptos/sesgo.png",
          caption: "Interpretaciones de los patrones aleatorios de cr\xe1teres en la luna. Un ejemplo com\xfan de un sesgo perceptivo causado por pareidolia. Fuente: Wikimedia Commons bajo licencia C.C. BY 3.0."
        }, {
          src: "/images/heroes-y-villanos/modulo4/conceptos/eco.jpg",
          caption: "\u201cEco, estudio para \u2018Una ba\xf1ista\u2019\u201d, 1884. Georges Seurat. Fuente: Wikimedia Commons bajo dominio p\xfablico."
        }, {
          src: "/images/heroes-y-villanos/modulo4/conceptos/cosplay.jpg",
          caption: "\u2019Cosplay\u2019 inspirado en la obra de Roy Lichenstein. Carnaval de Dusseld\xf6rf. (2014) Fuente: Wikimedia Commons bajo la licencia CC BY 2.0."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-4-conceptos",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "P\xfablico objetivo:"
                  }), (0, s.jsx)("p", {
                    children: "El concepto hace referencia a un consumidor representativo e ideal al cual se dirige una campa\xf1a o al comprador al que se aspira a seducir con un producto o un servicio."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Sesgo:"
                  }), (0, s.jsx)("p", {
                    children: "Es un peso desproporcionado a favor o en contra de una idea, cosa, persona o grupo en comparaci\xf3n con otra, generalmente de una manera que se considera injusta."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-siete",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "C\xe1mara de eco:"
                  }), (0, s.jsx)("p", {
                    children: "Es la descripci\xf3n metaf\xf3rica de una situaci\xf3n en la que la informaci\xf3n, ideas o creencias son amplificadas por transmisi\xf3n y repetici\xf3n en un sistema \xabcerrado\xbb donde las visiones diferentes o competidoras son censuradas, est\xe1n prohibidas o son minoritariamente representadas."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-seis",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Personificaci\xf3n (cosplay):"
                  }), (0, s.jsx)("p", {
                    children: "La acci\xf3n de evocar la presencia de otra persona por medio de la actuaci\xf3n o el vestuario."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("3")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    5416: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo4/ejercicios/cruela.jpg",
          caption: "Cruela de Vil. Personificaci\xf3n de Kingler Josu\xe9 Otoniel."
        }, {
          src: "/images/heroes-y-villanos/modulo4/ejercicios/mao.jpg",
          caption: "Mao. Personificaci\xf3n de Elizabeth Sarmiento."
        }, {
          src: "/images/heroes-y-villanos/modulo4/ejercicios/lenin.jpg",
          caption: "Lenin. Personificaci\xf3n de Isabel Pa\xf1i."
        }, {
          src: "/images/heroes-y-villanos/modulo4/ejercicios/ursula.jpg",
          caption: "\xdarsula. Personificaci\xf3n de Edi Jim\xe9nez."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = (e[0], e[1], (0, i.useState)(!1)),
            o = a[0],
            c = a[1],
            l = (0, i.useState)(null),
            t = l[0],
            d = l[1],
            u = function (e) {
              c(!o), d(void 0 !== e ? e : null)
            },
            m = function (e) {
              var a;
              for (var o in r) o === e && (a = r[o].src);
              return a
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Analiza los rasgos que m\xe1s identifican al villano/a o enemigo/a, por ejemplo, corte de cabello, rasgos faciales, ropa, expresiones, entre otros."
              }), (0, s.jsx)("li", {
                children: "Elabora un autorretrato fotogr\xe1fico en el que personifiques a tu cm villano/a o enemigo/a, utilizando disfraces, objetos y/o escenograf\xeda."
              }), (0, s.jsx)("li", {
                children: "(Opcional) Edita la imagen en Instagram, PowerPoint o Photoshop."
              }), (0, s.jsx)("li", {
                children: "(Opcional) Agrega elementos aplicando t\xe9cnicas de los primeros 3 m\xf3dulos."
              })]
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales:  C\xe1mara fotogr\xe1fica o celular con c\xe1mara, ropa, disfraces, y otros objetos. (Opcional): PowerPoint, Photoshop o Instagram."
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-4",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  u("0")
                },
                style: {
                  backgroundImage: "url(".concat(m("0"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Cruela de Vil."
                  }), (0, s.jsx)("br", {}), "Personificaci\xf3n de Kingler Josu\xe9 Otoniel"]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-dos",
                onClick: function () {
                  u("1")
                },
                style: {
                  backgroundImage: "url(".concat(m("1"))
                },
                children: (0, s.jsxs)("p", {
                  className: "text-lg-end",
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Mao."
                  }), (0, s.jsx)("br", {}), "Personificaci\xf3n de Elizabeth Sarmiento"]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-tres",
                onClick: function () {
                  u("2")
                },
                style: {
                  backgroundImage: "url(".concat(m("2"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Lenin."
                  }), (0, s.jsx)("br", {}), "Personificaci\xf3n de Isabel Pa\xf1i. "]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-cuatro",
                onClick: function () {
                  u("3")
                },
                style: {
                  backgroundImage: "url(".concat(m("3"))
                },
                children: (0, s.jsxs)("p", {
                  className: "text-lg-end",
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "\xdarsula."
                  }), (0, s.jsx)("br", {}), "Personificaci\xf3n de Edi Jim\xe9nez."]
                })
              }), o && (0, s.jsx)(n.Z, {
                activeImage: t,
                imagenes: r,
                closeImageLightBox: u
              })]
            })]
          })
        }
    },
    4245: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Humanizaci\xf3n del enemigo",
        subtitulo: "Creaci\xf3n de personajes",
        contenido: [o(1169).Z, o(8201).Z, o(9558).Z, o(5416).Z, o(6064).Z, o(6097).Z, o(1167).Z]
      }
    },
    1169: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo te invitaremos a \u201cponerte en los zapatos\u201d de tu enemigo o un personaje que consideres malvado por medio de un ejercicio de personificaci\xf3n y cosplay en el cual resaltar\xe1s los aspectos positivos y humanizantes de tus enemigos. Este ejercicio de creaci\xf3n tiene el objetivo de estimular la empat\xeda reconociendo que aquellas personas que consideramos despreciables o enemigos, son tambi\xe9n humanos con cualidades y defectos."
            }), (0, s.jsx)("img", {
              src: "/images/heroes-y-villanos/banner_mod4.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    1167: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/humanizacion-clase-teorica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/humanizacion-clase-practica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/humanizacion-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n en l\xednea con artista invitado Edi Jimenez"
            })]
          })
        }
      }
    },
    6097: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo4/referentes/leon.jpg",
          caption: "Sin t\xedtulo, 1979. Le\xf3n Ruiz. Fuente: Colecci\xf3n Mambo"
        }, {
          src: "/images/heroes-y-villanos/modulo4/referentes/cindy.png",
          caption: "Self-portrait with sun tan,  2003.  Cindy Sherman. Fuente: Phillips: Photographs 2019. Visita la obra en este enlace: bit.ly/3AGzvG7"
        }, {
          src: "/images/heroes-y-villanos/modulo4/referentes/samuel.jpeg",
          caption: "Self-Portrait as Mao Zedong, from the series Emperor of Africa, 2013. Samuel Fosso. \xa9 Samuel Fosso.  Courtesy Jean Marc Patras, Paris. Visita la obra en este enlace: bit.ly/3ubzqHL"
        }, {
          src: "/images/heroes-y-villanos/modulo4/referentes/morimura.jpg",
          caption: "A Requiem: Infinite Dream / CHE, 2007. Morimura Yasumasa. Visita la obra en este enlace: bit.ly/3zKydbW"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-4",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: " Sin t\xedtulo,"
                  }), " 1979. ", (0, s.jsx)("br", {}), "Leon Ruiz.", (0, s.jsx)("br", {}), " Fuente: Colecci\xf3n MAMBO."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("p", {
                  className: "text-lg-end",
                  children: [(0, s.jsxs)("span", {
                    className: "titulo-obra",
                    children: ["Self-portrait ", (0, s.jsx)("br", {}), "  with sun tan,"]
                  }), (0, s.jsx)("br", {}), "  2003.  Cindy Sherman.", (0, s.jsx)("br", {}), "Fuente: Phillips:", (0, s.jsx)("br", {}), "Photographs 2019. ", (0, s.jsx)("br", {})]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-tres",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsxs)("span", {
                    className: "titulo-obra",
                    children: ["Self-Portrait as Mao Zedong,", (0, s.jsx)("br", {}), " from the series Emperor of Africa, "]
                  }), (0, s.jsx)("br", {}), "2013. ", (0, s.jsx)("br", {}), "Samuel Fosso. ", (0, s.jsx)("br", {}), "\xa9 Samuel Fosso.", (0, s.jsx)("br", {}), "  Courtesy Jean Marc Patras, Paris.", (0, s.jsx)("br", {})]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-cuatro",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("p", {
                  className: "text-lg-end",
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "A Requiem: Infinite Dream / CHE,"
                  }), " 2007.", (0, s.jsx)("br", {}), "Morimura Yasumasa.", (0, s.jsx)("br", {})]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    9558: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En esta sesi\xf3n estudiar\xe1s artistas que usan el m\xe9todo de la personificaci\xf3n: actores y actrices, artistas pl\xe1sticos, performers y modelos, entre otros. Entender\xe1s las implicaciones del uso del cuerpo para representar a otras personas y crear personajes. Investigar\xe1s las acciones loables o admirables realizadas por los personajes y sus rasgos m\xe1s emblem\xe1ticos para realizar un retrato fotogr\xe1fico en el que personifiques tu villanos/a usando disfraces, objetos y escenograf\xeda.  "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/humanizacion-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    8201: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En esta sesi\xf3n tomar\xe1s una postura emp\xe1tica con los personajes que desprecias. Realizar\xe1s de nuevo un cambio de roles para estar ahora del lado del/a enemigo/a o villano/a y tratar de defenderlo/a. Buscar\xe1s caracter\xedsticas o actos admirables del villano/a o enemigo/a e e identificar\xe1s qu\xe9 tipo de personas suelen admirar a estos personajes. Exaltar\xe1s la imagen de los personajes a partir de ejemplos en la historia del arte y en la actualidad. Este m\xf3dulo concluir\xe1 con un retrato en el que personificar\xe1s (cosplay) a tu villano o villana. "
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/humanizacion-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    5162: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo3/conceptos/iconoclastia.jpg",
          caption: "\u201cDestrucci\xf3n de \xedconos en Zurich, Suiza\u201d. Autor desonocido (1524). Fuente: Wikimedia Commons bajo dominio p\xfablico."
        }, {
          src: "/images/heroes-y-villanos/modulo3/conceptos/satira.jpg",
          caption: "Fotograma de la pel\xedcula \u201cEl gran dictador\u201d de Charles Chaplin (1940). Fuente: Wikimedia Commons bajo dominio p\xfablico."
        }, {
          src: "/images/heroes-y-villanos/modulo3/conceptos/calumnia.jpg",
          caption: "\u201cLa Calumnia de Apele\u201d. Sandro Boticelli (1495). Fuente: Wikimedia Commons bajo dominio p\xfablico. "
        }, {
          src: "/images/heroes-y-villanos/modulo3/conceptos/fotomontaje.jpg",
          caption: "\u201cSin t\xedtulo\u201d, 1906. Anton Stankowski. Fuente: Colecci\xf3n MAMBO."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-4-conceptos",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Iconoclastia:"
                  }), (0, s.jsx)("p", {
                    children: "La deliberada destrucci\xf3n de \xedconos, s\xedmbolos o monumentos generalmente por motivos religiosos o pol\xedticos."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "S\xe1tira:"
                  }), (0, s.jsx)("p", {
                    children: " Cr\xedtica a las costumbres de alguien con intenci\xf3n moralizadora, l\xfadica o burlesca."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-siete",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Calumnia:"
                  }), (0, s.jsx)("p", {
                    children: "Acusaci\xf3n o imputaci\xf3n falsa hecha contra alguien con la intenci\xf3n de causarle da\xf1o o de perjudicarle. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-4-seis",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Fotomontaje:"
                  }), (0, s.jsx)("p", {
                    children: "Composici\xf3n fotogr\xe1fica que resulta de la combinaci\xf3n de fragmentos de diversas fotograf\xedas, a veces acompa\xf1adas de dibujos, y que se hace generalmente con intenci\xf3n art\xedstica o publicitaria."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("3")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    9430: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo3/ejercicios/andyw.jpg",
          caption: "Andy Warhol. Fotomontaje de Valentina Santos."
        }, {
          src: "/images/heroes-y-villanos/modulo3/ejercicios/mariaf.jpg",
          caption: "Maria Fernanda Cabal. Fotomontaje de Juliana Ortiz."
        }, {
          src: "/images/heroes-y-villanos/modulo3/ejercicios/hitler.jpg",
          caption: "Hitler intervenci\xf3n. Evelyn Ben\xedtez. "
        }, {
          src: "/images/heroes-y-villanos/modulo3/ejercicios/chuleta.jpg",
          caption: "Simpsons. Fotomontaje de Chuleta Prieto."
        }, {
          src: "/images/heroes-y-villanos/modulo3/ejercicios/evelyn.jpg",
          caption: "Einstein. Fotomontaje de Evelyn Ben\xedtez."
        }, {
          src: "/images/heroes-y-villanos/modulo3/ejercicios/helio.jpg",
          caption: "Helio Oiticica. Fotomontaje de Isabel Pa\xf1i."
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = (e[0], e[1], (0, i.useState)(!1)),
            o = a[0],
            c = a[1],
            l = (0, i.useState)(null),
            t = l[0],
            d = l[1],
            u = function (e) {
              c(!o), d(void 0 !== e ? e : null)
            },
            m = function (e) {
              var a;
              for (var o in r) o === e && (a = r[o].src);
              return a
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Busca medios de comunicaci\xf3n o portales donde se critique al h\xe9roe o hero\xedna escogido/a."
              }), (0, s.jsx)("li", {
                children: "Investiga cu\xe1les han sido sus errores m\xe1s memorables y las razones por las cuales se le critica."
              }), (0, s.jsx)("li", {
                children: "Haz una lista de defectos de este personaje."
              }), (0, s.jsx)("li", {
                children: "Escoge im\xe1genes que representen estos defectos."
              }), (0, s.jsx)("li", {
                children: "Busca una imagen de nuestro personaje."
              })]
            }), (0, s.jsx)("p", {
              className: "titulo-ejercicio",
              children: "Versi\xf3n an\xe1loga"
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: Recortes o fotos impresas del villano o villana, colores, l\xe1pices, marcadores. t\xe9mperas."
            }), (0, s.jsx)("ol", {
              className: "lista-ejercicio-6",
              children: (0, s.jsx)("li", {
                children: "Haz una intervenci\xf3n sobre la foto con los materiales para hacer evidentes los defectos y caracter\xedsticas despreciables de este personaje."
              })
            }), (0, s.jsx)("p", {
              className: "titulo-ejercicio",
              children: "Versi\xf3n digital"
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales:  foto del villano o villana."
            }), (0, s.jsx)("ol", {
              className: "lista-ejercicio-6",
              children: (0, s.jsx)("li", {
                children: "Usa Instagram para intervenir la imagen por medio de stickers, gifs, rayones y texto."
              })
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-6",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-uno",
                onClick: function () {
                  u("0")
                },
                style: {
                  backgroundImage: "url(".concat(m("0"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "pie-foto-ejercicio",
                      children: "Andy Warhol."
                    }), (0, s.jsx)("br", {}), "Fotomontaje de Valentina Santos."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-dos",
                onClick: function () {
                  u("1")
                },
                style: {
                  backgroundImage: "url(".concat(m("1"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "pie-foto-ejercicio",
                      children: "Maria Fernanda Cabal."
                    }), (0, s.jsx)("br", {}), "Fotomontaje de Juliana Ortiz. "]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-tres",
                onClick: function () {
                  u("2")
                },
                style: {
                  backgroundImage: "url(".concat(m("2"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "pie-foto-ejercicio",
                      children: "Hitler intervenci\xf3n."
                    }), (0, s.jsx)("br", {}), "Evelyn Ben\xedtez."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-cuatro",
                onClick: function () {
                  u("3")
                },
                style: {
                  backgroundImage: "url(".concat(m("3"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "pie-foto-ejercicio",
                      children: "Simpsons."
                    }), (0, s.jsx)("br", {}), "Fotomontaje de Chuleta Prieto."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-siete",
                onClick: function () {
                  u("4")
                },
                style: {
                  backgroundImage: "url(".concat(m("4"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "pie-foto-ejercicio",
                      children: "Einstein."
                    }), (0, s.jsx)("br", {}), "Fotomontaje de Evelyn Ben\xedtez."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-ocho",
                onClick: function () {
                  u("5")
                },
                style: {
                  backgroundImage: "url(".concat(m("5"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "pie-foto-ejercicio",
                      children: "Helio Oiticica."
                    }), (0, s.jsx)("br", {}), "Fotomontaje de Isabel Pa\xf1i."]
                  })
                })
              }), o && (0, s.jsx)(n.Z, {
                activeImage: t,
                imagenes: r,
                closeImageLightBox: u
              })]
            })]
          })
        }
    },
    8210: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Iconoclastia",
        subtitulo: "Fotomontaje",
        contenido: [o(2816).Z, o(7700).Z, o(8303).Z, o(9430).Z, o(5162).Z, o(9249).Z, o(1594).Z]
      }
    },
    2816: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo aprender\xe1s sobre diferentes estrategias visuales para atacar, deformar y cuestionar \xeddolos. Identificar\xe1s aspectos negativos o inmorales de tus h\xe9roes para construir una imagen en la que analizar\xe1s cr\xedticamente a los personajes que admiras. Este ejercicio estar\xe1 encaminado a generar un cuestionamiento que te permita ponerte en la posici\xf3n de alguien que piensa diferente a ti y as\xed salir de la c\xe1mara de eco que reafirma tus opiniones personales."
            }), (0, s.jsx)("img", {
              src: "/images/heroes-y-villanos/banner_mod3.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    1594: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/iconoclasta-clase-teorica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/iconoclasta-clase-practica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/iconoclasta-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n en l\xednea con artista invitada Chuleta Prieto"
            })]
          })
        }
      }
    },
    9249: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo3/referentes/remocion.jpg",
          caption: "Remoci\xf3n de trotsky. Autor desconocido. 1919. Imagen de dominio p\xfablico."
        }, {
          src: "/images/heroes-y-villanos/modulo3/referentes/diego.jpg",
          caption: "Sin t\xedtulo, 1973. Diego Arango y Nirma Zarate. Fuente: Colecci\xf3n MAMBO."
        }, {
          src: "/images/heroes-y-villanos/modulo3/referentes/puroveneno.jpg",
          caption: "Sin nombre, s.f. Puro veneno. Imagen de uso libre."
        }, {
          src: "/images/heroes-y-villanos/modulo3/referentes/yves.jpg",
          caption: "Salto al vac\xedo,1960. Yves Klein. \xa9 Harry Shunk and Janos Kender J.Paul Getty Trust. The Getty Research Institute, Los Angeles. (2014.R.20) Visita la obra en este enlace: https://bit.ly/3i757x8"
        }, {
          src: "/images/heroes-y-villanos/modulo3/referentes/jean.jpg",
          caption: "Kim Kardashian, 2015. Jean Paul Goude. Visita la obra en este enlace: https://bit.ly/3COAapH"
        }, {
          src: "/images/heroes-y-villanos/modulo3/referentes/maurice.jpg",
          caption: "Henri Toulouse Lautrec como artista y modelo, 1890. Maurice Ruibert. Fuente: MET Museum. Visita la obra en este enlace: https://bit.ly/3CFJlIT"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-6",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-uno",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Salto al vac\xedo,"
                    }), " 1960. ", (0, s.jsx)("br", {}), " Yves Klein. ", (0, s.jsx)("br", {}), " \xa9 Harry Shunk and Janos Kender J.Paul Getty Trust. The Getty Research Institute, Los Angeles. (2014.R.20) ", (0, s.jsx)("br", {})]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-dos",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Remoci\xf3n de trotsky,"
                    }), " 1919. ", (0, s.jsx)("br", {}), "Autor desconocido.", (0, s.jsx)("br", {}), "  Imagen de dominio p\xfablico."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-tres",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Sin t\xedtulo,"
                    }), "1973. ", (0, s.jsx)("br", {}), "Diego Arango y Nirma Z\xe1rate.", (0, s.jsx)("br", {}), " Fuente: Colecci\xf3n MAMBO."]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-cuatro",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Kim Kardashian,"
                    }), " 2015.", (0, s.jsx)("br", {}), "Jean Paul Goude.", (0, s.jsx)("br", {})]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-cinco",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Sin nombre,"
                    }), " s.f.", (0, s.jsx)("br", {}), "Puro Veneno. Imagen de uso libre.", (0, s.jsx)("br", {}), "Fuente: @purovenenofire"]
                  })
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-6-ocho",
                onClick: function () {
                  d("5")
                },
                style: {
                  backgroundImage: "url(".concat(u("5"))
                },
                children: (0, s.jsx)("div", {
                  className: "text-conceptos",
                  children: (0, s.jsxs)("p", {
                    children: [(0, s.jsx)("span", {
                      className: "titulo-obra",
                      children: "Henri Toulouse Lautrec como artista y modelo,"
                    }), " 1890.", (0, s.jsx)("br", {}), "Maurice Ruibert.", (0, s.jsx)("br", {}), " Fuente: MET Museum. ", (0, s.jsx)("br", {})]
                  })
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    8303: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En esta sesi\xf3n aprender\xe1s sobre la t\xe9cnica del fotomontaje y analizar\xe1s los posters en el contexto del arte pol\xedtico y del deporte. Realizar\xe1s un retrato de su h\xe9roe en el cual se representen sus defectos y sus caracter\xedsticas negativas."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/iconoclasta-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    7700: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En esta sesi\xf3n realizar\xe1s un ejercicio para cambiar los roles y ponerte en el lugar de quienes critican a tus h\xe9roes,  para entender a cada h\xe9roe o hero\xedna como una persona con falencias y defectos. Tomar\xe1s una postura cr\xedtica a trav\xe9s del ataque a los \xeddolos propios por medio de la imagen. Estudiar\xe1s el uso de la iconoclastia y la s\xe1tira para deformar la imagen de h\xe9roes y figuras c\xe9lebres. Adem\xe1s entender\xe1s cu\xe1ndo estas estrategias son herramientas de difamaci\xf3n y calumnia."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/iconoclasta-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    7193: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "H\xe9roes y villanos",
        duracion: ["4 m\xf3dulos", "8 sesiones", "12 horas - 14 horas", "4 ejercicios de creaci\xf3n", "8 videos", "En Espa\xf1ol"],
        autor: "Javier Fern\xe1ndez es un artista multidisciplinario que combina la ilustraci\xf3n, el video, la animaci\xf3n, la m\xfasica y el tatuaje a trav\xe9s de propuestas que utilizan el humor para comunicar mensajes cr\xedticos sobre la sociedad. Es l\xedder de la agrupaci\xf3n Aguas Ardientes que se ha presentado en escenarios como Rock Al Parque y el Festival Hermoso Ruido, con una propuesta que ha tenido mucho \xe9xito en la escena de la m\xfasica local por medio de canciones que hacen s\xe1tira a la cotidianidad colombiana. Trabaja bajo el seud\xf3nimo Monkey the Human como tatuador y su trabajo ha sido reconocido por medios de comunicaci\xf3n como Bac\xe1nika y Cartel Urbano. Ha trabajado en cine, publicidad y proyectos multimedia.",
        arroba: "",
        categoria: "Collage y fotomontaje",
        descripcion: "Este curso te invita a investigar, crear y re\ufb02exionar en torno a la representaci\xf3n de h\xe9roes y villanos en la historia del arte, la publicidad, el cine y los medios de comunicaci\xf3n. Incentivar\xe1 en ti la capacidad para analizar cr\xedticamente a tus \xeddolos y desarrollar empat\xeda con los personajes que rechazas, invit\xe1ndote a trabajar a partir de tus referentes e intereses personales. Aprender\xe1s sobre t\xe9cnicas de creaci\xf3n visual como el collage, el fotomontaje, el ready made y la creaci\xf3n de personajes con el objetivo de fomentar perspectivas cr\xedticas abiertas al di\xe1logo. En este curso producido por el Museo de Arte Moderno de Bogot\xe1, el artista Javier Fern\xe1ndez te guiar\xe1 a trav\xe9s de diferentes t\xe9cnicas de creaci\xf3n visual y gr\xe1\ufb01ca al tiempo que investigas y creas im\xe1genes que cuestionan las ideas preconcebidas sobre tus h\xe9roes y villanos.",
        video: "../video/heroes-villanos-portada.mp4",
        habilidades: ["Collage", "Fotomontaje", "Intervenci\xf3n fotogr\xe1fica", "Lectura de im\xe1genes", "An\xe1lisis visual", "Creaci\xf3n de personajes (Cosplay)", "Investigaci\xf3n para la creaci\xf3n"],
        dirigido: "Este curso est\xe1 dirigido a adolescentes, j\xf3venes y adultos interesados en el arte gr\xe1\ufb01co y la cultura pop. No se requiere ser artista o conocedor de las t\xe9cnicas del dibujo y las artes visuales. Recomendado para estudiantes de bachillerato con intereses en el dise\xf1o, el arte y la publicidad, profesores, artistas visuales y p\xfablico en general interesado en aprender y desarrollar t\xe9cnicas de investigaci\xf3n y creaci\xf3n en torno a la imagen.",
        pie_imagen_titulo: "Declaraci\xf3n de amor a Venezuela,",
        pie_imagen_cuerpo: " 1976. Juan Camilo Uribe. Fuente: Colecci\xf3n MAMBO",
        modulos: [o(1101).Z, o(3531).Z, o(8210).Z, o(4245).Z],
        testimonios: ["\u201cEn \u2018H\xe9roes y Villanos\u2019 aprend\xed a investigar para crear, aprend\xed sobre collage, narraci\xf3n visual y a pensar en el otro.\u201d", "\u201cParticip\xe9 en \u2018H\xe9roes y Villanos\u2019, aprend\xed sobre el uso de la t\xe9cnica del collage, sobre la necesidad de trabajar las distintas dimensiones humanas, tanto aquellas que tienen que ver con las virtudes como con la sombra humana, presentes en cada existencia.\u201d", "\u201cEn el curso \u2018H\xe9roes y Villanos\u2019 con Javier Fern\xe1ndez aprend\xed sobre la Iconoclastia: ruptura de im\xe1gen es a trav\xe9s de t\xe9cnicas art\xedsticas como el fotomontaje\u201d", "\u201cFue una experiencia fascinante, la propuesta result\xf3 clave y como una invitaci\xf3n para revisar la fragilidad humana, la necesidad de no caer en radicalismos y en polaridades que poco construyen.\u201d", "\u201cUna experiencia muy interesante, apropiada para el momento, creativa, motivante, \xfanica.\u201d"]
      }
    },
    333: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Conceptos clave",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo2/conceptos/propaganda.jpg",
          caption: "\u201cCartel de reclutamiento de los Estados Unidos\u201d. James Montgomerya (1917). Fuente: Wikimedia Commons bajo dominio p\xfablico."
        }, {
          src: "/images/heroes-y-villanos/modulo2/conceptos/apropiacionismo.jpg",
          caption: "\u201cLa nueva imagen de Colombia\u201d. Juan Camilo Uribe (1979) Fuente: Colecci\xf3n MAMBO."
        }, {
          src: "/images/heroes-y-villanos/modulo2/conceptos/ready.jpg",
          caption: "\u201cAlacena con zapatos\u201d. Grupo El Sindicato (1970). Fuente. Colecci\xf3n MAMBO"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            },
            m = function (e) {
              var a = r[e].caption;
              return a
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-3",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Propaganda:"
                  }), (0, s.jsx)("p", {
                    children: "Difusi\xf3n o divulgaci\xf3n de informaci\xf3n, ideas u opiniones de car\xe1cter pol\xedtico, religioso, comercial, etc., con la intenci\xf3n de que alguien act\xfae de una determinada manera, piense seg\xfan unas ideas o adquiera un determinado producto."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("0")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-dos",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Apropiacionismo:"
                  }), (0, s.jsx)("p", {
                    children: "Movimiento art\xedstico en el que el artista usa elementos de obras ajenas o de la cultura popular. "
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("1")
                  })]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-3-tres",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("div", {
                  className: "text-conceptos",
                  children: [(0, s.jsx)("h2", {
                    children: "Ready Made:"
                  }), (0, s.jsx)("p", {
                    children: "El arte realizado mediante el uso de objetos cotidianos que no cumplen una funci\xf3n art\xedstica."
                  }), (0, s.jsx)("p", {
                    className: "pie-imagen",
                    children: m("2")
                  })]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    2267: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2004),
        r = o(2642);
      a.Z = {
        name: "Ejercicio de creaci\xf3n",
        contenido: function () {
          return (0, s.jsx)(l, {})
        }
      };
      var c = [{
          src: "/images/heroes-y-villanos/modulo2/ejercicios/mao.jpg",
          caption: "Mao. Fotograf\xeda intervenida por Elizabeth Sarmiento."
        }, {
          src: "/images/heroes-y-villanos/modulo2/ejercicios/garavito.jpg",
          caption: "Garavito. Fotograf\xeda intervenida por Maria Isabel Rodr\xedguez."
        }, {
          src: "/images/heroes-y-villanos/modulo2/ejercicios/uribe.jpg",
          caption: "Uribe. Fotograf\xeda intervenida por Ginna Vel\xe1zquez."
        }, {
          src: "/images/heroes-y-villanos/modulo2/ejercicios/picasso.png",
          caption: "Picasso. Fotograf\xeda intervenida por Laura Zamudio. "
        }],
        l = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            l = (0, i.useState)(!1),
            t = l[0],
            d = l[1],
            u = (0, i.useState)(null),
            m = u[0],
            p = u[1],
            g = function (e) {
              d(!t), p(void 0 !== e ? e : null)
            },
            h = function (e) {
              var a;
              for (var o in c) o === e && (a = c[o].src);
              return a
            };
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsxs)("ol", {
              className: "lista-ejercicio",
              children: [(0, s.jsx)("li", {
                children: "Escoge a un villano o villana."
              }), (0, s.jsx)("li", {
                children: "Investiga cu\xe1les son sus peores haza\xf1as y sus caracter\xedsticas m\xe1s despreciables."
              }), (0, s.jsx)("li", {
                children: "Haz una lista de defectos de este personaje e im\xe1genes que simbolizan estos defectos."
              }), (0, s.jsx)("li", {
                children: "Escoge un evento o momento espec\xedfico de la vida de este personaje que muestre sus defectos."
              }), (0, s.jsx)("li", {
                children: "Busca una imagen del personaje."
              })]
            }), (0, s.jsx)("p", {
              className: "titulo-ejercicio",
              children: "Versi\xf3n an\xe1loga"
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: Recortes o fotos impresas del villano o villana, colores, l\xe1pices, marcadores. T\xe9mperas."
            }), (0, s.jsx)("ol", {
              className: "lista-ejercicio-6",
              children: (0, s.jsx)("li", {
                children: "Haz una intervenci\xf3n sobre la foto con los materiales para hacer evidenciar los defectos y caracter\xedsticas despreciables de este personaje."
              })
            }), (0, s.jsx)("p", {
              className: "titulo-ejercicio",
              children: "Versi\xf3n digital"
            }), (0, s.jsx)("p", {
              className: "materiales pt-3",
              children: "Materiales: foto de nuestro villano o villana. Instagram"
            }), (0, s.jsx)("ol", {
              className: "lista-ejercicio-6",
              children: (0, s.jsx)("li", {
                children: "Usa Instagram para intervenir la imagen por medio de stickers, gifs, rayones y texto."
              })
            }), (0, s.jsx)("p", {
              className: "subtitulo-naranja",
              children: "Revisa los ejercicios de otros participantes"
            }), (0, s.jsxs)("div", {
              className: "galeria-5",
              children: [(0, s.jsxs)("div", {
                className: "modulo-cover-item galeria-5-uno px-0",
                children: [(0, s.jsx)(n.Z, {
                  playsinline: !0,
                  url: "../../../video/armando.mp4",
                  width: "100%",
                  height: "100%",
                  controls: !0,
                  style: {
                    transition: "all 1s"
                  },
                  onEnded: function () {
                    o(!a)
                  },
                  light: "/images/heroes-y-villanos/modulo2/ejercicios/armando.jpg",
                  config: {
                    file: {
                      attributes: {
                        controlsList: "play nodownload"
                      }
                    }
                  }
                }), (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Armando el Patriarcado."
                  }), (0, s.jsx)("br", {}), "Fotograf\xeda intervenida por el artista invitado El Chico Sin Cabello de Pan."]
                })]
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-dos",
                onClick: function () {
                  g("0")
                },
                style: {
                  backgroundImage: "url(".concat(h("0"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Mao."
                  }), (0, s.jsx)("br", {}), "Fotograf\xeda intervenida por Elizabeth Sarmiento."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-seis",
                onClick: function () {
                  g("1")
                },
                style: {
                  backgroundImage: "url(".concat(h("1"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Garavito. "
                  }), (0, s.jsx)("br", {}), "Fotograf\xeda intervenida por Maria Isabel Rodr\xedguez."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cuatro",
                onClick: function () {
                  g("2")
                },
                style: {
                  backgroundImage: "url(".concat(h("2"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Uribe."
                  }), (0, s.jsx)("br", {}), "Fotograf\xeda intervenida por Ginna Vel\xe1zquez."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-siete",
                onClick: function () {
                  g("3")
                },
                style: {
                  backgroundImage: "url(".concat(h("3"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "pie-foto-ejercicio",
                    children: "Picasso."
                  }), (0, s.jsx)("br", {}), "Fotograf\xeda intervenida por Laura Zamudio."]
                })
              }), t && (0, s.jsx)(r.Z, {
                activeImage: m,
                imagenes: c,
                closeImageLightBox: g
              })]
            })]
          })
        }
    },
    3531: function (e, a, o) {
      "use strict";
      a.Z = {
        name: "Villanos",
        subtitulo: "Intervenci\xf3n sobre fotograf\xeda",
        contenido: [o(441).Z, o(8030).Z, o(5783).Z, o(2267).Z, o(333).Z, o(9478).Z, o(1387).Z]
      }
    },
    441: function (e, a, o) {
      "use strict";
      var s = o(5893);
      a.Z = {
        name: "Presentaci\xf3n",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo analizar\xe1s la construcci\xf3n visual de la figura del \u2018enemigo\u2019 y del \u2018malvado\u2019 en el cine, los medios y la publicidad. Identificar\xe1s tus enemigos personales y exaltar\xe1s los aspectos negativos de \xe9stos, por medio de la t\xe9cnica de la fotograf\xeda intervenida. Entender\xe1s c\xf3mo los valores y la moral cambian dependiendo del momento hist\xf3rico y la cultura, y c\xf3mo se reflejan en las representaciones visuales del \u2018malo\u2019 o del \u2018enemigo\u2019."
            }), (0, s.jsx)("img", {
              src: "/images/heroes-y-villanos/banner_mod2.jpg",
              className: "img-fluid"
            })]
          })
        }
      }
    },
    1387: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Recursos adicionales",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/villanos-clase-teorica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase te\xf3rica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/villanos-clase-practica-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Clase pr\xe1ctica en l\xednea"
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/villanos-clase-socializacion.mp4",
              width: "100%",
              height: "50vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            }), (0, s.jsx)("p", {
              className: "titulo-recursos mb-0",
              children: "Socializaci\xf3n con artista invitado El Chico sin cabello de Pan"
            })]
          })
        }
      }
    },
    9478: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = o(7294),
        n = o(2642);
      a.Z = {
        name: "Referentes",
        contenido: function () {
          return (0, s.jsx)(c, {})
        }
      };
      var r = [{
          src: "/images/heroes-y-villanos/modulo2/referentes/martirio.jpg",
          caption: "El martirio de San Sebasti\xe1n, 1980. \xc1lvaro Barrios. Fuente: Colecci\xf3n MAMBO"
        }, {
          src: "/images/heroes-y-villanos/modulo2/referentes/lhooq.jpg",
          caption: "L.H.O.O.Q, 1919. Marcel Duchamp. Fuente: Dominio P\xfablico"
        }, {
          src: "/images/heroes-y-villanos/modulo2/referentes/serie.jpg",
          caption: "Serie interiores, 1983. Fernell Franco. Fuente: Colecci\xf3n MAMBO"
        }, {
          src: "/images/heroes-y-villanos/modulo2/referentes/andy.png",
          caption: "Mao, 1972. Andy Warhol. Fuente: The Art Institute of Chicago. \xa9 2018 The Andy Warhol Foundation for the Visual Arts, Inc. /Artists Rights Society (ARS), New York. Visita la obra en este enlace: bit.ly/3kzVicS"
        }, {
          src: "/images/heroes-y-villanos/modulo2/referentes/barbara.jpg",
          caption: "Not stupid enough, 1997. Barbara Kruger. Fuente: \xa9 Artnet Worldwide Corporation. Visita la obra en este enlace: bit.ly/3o6hnBN"
        }],
        c = function () {
          var e = (0, i.useState)(!1),
            a = e[0],
            o = e[1],
            c = (0, i.useState)(null),
            l = c[0],
            t = c[1],
            d = function (e) {
              o(!a), t(void 0 !== e ? e : null)
            },
            u = function (e) {
              return r[e].src
            };
          return (0, s.jsx)(s.Fragment, {
            children: (0, s.jsxs)("div", {
              className: "galeria-5",
              children: [(0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-uno",
                onClick: function () {
                  d("0")
                },
                style: {
                  backgroundImage: "url(".concat(u("0"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: " El martirio de San Sebasti\xe1n,"
                  }), " 1980. ", (0, s.jsx)("br", {}), " \xc1lvaro Barrios.", (0, s.jsx)("br", {}), " Fuente: Colecci\xf3n MAMBO "]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-dos",
                onClick: function () {
                  d("3")
                },
                style: {
                  backgroundImage: "url(".concat(u("3"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Mao,"
                  }), " 1972.", (0, s.jsx)("br", {}), "Andy Warhol.", (0, s.jsx)("br", {}), "Fuente: The Art Institute of Chicago. ", (0, s.jsx)("br", {}), "\xa9 2018 The Andy Warhol Foundation for the Visual Arts, Inc. / ", (0, s.jsx)("br", {}), "Artists Rights Society (ARS), New York.", (0, s.jsx)("br", {})]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-tres",
                onClick: function () {
                  d("1")
                },
                style: {
                  backgroundImage: "url(".concat(u("1"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "L.H.O.O.Q,"
                  }), " 1919. ", (0, s.jsx)("br", {}), " Marcel Duchamp.", (0, s.jsx)("br", {}), " Fuente: Dominio P\xfablico."]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cuatro",
                onClick: function () {
                  d("4")
                },
                style: {
                  backgroundImage: "url(".concat(u("4"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Not stupid enough,"
                  }), "1997. ", (0, s.jsx)("br", {}), "Barbara Kruger. ", (0, s.jsx)("br", {}), " Fuente: \xa9 Artnet Worldwide Corporation. ", (0, s.jsx)("br", {})]
                })
              }), (0, s.jsx)("div", {
                className: "modulo-cover-item galeria-5-cinco",
                onClick: function () {
                  d("2")
                },
                style: {
                  backgroundImage: "url(".concat(u("2"))
                },
                children: (0, s.jsxs)("p", {
                  children: [(0, s.jsx)("span", {
                    className: "titulo-obra",
                    children: "Serie interiores,"
                  }), " 1983. ", (0, s.jsx)("br", {}), "Fernell Franco.", (0, s.jsx)("br", {}), " Fuente: Colecci\xf3n MAMBO"]
                })
              }), a && (0, s.jsx)(n.Z, {
                activeImage: l,
                imagenes: r,
                closeImageLightBox: d
              })]
            })
          })
        }
    },
    5783: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n pr\xe1ctica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "El componente pr\xe1ctico de este m\xf3dulo se enfoca en la alteraci\xf3n de im\xe1genes, aprender\xe1s c\xf3mo se interviene un retrato para cambiar su significado. Esta semana escoger\xe1s una persona que consideres un villano y por medio de la t\xe9cnica de fotograf\xeda intervenida realizar\xe1s un retrato en el cual se evidencien los motivos para considerar a esta persona como mal\xe9vola o inmoral. Descubrir\xe1s nuevos ejemplos de esta t\xe9cnica en la historia del arte y en la actualidad."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/villanos-clase-practica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    8030: function (e, a, o) {
      "use strict";
      var s = o(5893),
        i = (o(7294), o(2004));
      a.Z = {
        name: "Sesi\xf3n te\xf3rica",
        contenido: function () {
          return (0, s.jsxs)(s.Fragment, {
            children: [(0, s.jsx)("p", {
              className: "pb-lg-3 ",
              children: "En este m\xf3dulo se analizar\xe1s qu\xe9 decisiones visuales se usan para vilificar a una persona y c\xf3mo estas caracter\xedsticas cambian entre las diferentes culturas. Reconocer\xe1s cu\xe1les son los clich\xe9s y arquetipos de los villanos en la historia y en los medios de comunicaci\xf3n, identificando en qu\xe9 tipo de casos el p\xfablico se identifica m\xe1s con los villanos de las historias. Har\xe1s una revisi\xf3n sobre los villanos m\xe1s ic\xf3nicos de la historia como Hitler y Charles Manson, y los de ficci\xf3n como Guas\xf3n, Voldemort, Scar y Satan\xe1s. Analizar\xe1s c\xf3mo ciertos elementos visuales se convierten en s\xedmbolos del mal como, por ejemplo, el bigote de Hitler."
            }), (0, s.jsx)(i.Z, {
              className: "video-modulos ",
              playsinline: !0,
              url: "../../../video/villanos-clase-teorica.mp4",
              width: "100%",
              height: "70vh",
              controls: !0,
              style: {
                transition: "all 1s"
              },
              onEnded: function () {
                setHoverVideo(!hoverVideo)
              },
              config: {
                file: {
                  attributes: {
                    controlsList: "play nodownload"
                  }
                }
              }
            })]
          })
        }
      }
    },
    5552: function (e, a, o) {
      "use strict";
      a.Z = [{
        name: "H\xe9roes y villanos",
        subtitle: "Collage y fotomontaje",
        description: "Aprende sobre la representaci\xf3n de h\xe9roes y villanos en la historia del arte, la publicidad, el cine y los medios de comunicaci\xf3n.",
        contenido: o(7193).Z
      }, {
        name: "Cuerpos desobedientes",
        subtitle: "G\xe9nero y performance",
        description: "Aprende sobre la historia y las t\xe9cnicas no convencionales del arte conceptual y contempor\xe1neo.",
        contenido: o(9265).Z
      }, {
        name: "Gente haciendo cosas raras a distancia",
        subtitle: "Arte conceptual y t\xe9cnicas no convencionales",
        description: "Aprende sobre el performance y la representaci\xf3n del g\xe9nero en la historia del arte y los medios de comunicaci\xf3n.",
        contenido: o(8429).Z
      }]
    },
    1722: function (e, a) {
      "use strict";
      a.Z = function (e) {
        return e.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/["']/g, "").replace(/:\s*/g, " ").replace(/,/g, "").replace(/\//g, "").replace(/\?/g, "").replace(/\\xbf/g, "").split(" ").join("-").toLowerCase()
      }
    },
    4453: function () {}
  }
]);