self["webpackHotUpdate_N_E"]("pages/_app",{

/***/ "./components/header/index.js":
/*!************************************!*\
  !*** ./components/header/index.js ***!
  \************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _submenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./submenu */ "./components/header/submenu.js");
/* harmony import */ var _cursos__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../cursos */ "./cursos/index.js");
/* harmony import */ var _helpers_normalizeString__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../helpers/normalizeString */ "./helpers/normalizeString.js");
/* harmony import */ var _helpers_useIsMobile__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../../helpers/useIsMobile */ "./helpers/useIsMobile.js");
/* module decorator */ module = __webpack_require__.hmd(module);



var _jsxFileName = "C:\\MAMP\\htdocs\\19_mambo\\academia-mambo\\components\\header\\index.js",
    _this = undefined,
    _s = $RefreshSig$();


 //import Head from 'next/head'







var Header = function Header() {
  _s();

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(0),
      height = _useState[0],
      setHeight = _useState[1];

  var _useState2 = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(0),
      prevHeight = _useState2[0],
      setPrevHeight = _useState2[1];

  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false),
      positionScroll = _useState3[0],
      setPositionScroll = _useState3[1];

  var _useState4 = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false),
      menuResponsive = _useState4[0],
      setMenuResponsive = _useState4[1];

  var router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  var isMobile = (0,_helpers_useIsMobile__WEBPACK_IMPORTED_MODULE_7__.default)();
  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(function () {
    if (router.pathname === '/') {
      var mosaico = document.getElementById('galeria-mosaico');
      var mosaicoTop = mosaico.offsetTop;
      window.scrollTo({
        top: mosaicoTop - height,
        behavior: 'smooth'
      });
    } else if (router.pathname === '/mambo-viajero') {
      setHeight(75);
    } else {
      setHeight(147);
    }
  }, [router, prevHeight]);

  var CursosHeader = function CursosHeader() {
    var cursos = [];
    _cursos__WEBPACK_IMPORTED_MODULE_5__.default.forEach(function (item, index) {
      cursos.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
        onClick: function onClick() {
          return setMenuResponsive(false);
        },
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
          href: "./".concat((0,_helpers_normalizeString__WEBPACK_IMPORTED_MODULE_6__.default)(item.name)),
          children: item.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 78
        }, _this)
      }, "cursos-".concat(index), false, {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 9
      }, _this));
    });
    return cursos;
  };

  var ExploraHeader = function ExploraHeader() {
    var options = [{
      name: 'Colección mambo',
      link: 'https://www.mambogota.com/'
    }, {
      name: 'Centro de documentación',
      link: 'https://www.mambogota.com/'
    }, {
      name: 'Contáctanos',
      link: 'mailto: educacion@mambogota.com'
    }];
    var explora = [];
    explora.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
      onClick: function onClick() {
        return setMenuResponsive(false);
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
        href: "./ficha-mambo-viajero/index.html",
        children: "App Aula MAMBO"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 91
      }, _this)
    }, "ficha-mambo-viajero", false, {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 18
    }, _this));
    options.forEach(function (item, index) {
      explora.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
        onClick: function onClick() {
          return setMenuResponsive(false);
        },
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
          href: `.${item.link}`,
          target: "_blank",
          rel: "noopener noreferrer",
          onClick: function onClick() {
            return setMenuResponsive(false);
          },
          children: item.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 64,
          columnNumber: 11
        }, _this)
      }, "explora-".concat(index), false, {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 9
      }, _this));
    });
    return explora;
  };

  var claseMenu = function claseMenu() {
    var clase;

    if (menuResponsive) {
      clase = router.pathname === './mambo-viajero/index.html' ? "header-scroll menu-responsive" : "header menu-responsive";
    } else {
      if (positionScroll) {
        clase = "header-scroll";
      } else {
        clase = "header";
      }

      clase = router.pathname === './mambo-viajero/index.html' ? "header-scroll" : clase;
    }

    return clase;
  };

  var refHeader = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)(function (node) {
    if (node !== null) {
      var ticking = false;

      var scrollSetPosition = function scrollSetPosition(e) {
        if (!ticking) {
          window.requestAnimationFrame(function () {
            if (window.scrollY >= 10) {
              setPositionScroll(true);
            } else {
              setPositionScroll(false);
            }

            ticking = false;
          });
        }

        ticking = true;
      };

      var menuBlue = function menuBlue() {
        var widthHeader = node.getBoundingClientRect().width;

        if (widthHeader > 992) {
          setMenuResponsive(false);
        }
      };

      var heightWhiteSpace = function heightWhiteSpace() {
        var heightHeader;

        if (!isMobile) {
          var scroll = window.scrollY;
          heightHeader = scroll > 0 ? 75 : 147;
        } else {
          heightHeader = node.getBoundingClientRect().height;
        }

        var newHeight = router.pathname === '/mambo-viajero' ? 75 : heightHeader;
        setHeight(newHeight);
        setPrevHeight(newHeight <= prevHeight ? newHeight : height);
      };

      heightWhiteSpace();
      menuBlue();
      window.addEventListener('resize', heightWhiteSpace);
      window.addEventListener('scroll', heightWhiteSpace);
      window.addEventListener('resize', menuBlue);
      window.addEventListener('scroll', scrollSetPosition);
      return function () {
        window.removeEventListener('resize', heightWhiteSpace);
        window.removeEventListener('scroll', heightWhiteSpace);
        window.removeEventListener('resize', menuBlue);
        window.removeEventListener('scroll', scrollSetPosition);
      };
    }
  }, []);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("header", {
      id: "header",
      ref: refHeader,
      className: claseMenu(),
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "d-flex align-item-center pt-3 pb-2",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "logo d-flex align-item-center",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
            href: "./",
            children: "Academia mambo"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 158,
            columnNumber: 13
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
            src: menuResponsive ? "/images/isologo-blanco.svg" : "/images/isologo.svg",
            role: "button",
            onClick: function onClick() {
              return window.location.href = './';
            }
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 159,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 157,
          columnNumber: 11
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "icono-menu ms-auto d-lg-none",
          onClick: function onClick() {
            return setMenuResponsive(!menuResponsive);
          },
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
            alt: "Icono menu",
            className: menuResponsive ? 'd-none' : 'd-block',
            src: "/images/iconos/menu.svg"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 163,
            columnNumber: 13
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("img", {
            alt: "Icono cerrar",
            className: !menuResponsive ? 'd-none' : 'd-block',
            src: "/images/iconos/x.svg"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 164,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 162,
          columnNumber: 11
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 156,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("ul", {
        className: "menu-header",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_submenu__WEBPACK_IMPORTED_MODULE_4__.default, {
          name: "Cursos",
          options: CursosHeader
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 168,
          columnNumber: 11
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_submenu__WEBPACK_IMPORTED_MODULE_4__.default, {
          name: "Explora",
          options: ExploraHeader
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 169,
          columnNumber: 11
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          onClick: function onClick() {
            return setMenuResponsive(false);
          },
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
            href: "./acerca/index.html",
            children: "Acerca de"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 170,
            columnNumber: 56
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 170,
          columnNumber: 11
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("li", {
          className: "ms-lg-auto mt-auto mt-lg-0",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
            href: "",
            className: "btn-iniciar-sesion",
            children: "iniciar sesi\xF3n"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 172,
            columnNumber: 13
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 171,
          columnNumber: 11
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 167,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 151,
      columnNumber: 7
    }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
      className: "white-space",
      style: {
        height: height
      }
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 176,
      columnNumber: 7
    }, _this)]
  }, void 0, true);
};

_s(Header, "WmsVA6Dl7J60b82XYKKjaOg3kv8=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter, _helpers_useIsMobile__WEBPACK_IMPORTED_MODULE_7__.default];
});

_c = Header;
/* harmony default export */ __webpack_exports__["default"] = (Header);

var _c;

$RefreshReg$(_c, "Header");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ App; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var C_MAMP_htdocs_19_mambo_academia_mambo_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Footer */ "./components/Footer.js");
/* harmony import */ var _components_header___WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/header/ */ "./components/header/index.js");
/* harmony import */ var _Store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Store */ "./Store/index.js");
/* harmony import */ var _styles_styles_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../styles/styles.scss */ "./styles/styles.scss");
/* harmony import */ var _styles_styles_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_styles_scss__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var swiper_swiper_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! swiper/swiper.scss */ "./node_modules/swiper/swiper.scss");
/* harmony import */ var swiper_swiper_scss__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(swiper_swiper_scss__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var swiper_components_pagination_pagination_scss__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! swiper/components/pagination/pagination.scss */ "./node_modules/swiper/components/pagination/pagination.scss");
/* harmony import */ var swiper_components_pagination_pagination_scss__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(swiper_components_pagination_pagination_scss__WEBPACK_IMPORTED_MODULE_8__);
/* module decorator */ module = __webpack_require__.hmd(module);



var _jsxFileName = "C:\\MAMP\\htdocs\\19_mambo\\academia-mambo\\pages\\_app.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0,C_MAMP_htdocs_19_mambo_academia_mambo_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }








function App(_ref) {
  var Component = _ref.Component,
      pageProps = _ref.pageProps;
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("title", {
        children: "Academia MAMBO"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
        rel: "icon",
        href: "/favicon.ico"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 17,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_header___WEBPACK_IMPORTED_MODULE_4__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Store__WEBPACK_IMPORTED_MODULE_5__.Provider, {
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, _objectSpread({}, pageProps), void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 17
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 13
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Footer__WEBPACK_IMPORTED_MODULE_3__.default, {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 13
    }, this)]
  }, void 0, true);
}
_c = App;

var _c;

$RefreshReg$(_c, "App");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9oZWFkZXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vX05fRS8uL3BhZ2VzL19hcHAuanMiXSwibmFtZXMiOlsiSGVhZGVyIiwidXNlU3RhdGUiLCJoZWlnaHQiLCJzZXRIZWlnaHQiLCJwcmV2SGVpZ2h0Iiwic2V0UHJldkhlaWdodCIsInBvc2l0aW9uU2Nyb2xsIiwic2V0UG9zaXRpb25TY3JvbGwiLCJtZW51UmVzcG9uc2l2ZSIsInNldE1lbnVSZXNwb25zaXZlIiwicm91dGVyIiwidXNlUm91dGVyIiwiaXNNb2JpbGUiLCJ1c2VJc01vYmlsZSIsInVzZUVmZmVjdCIsInBhdGhuYW1lIiwibW9zYWljbyIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJtb3NhaWNvVG9wIiwib2Zmc2V0VG9wIiwid2luZG93Iiwic2Nyb2xsVG8iLCJ0b3AiLCJiZWhhdmlvciIsIkN1cnNvc0hlYWRlciIsImN1cnNvcyIsIkN1cnNvcyIsIml0ZW0iLCJpbmRleCIsInB1c2giLCJub3JtYWxpemVTdHJpbmciLCJuYW1lIiwiRXhwbG9yYUhlYWRlciIsIm9wdGlvbnMiLCJsaW5rIiwiZXhwbG9yYSIsImZvckVhY2giLCJjbGFzZU1lbnUiLCJjbGFzZSIsInJlZkhlYWRlciIsInVzZUNhbGxiYWNrIiwibm9kZSIsInRpY2tpbmciLCJzY3JvbGxTZXRQb3NpdGlvbiIsImUiLCJyZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJzY3JvbGxZIiwibWVudUJsdWUiLCJ3aWR0aEhlYWRlciIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsIndpZHRoIiwiaGVpZ2h0V2hpdGVTcGFjZSIsImhlaWdodEhlYWRlciIsInNjcm9sbCIsIm5ld0hlaWdodCIsImFkZEV2ZW50TGlzdGVuZXIiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwibG9jYXRpb24iLCJocmVmIiwiQXBwIiwiQ29tcG9uZW50IiwicGFnZVByb3BzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtDQUVBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTUEsTUFBTSxHQUFHLFNBQVRBLE1BQVMsR0FBTTtBQUFBOztBQUFBLGtCQUNTQywrQ0FBUSxDQUFDLENBQUQsQ0FEakI7QUFBQSxNQUNaQyxNQURZO0FBQUEsTUFDSkMsU0FESTs7QUFBQSxtQkFFaUJGLCtDQUFRLENBQUMsQ0FBRCxDQUZ6QjtBQUFBLE1BRVpHLFVBRlk7QUFBQSxNQUVBQyxhQUZBOztBQUFBLG1CQUd5QkosK0NBQVEsQ0FBQyxLQUFELENBSGpDO0FBQUEsTUFHWkssY0FIWTtBQUFBLE1BR0lDLGlCQUhKOztBQUFBLG1CQUl5Qk4sK0NBQVEsQ0FBQyxLQUFELENBSmpDO0FBQUEsTUFJWk8sY0FKWTtBQUFBLE1BSUlDLGlCQUpKOztBQUtuQixNQUFNQyxNQUFNLEdBQUdDLHNEQUFTLEVBQXhCO0FBQ0EsTUFBTUMsUUFBUSxHQUFHQyw2REFBVyxFQUE1QjtBQUVBQyxrREFBUyxDQUFDLFlBQU07QUFFZCxRQUFJSixNQUFNLENBQUNLLFFBQVAsS0FBb0IsR0FBeEIsRUFBNkI7QUFDM0IsVUFBTUMsT0FBTyxHQUFHQyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsaUJBQXhCLENBQWhCO0FBQ0EsVUFBTUMsVUFBVSxHQUFHSCxPQUFPLENBQUNJLFNBQTNCO0FBRUFDLFlBQU0sQ0FBQ0MsUUFBUCxDQUFnQjtBQUNkQyxXQUFHLEVBQUVKLFVBQVUsR0FBR2pCLE1BREo7QUFFZHNCLGdCQUFRLEVBQUU7QUFGSSxPQUFoQjtBQUlELEtBUkQsTUFRTSxJQUFJZCxNQUFNLENBQUNLLFFBQVAsS0FBb0IsZ0JBQXhCLEVBQTBDO0FBQzlDWixlQUFTLENBQUMsRUFBRCxDQUFUO0FBQ0QsS0FGSyxNQUVDO0FBQ0xBLGVBQVMsQ0FBQyxHQUFELENBQVQ7QUFDRDtBQUNGLEdBZlEsRUFlTixDQUFDTyxNQUFELEVBQVNOLFVBQVQsQ0FmTSxDQUFUOztBQWtCQSxNQUFNcUIsWUFBWSxHQUFHLFNBQWZBLFlBQWUsR0FBTTtBQUN6QixRQUFJQyxNQUFNLEdBQUcsRUFBYjtBQUNBQyx3REFBQSxDQUFlLFVBQUNDLElBQUQsRUFBT0MsS0FBUCxFQUFpQjtBQUM5QkgsWUFBTSxDQUFDSSxJQUFQLGVBQ0U7QUFBNEIsZUFBTyxFQUFFO0FBQUEsaUJBQU1yQixpQkFBaUIsQ0FBQyxLQUFELENBQXZCO0FBQUEsU0FBckM7QUFBQSwrQkFBcUU7QUFBRyxjQUFJLGFBQU1zQixpRUFBZSxDQUFDSCxJQUFJLENBQUNJLElBQU4sQ0FBckIsQ0FBUDtBQUFBLG9CQUE0Q0osSUFBSSxDQUFDSTtBQUFqRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXJFLDBCQUFtQkgsS0FBbkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBRUQsS0FIRDtBQUlBLFdBQU9ILE1BQVA7QUFDRCxHQVBEOztBQVNBLE1BQU1PLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsR0FBTTtBQUMxQixRQUFJQyxPQUFPLEdBQUcsQ0FDWjtBQUNFRixVQUFJLEVBQUUsaUJBRFI7QUFFRUcsVUFBSSxFQUFFO0FBRlIsS0FEWSxFQUtaO0FBQ0VILFVBQUksRUFBRSx5QkFEUjtBQUVFRyxVQUFJLEVBQUU7QUFGUixLQUxZLEVBU1o7QUFDRUgsVUFBSSxFQUFFLGFBRFI7QUFFRUcsVUFBSSxFQUFFO0FBRlIsS0FUWSxDQUFkO0FBYUEsUUFBSUMsT0FBTyxHQUFHLEVBQWQ7QUFDQUEsV0FBTyxDQUFDTixJQUFSLGVBQWE7QUFBZ0MsYUFBTyxFQUFFO0FBQUEsZUFBTXJCLGlCQUFpQixDQUFDLEtBQUQsQ0FBdkI7QUFBQSxPQUF6QztBQUFBLDZCQUF5RTtBQUFHLFlBQUksd0JBQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBekU7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQUFiO0FBQ0F5QixXQUFPLENBQUNHLE9BQVIsQ0FBZ0IsVUFBQ1QsSUFBRCxFQUFPQyxLQUFQLEVBQWlCO0FBQy9CTyxhQUFPLENBQUNOLElBQVIsZUFDRTtBQUE2QixlQUFPLEVBQUU7QUFBQSxpQkFBTXJCLGlCQUFpQixDQUFDLEtBQUQsQ0FBdkI7QUFBQSxTQUF0QztBQUFBLCtCQUNFO0FBQUcsY0FBSSxFQUFFbUIsSUFBSSxDQUFDTyxJQUFkO0FBQW9CLGdCQUFNLEVBQUMsUUFBM0I7QUFBb0MsYUFBRyxFQUFDLHFCQUF4QztBQUE4RCxpQkFBTyxFQUFFO0FBQUEsbUJBQU0xQixpQkFBaUIsQ0FBQyxLQUFELENBQXZCO0FBQUEsV0FBdkU7QUFBQSxvQkFBeUdtQixJQUFJLENBQUNJO0FBQTlHO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERiwyQkFBb0JILEtBQXBCO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERjtBQU1ELEtBUEQ7QUFRQSxXQUFPTyxPQUFQO0FBQ0QsR0F6QkQ7O0FBMkJBLE1BQU1FLFNBQVMsR0FBRyxTQUFaQSxTQUFZLEdBQU07QUFDdEIsUUFBSUMsS0FBSjs7QUFDQSxRQUFJL0IsY0FBSixFQUFvQjtBQUNsQitCLFdBQUssR0FBRzdCLE1BQU0sQ0FBQ0ssUUFBUCxLQUFvQixnQkFBcEIsR0FDSiwrQkFESSxHQUVKLHdCQUZKO0FBR0QsS0FKRCxNQUlPO0FBQ0wsVUFBSVQsY0FBSixFQUFvQjtBQUNsQmlDLGFBQUssR0FBRyxlQUFSO0FBQ0QsT0FGRCxNQUVPO0FBQ0xBLGFBQUssR0FBRyxRQUFSO0FBQ0Q7O0FBRURBLFdBQUssR0FBRzdCLE1BQU0sQ0FBQ0ssUUFBUCxLQUFvQixnQkFBcEIsR0FBdUMsZUFBdkMsR0FBeUR3QixLQUFqRTtBQUNEOztBQUNELFdBQU9BLEtBQVA7QUFDRCxHQWhCRDs7QUFtQkEsTUFBTUMsU0FBUyxHQUFHQyxrREFBVyxDQUFDLFVBQUNDLElBQUQsRUFBVTtBQUN0QyxRQUFJQSxJQUFJLEtBQUssSUFBYixFQUFtQjtBQUNqQixVQUFJQyxPQUFPLEdBQUcsS0FBZDs7QUFFQSxVQUFNQyxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUNDLENBQUQsRUFBTztBQUMvQixZQUFJLENBQUNGLE9BQUwsRUFBYztBQUNadEIsZ0JBQU0sQ0FBQ3lCLHFCQUFQLENBQTZCLFlBQVk7QUFDdkMsZ0JBQUl6QixNQUFNLENBQUMwQixPQUFQLElBQWtCLEVBQXRCLEVBQTBCO0FBQ3hCeEMsK0JBQWlCLENBQUMsSUFBRCxDQUFqQjtBQUNELGFBRkQsTUFFTztBQUNMQSwrQkFBaUIsQ0FBQyxLQUFELENBQWpCO0FBQ0Q7O0FBQ0RvQyxtQkFBTyxHQUFHLEtBQVY7QUFDRCxXQVBEO0FBUUQ7O0FBQ0RBLGVBQU8sR0FBRyxJQUFWO0FBQ0QsT0FaRDs7QUFjQSxVQUFNSyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxHQUFNO0FBQ3JCLFlBQU1DLFdBQVcsR0FBR1AsSUFBSSxDQUFDUSxxQkFBTCxHQUE2QkMsS0FBakQ7O0FBQ0EsWUFBSUYsV0FBVyxHQUFHLEdBQWxCLEVBQXVCO0FBQ3JCeEMsMkJBQWlCLENBQUMsS0FBRCxDQUFqQjtBQUNEO0FBQ0YsT0FMRDs7QUFPQSxVQUFNMkMsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixHQUFNO0FBQzdCLFlBQUlDLFlBQUo7O0FBQ0EsWUFBSSxDQUFDekMsUUFBTCxFQUFlO0FBQ2IsY0FBTTBDLE1BQU0sR0FBR2pDLE1BQU0sQ0FBQzBCLE9BQXRCO0FBQ0FNLHNCQUFZLEdBQUdDLE1BQU0sR0FBRyxDQUFULEdBQWEsRUFBYixHQUFrQixHQUFqQztBQUNELFNBSEQsTUFHTztBQUNMRCxzQkFBWSxHQUFHWCxJQUFJLENBQUNRLHFCQUFMLEdBQTZCaEQsTUFBNUM7QUFDRDs7QUFFRCxZQUFNcUQsU0FBUyxHQUFHN0MsTUFBTSxDQUFDSyxRQUFQLEtBQW9CLGdCQUFwQixHQUF1QyxFQUF2QyxHQUE0Q3NDLFlBQTlEO0FBQ0FsRCxpQkFBUyxDQUFDb0QsU0FBRCxDQUFUO0FBQ0FsRCxxQkFBYSxDQUFDa0QsU0FBUyxJQUFJbkQsVUFBYixHQUEwQm1ELFNBQTFCLEdBQXNDckQsTUFBdkMsQ0FBYjtBQUNELE9BWkQ7O0FBZUFrRCxzQkFBZ0I7QUFDaEJKLGNBQVE7QUFDUjNCLFlBQU0sQ0FBQ21DLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDSixnQkFBbEM7QUFDQS9CLFlBQU0sQ0FBQ21DLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDSixnQkFBbEM7QUFDQS9CLFlBQU0sQ0FBQ21DLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDUixRQUFsQztBQUNBM0IsWUFBTSxDQUFDbUMsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0NaLGlCQUFsQztBQUdBLGFBQU8sWUFBTTtBQUNYdkIsY0FBTSxDQUFDb0MsbUJBQVAsQ0FBMkIsUUFBM0IsRUFBcUNMLGdCQUFyQztBQUNBL0IsY0FBTSxDQUFDb0MsbUJBQVAsQ0FBMkIsUUFBM0IsRUFBcUNMLGdCQUFyQztBQUNBL0IsY0FBTSxDQUFDb0MsbUJBQVAsQ0FBMkIsUUFBM0IsRUFBcUNULFFBQXJDO0FBQ0EzQixjQUFNLENBQUNvQyxtQkFBUCxDQUEyQixRQUEzQixFQUFxQ2IsaUJBQXJDO0FBQ0QsT0FMRDtBQU1EO0FBQ0YsR0F2RDRCLEVBdUQxQixFQXZEMEIsQ0FBN0I7QUEwREEsc0JBQ0U7QUFBQSw0QkFDRTtBQUNFLFFBQUUsRUFBQyxRQURMO0FBRUUsU0FBRyxFQUFFSixTQUZQO0FBR0UsZUFBUyxFQUFFRixTQUFTLEVBSHRCO0FBQUEsOEJBS0U7QUFBSyxpQkFBUyxFQUFDLG9DQUFmO0FBQUEsZ0NBQ0U7QUFBSyxtQkFBUyxFQUFDLCtCQUFmO0FBQUEsa0NBQ0U7QUFBRyxnQkFBSSxFQUFDLEdBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFFRTtBQUFLLGVBQUcsRUFBRTlCLGNBQWMsR0FBRyw0QkFBSCxHQUFrQyxxQkFBMUQ7QUFBaUYsZ0JBQUksRUFBQyxRQUF0RjtBQUErRixtQkFBTyxFQUFFO0FBQUEscUJBQU1hLE1BQU0sQ0FBQ3FDLFFBQVAsQ0FBZ0JDLElBQWhCLEdBQXVCLEdBQTdCO0FBQUE7QUFBeEc7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREYsZUFNRTtBQUFLLG1CQUFTLEVBQUMsOEJBQWY7QUFBOEMsaUJBQU8sRUFBRTtBQUFBLG1CQUFNbEQsaUJBQWlCLENBQUMsQ0FBQ0QsY0FBRixDQUF2QjtBQUFBLFdBQXZEO0FBQUEsa0NBQ0U7QUFBSyxlQUFHLEVBQUMsWUFBVDtBQUFzQixxQkFBUyxFQUFFQSxjQUFjLEdBQUcsUUFBSCxHQUFjLFNBQTdEO0FBQXdFLGVBQUcsRUFBQztBQUE1RTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBRUU7QUFBSyxlQUFHLEVBQUMsY0FBVDtBQUF3QixxQkFBUyxFQUFFLENBQUNBLGNBQUQsR0FBa0IsUUFBbEIsR0FBNkIsU0FBaEU7QUFBMkUsZUFBRyxFQUFDO0FBQS9FO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5GO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUxGLGVBZ0JFO0FBQUksaUJBQVMsRUFBQyxhQUFkO0FBQUEsZ0NBQ0UsOERBQUMsNkNBQUQ7QUFBUyxjQUFJLEVBQUMsUUFBZDtBQUF1QixpQkFBTyxFQUFFaUI7QUFBaEM7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERixlQUVFLDhEQUFDLDZDQUFEO0FBQVMsY0FBSSxFQUFDLFNBQWQ7QUFBd0IsaUJBQU8sRUFBRVE7QUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGRixlQUdFO0FBQUksaUJBQU8sRUFBRTtBQUFBLG1CQUFNeEIsaUJBQWlCLENBQUMsS0FBRCxDQUF2QjtBQUFBLFdBQWI7QUFBQSxpQ0FBNkM7QUFBRyxnQkFBSSxFQUFDLFNBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFIRixlQUlFO0FBQUksbUJBQVMsRUFBQyw0QkFBZDtBQUFBLGlDQUNFO0FBQUcsZ0JBQUksRUFBQyxFQUFSO0FBQVcscUJBQVMsRUFBQyxvQkFBckI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQWhCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFERixlQTBCRTtBQUFLLGVBQVMsRUFBQyxhQUFmO0FBQTZCLFdBQUssRUFBRTtBQUFFUCxjQUFNLEVBQUVBO0FBQVY7QUFBcEM7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQTFCRjtBQUFBLGtCQURGO0FBK0JELENBMUtEOztHQUFNRixNO1VBS1dXLGtELEVBQ0VFLHlEOzs7S0FOYmIsTTtBQTRLTiwrREFBZUEsTUFBZjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDckxBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRWUsU0FBUzRELEdBQVQsT0FBdUM7QUFBQSxNQUF4QkMsU0FBd0IsUUFBeEJBLFNBQXdCO0FBQUEsTUFBYkMsU0FBYSxRQUFiQSxTQUFhO0FBQ2xELHNCQUNJO0FBQUEsNEJBQ0ksOERBQUMsa0RBQUQ7QUFBQSw4QkFDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBRUk7QUFBRyxXQUFHLEVBQUMsTUFBUDtBQUFjLFlBQUksRUFBQztBQUFuQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREosZUFNSSw4REFBQyx3REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBTkosZUFPSSw4REFBQyw0Q0FBRDtBQUFBLDZCQUNJLDhEQUFDLFNBQUQsb0JBQWVBLFNBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFQSixlQVVJLDhEQUFDLHVEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFWSjtBQUFBLGtCQURKO0FBY0g7S0FmdUJGLEciLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC4xODY3OGY3YmI4NzVlZjk3YWQ0ZS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IExpbmsgZnJvbSAnbmV4dC9saW5rJ1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcidcclxuLy9pbXBvcnQgSGVhZCBmcm9tICduZXh0L2hlYWQnXHJcbmltcG9ydCBSZWFjdCwgeyB1c2VDYWxsYmFjaywgdXNlRWZmZWN0LCB1c2VSZWYsIHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBTdWJtZW51IGZyb20gXCIuL3N1Ym1lbnVcIjtcclxuaW1wb3J0IEN1cnNvcyBmcm9tICcuLi8uLi9jdXJzb3MnO1xyXG5pbXBvcnQgbm9ybWFsaXplU3RyaW5nIGZyb20gJy4uLy4uL2hlbHBlcnMvbm9ybWFsaXplU3RyaW5nJztcclxuaW1wb3J0IHVzZUlzTW9iaWxlIGZyb20gJy4vLi4vLi4vaGVscGVycy91c2VJc01vYmlsZSc7XHJcblxyXG5jb25zdCBIZWFkZXIgPSAoKSA9PiB7XHJcbiAgY29uc3QgW2hlaWdodCwgc2V0SGVpZ2h0XSA9IHVzZVN0YXRlKDApO1xyXG4gIGNvbnN0IFtwcmV2SGVpZ2h0LCBzZXRQcmV2SGVpZ2h0XSA9IHVzZVN0YXRlKDApO1xyXG4gIGNvbnN0IFtwb3NpdGlvblNjcm9sbCwgc2V0UG9zaXRpb25TY3JvbGxdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IFttZW51UmVzcG9uc2l2ZSwgc2V0TWVudVJlc3BvbnNpdmVdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpXHJcbiAgY29uc3QgaXNNb2JpbGUgPSB1c2VJc01vYmlsZSgpO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgXHJcbiAgICBpZiAocm91dGVyLnBhdGhuYW1lID09PSAnLycpIHtcclxuICAgICAgY29uc3QgbW9zYWljbyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdnYWxlcmlhLW1vc2FpY28nKTtcclxuICAgICAgY29uc3QgbW9zYWljb1RvcCA9IG1vc2FpY28ub2Zmc2V0VG9wO1xyXG5cclxuICAgICAgd2luZG93LnNjcm9sbFRvKHtcclxuICAgICAgICB0b3A6IG1vc2FpY29Ub3AgLSBoZWlnaHQsXHJcbiAgICAgICAgYmVoYXZpb3I6ICdzbW9vdGgnXHJcbiAgICAgIH0pO1xyXG4gICAgfWVsc2UgaWYgKHJvdXRlci5wYXRobmFtZSA9PT0gJy9tYW1iby12aWFqZXJvJykge1xyXG4gICAgICBzZXRIZWlnaHQoNzUpXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRIZWlnaHQoMTQ3KVxyXG4gICAgfVxyXG4gIH0sIFtyb3V0ZXIsIHByZXZIZWlnaHRdKVxyXG5cclxuXHJcbiAgY29uc3QgQ3Vyc29zSGVhZGVyID0gKCkgPT4ge1xyXG4gICAgbGV0IGN1cnNvcyA9IFtdO1xyXG4gICAgQ3Vyc29zLmZvckVhY2goKGl0ZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgIGN1cnNvcy5wdXNoKFxyXG4gICAgICAgIDxsaSBrZXk9e2BjdXJzb3MtJHtpbmRleH1gfSBvbkNsaWNrPXsoKSA9PiBzZXRNZW51UmVzcG9uc2l2ZShmYWxzZSl9PjxhIGhyZWY9e2AvJHtub3JtYWxpemVTdHJpbmcoaXRlbS5uYW1lKX1gfT57aXRlbS5uYW1lfTwvYT48L2xpPiAgICAgIClcclxuICAgIH0pXHJcbiAgICByZXR1cm4gY3Vyc29zO1xyXG4gIH1cclxuXHJcbiAgY29uc3QgRXhwbG9yYUhlYWRlciA9ICgpID0+IHtcclxuICAgIGxldCBvcHRpb25zID0gW1xyXG4gICAgICB7XHJcbiAgICAgICAgbmFtZTogJ0NvbGVjY2nDs24gbWFtYm8nLFxyXG4gICAgICAgIGxpbms6ICdodHRwczovL3d3dy5tYW1ib2dvdGEuY29tLydcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIG5hbWU6ICdDZW50cm8gZGUgZG9jdW1lbnRhY2nDs24nLFxyXG4gICAgICAgIGxpbms6ICdodHRwczovL3d3dy5tYW1ib2dvdGEuY29tLydcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIG5hbWU6ICdDb250w6FjdGFub3MnLFxyXG4gICAgICAgIGxpbms6ICdtYWlsdG86IGVkdWNhY2lvbkBtYW1ib2dvdGEuY29tJ1xyXG4gICAgICB9XVxyXG4gICAgbGV0IGV4cGxvcmEgPSBbXTtcclxuICAgIGV4cGxvcmEucHVzaCg8bGkga2V5PXtgZmljaGEtbWFtYm8tdmlhamVyb2B9IG9uQ2xpY2s9eygpID0+IHNldE1lbnVSZXNwb25zaXZlKGZhbHNlKX0+PGEgaHJlZj17YC9maWNoYS1tYW1iby12aWFqZXJvYH0gPkFwcCBBdWxhIE1BTUJPPC9hPjwvbGk+KVxyXG4gICAgb3B0aW9ucy5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xyXG4gICAgICBleHBsb3JhLnB1c2goXHJcbiAgICAgICAgPGxpIGtleT17YGV4cGxvcmEtJHtpbmRleH1gfSBvbkNsaWNrPXsoKSA9PiBzZXRNZW51UmVzcG9uc2l2ZShmYWxzZSl9PlxyXG4gICAgICAgICAgPGEgaHJlZj17aXRlbS5saW5rfSB0YXJnZXQ9XCJfYmxhbmtcIiByZWw9XCJub29wZW5lciBub3JlZmVycmVyXCIgb25DbGljaz17KCkgPT4gc2V0TWVudVJlc3BvbnNpdmUoZmFsc2UpfSA+e2l0ZW0ubmFtZX08L2E+XHJcbiAgICAgICAgPC9saT5cclxuXHJcbiAgICAgIClcclxuICAgIH0pXHJcbiAgICByZXR1cm4gZXhwbG9yYTtcclxuICB9XHJcblxyXG4gIGNvbnN0IGNsYXNlTWVudSA9ICgpID0+IHtcclxuICAgIGxldCBjbGFzZTtcclxuICAgIGlmIChtZW51UmVzcG9uc2l2ZSkge1xyXG4gICAgICBjbGFzZSA9IHJvdXRlci5wYXRobmFtZSA9PT0gJy9tYW1iby12aWFqZXJvJ1xyXG4gICAgICAgID8gXCJoZWFkZXItc2Nyb2xsIG1lbnUtcmVzcG9uc2l2ZVwiXHJcbiAgICAgICAgOiBcImhlYWRlciBtZW51LXJlc3BvbnNpdmVcIjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmIChwb3NpdGlvblNjcm9sbCkge1xyXG4gICAgICAgIGNsYXNlID0gXCJoZWFkZXItc2Nyb2xsXCI7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY2xhc2UgPSBcImhlYWRlclwiO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICBjbGFzZSA9IHJvdXRlci5wYXRobmFtZSA9PT0gJy9tYW1iby12aWFqZXJvJyA/IFwiaGVhZGVyLXNjcm9sbFwiIDogY2xhc2VcclxuICAgIH1cclxuICAgIHJldHVybiBjbGFzZVxyXG4gIH1cclxuXHJcblxyXG4gIGNvbnN0IHJlZkhlYWRlciA9IHVzZUNhbGxiYWNrKChub2RlKSA9PiB7XHJcbiAgICBpZiAobm9kZSAhPT0gbnVsbCkge1xyXG4gICAgICBsZXQgdGlja2luZyA9IGZhbHNlO1xyXG5cclxuICAgICAgY29uc3Qgc2Nyb2xsU2V0UG9zaXRpb24gPSAoZSkgPT4ge1xyXG4gICAgICAgIGlmICghdGlja2luZykge1xyXG4gICAgICAgICAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGlmICh3aW5kb3cuc2Nyb2xsWSA+PSAxMCkge1xyXG4gICAgICAgICAgICAgIHNldFBvc2l0aW9uU2Nyb2xsKHRydWUpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHNldFBvc2l0aW9uU2Nyb2xsKGZhbHNlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGlja2luZyA9IHRydWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IG1lbnVCbHVlID0gKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHdpZHRoSGVhZGVyID0gbm9kZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aDtcclxuICAgICAgICBpZiAod2lkdGhIZWFkZXIgPiA5OTIpIHtcclxuICAgICAgICAgIHNldE1lbnVSZXNwb25zaXZlKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IGhlaWdodFdoaXRlU3BhY2UgPSAoKSA9PiB7XHJcbiAgICAgICAgbGV0IGhlaWdodEhlYWRlcjtcclxuICAgICAgICBpZiAoIWlzTW9iaWxlKSB7XHJcbiAgICAgICAgICBjb25zdCBzY3JvbGwgPSB3aW5kb3cuc2Nyb2xsWTtcclxuICAgICAgICAgIGhlaWdodEhlYWRlciA9IHNjcm9sbCA+IDAgPyA3NSA6IDE0NztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgaGVpZ2h0SGVhZGVyID0gbm9kZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5oZWlnaHRcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgY29uc3QgbmV3SGVpZ2h0ID0gcm91dGVyLnBhdGhuYW1lID09PSAnL21hbWJvLXZpYWplcm8nID8gNzUgOiBoZWlnaHRIZWFkZXI7XHJcbiAgICAgICAgc2V0SGVpZ2h0KG5ld0hlaWdodCk7XHJcbiAgICAgICAgc2V0UHJldkhlaWdodChuZXdIZWlnaHQgPD0gcHJldkhlaWdodCA/IG5ld0hlaWdodCA6IGhlaWdodClcclxuICAgICAgfVxyXG5cclxuXHJcbiAgICAgIGhlaWdodFdoaXRlU3BhY2UoKVxyXG4gICAgICBtZW51Qmx1ZSgpXHJcbiAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCBoZWlnaHRXaGl0ZVNwYWNlKVxyXG4gICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgaGVpZ2h0V2hpdGVTcGFjZSlcclxuICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIG1lbnVCbHVlKVxyXG4gICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgc2Nyb2xsU2V0UG9zaXRpb24pO1xyXG5cclxuXHJcbiAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIGhlaWdodFdoaXRlU3BhY2UpXHJcbiAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIGhlaWdodFdoaXRlU3BhY2UpXHJcbiAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIG1lbnVCbHVlKVxyXG4gICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCBzY3JvbGxTZXRQb3NpdGlvbik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9LCBbXSlcclxuXHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8PlxyXG4gICAgICA8aGVhZGVyXHJcbiAgICAgICAgaWQ9J2hlYWRlcidcclxuICAgICAgICByZWY9e3JlZkhlYWRlcn1cclxuICAgICAgICBjbGFzc05hbWU9e2NsYXNlTWVudSgpfVxyXG4gICAgICA+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLWZsZXggYWxpZ24taXRlbS1jZW50ZXIgcHQtMyBwYi0yXCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxvZ28gZC1mbGV4IGFsaWduLWl0ZW0tY2VudGVyXCI+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIvXCI+QWNhZGVtaWEgbWFtYm88L2E+XHJcbiAgICAgICAgICAgIDxpbWcgc3JjPXttZW51UmVzcG9uc2l2ZSA/IFwiL2ltYWdlcy9pc29sb2dvLWJsYW5jby5zdmdcIiA6IFwiL2ltYWdlcy9pc29sb2dvLnN2Z1wifSByb2xlPVwiYnV0dG9uXCIgb25DbGljaz17KCkgPT4gd2luZG93LmxvY2F0aW9uLmhyZWYgPSAnLyd9IC8+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT0naWNvbm8tbWVudSBtcy1hdXRvIGQtbGctbm9uZScgb25DbGljaz17KCkgPT4gc2V0TWVudVJlc3BvbnNpdmUoIW1lbnVSZXNwb25zaXZlKX0+XHJcbiAgICAgICAgICAgIDxpbWcgYWx0PSdJY29ubyBtZW51JyBjbGFzc05hbWU9e21lbnVSZXNwb25zaXZlID8gJ2Qtbm9uZScgOiAnZC1ibG9jayd9IHNyYz1cIi9pbWFnZXMvaWNvbm9zL21lbnUuc3ZnXCIgLz5cclxuICAgICAgICAgICAgPGltZyBhbHQ9J0ljb25vIGNlcnJhcicgY2xhc3NOYW1lPXshbWVudVJlc3BvbnNpdmUgPyAnZC1ub25lJyA6ICdkLWJsb2NrJ30gc3JjPVwiL2ltYWdlcy9pY29ub3MveC5zdmdcIiAvPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPHVsIGNsYXNzTmFtZT1cIm1lbnUtaGVhZGVyXCI+XHJcbiAgICAgICAgICA8U3VibWVudSBuYW1lPVwiQ3Vyc29zXCIgb3B0aW9ucz17Q3Vyc29zSGVhZGVyfSAvPlxyXG4gICAgICAgICAgPFN1Ym1lbnUgbmFtZT1cIkV4cGxvcmFcIiBvcHRpb25zPXtFeHBsb3JhSGVhZGVyfSAvPlxyXG4gICAgICAgICAgPGxpIG9uQ2xpY2s9eygpID0+IHNldE1lbnVSZXNwb25zaXZlKGZhbHNlKX0+PGEgaHJlZj1cIi9hY2VyY2FcIj5BY2VyY2EgZGU8L2E+PC9saT5cclxuICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJtcy1sZy1hdXRvIG10LWF1dG8gbXQtbGctMFwiPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiXCIgY2xhc3NOYW1lPVwiYnRuLWluaWNpYXItc2VzaW9uXCI+aW5pY2lhciBzZXNpw7NuPC9hPlxyXG4gICAgICAgICAgPC9saT5cclxuICAgICAgICA8L3VsPlxyXG4gICAgICA8L2hlYWRlcj5cclxuICAgICAgPGRpdiBjbGFzc05hbWU9J3doaXRlLXNwYWNlJyBzdHlsZT17eyBoZWlnaHQ6IGhlaWdodCB9fSAvPlxyXG5cclxuICAgIDwvPlxyXG4gIClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgSGVhZGVyOyIsImltcG9ydCBIZWFkIGZyb20gJ25leHQvaGVhZCdcclxuXHJcbmltcG9ydCBGb290ZXIgZnJvbSAnLi4vY29tcG9uZW50cy9Gb290ZXInXHJcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi4vY29tcG9uZW50cy9oZWFkZXIvJ1xyXG5pbXBvcnQgeyBQcm92aWRlciB9IGZyb20gJy4uL1N0b3JlJ1xyXG5cclxuaW1wb3J0ICcuLi9zdHlsZXMvc3R5bGVzLnNjc3MnXHJcblxyXG5pbXBvcnQgJ3N3aXBlci9zd2lwZXIuc2Nzcyc7XHJcbmltcG9ydCAnc3dpcGVyL2NvbXBvbmVudHMvcGFnaW5hdGlvbi9wYWdpbmF0aW9uLnNjc3MnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQXBwKHsgQ29tcG9uZW50LCBwYWdlUHJvcHMgfSkge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8PlxyXG4gICAgICAgICAgICA8SGVhZD5cclxuICAgICAgICAgICAgICAgIDx0aXRsZT5BY2FkZW1pYSBNQU1CTzwvdGl0bGU+XHJcbiAgICAgICAgICAgICAgICA8YSByZWw9XCJpY29uXCIgaHJlZj1cIi9mYXZpY29uLmljb1wiIC8+XHJcbiAgICAgICAgICAgIDwvSGVhZD5cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIDxIZWFkZXIgLz5cclxuICAgICAgICAgICAgPFByb3ZpZGVyPlxyXG4gICAgICAgICAgICAgICAgPENvbXBvbmVudCB7Li4ucGFnZVByb3BzfSAvPlxyXG4gICAgICAgICAgICA8L1Byb3ZpZGVyPlxyXG4gICAgICAgICAgICA8Rm9vdGVyIC8+XHJcbiAgICAgICAgPC8+XHJcbiAgICApXHJcbn0iXSwic291cmNlUm9vdCI6IiJ9